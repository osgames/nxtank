// mazeViewer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "mazeViewer.h"

#define MAX_LOADSTRING  100
#define MAX_OF_MAZETYPE 2000				    // Thats alot of mazes I hope

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

//
// This should be a structure, but I was in a hurry
//
TCHAR* mazenmlist[MAX_OF_MAZETYPE];             // Lookup Table 
int mazenminfo[MAX_OF_MAZETYPE];                // Lookup Table 
int mazenmtype[MAX_OF_MAZETYPE];                // Lookup Table 

TCHAR szMazeName[MAX_LOADSTRING];               // If the first arg is set
int mazeid = -1;							    // If read from command line
int mazeNameSz = 0;							    // Size of the Maze Name

Maze maze = NULL;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

    //
	// Read first MazeName if its set
	//
	for (int i=0, j=0; i<MAX_LOADSTRING-1; i++) {
       TCHAR tch = lpCmdLine[i];
	   if ((tch == L' ') || (tch == (TCHAR) 0)) {
          szMazeName[j]=(TCHAR) 0;
          break;
	   } 		   
	   if ((tch == L'/') || (tch == L'\\')) {
	      j=0;
	   } else {
    	  szMazeName[j] = tch;
		  j++;
	   }
	}
    //
	// Were allowing the extension then filtering it off, because
	// if you map the file type to MazeViewer when you click on it
	// the MazeViewer will automatically load it.
    //
    mazeNameSz = wcslen(szMazeName);
	if (mazeNameSz >= 6 ) { // Basically we are stripping off the extension
	   if (wcsncmp(TEXT(".maze"),szMazeName+(mazeNameSz-5), 5) == 0) {
          szMazeName[(mazeNameSz-4)] = (TCHAR) 0;
	   }
	}

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MAZEVIEWER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MAZEVIEWER));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MAZEVIEWER));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MAZEVIEWER);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   // Scan directories
   HANDLE hFind = INVALID_HANDLE_VALUE;
   WIN32_FIND_DATA ffd;
   HMENU hMenu;
   HMENU hMenuPopup;
   int labelSz = 0;
   TCHAR label[MAX_LOADSTRING];
   TCHAR* tmpmazelabel;
   int mazecounter = 0;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowEx(NULL,szWindowClass,szTitle,
	      WS_OVERLAPPEDWINDOW|WS_HSCROLL|WS_VSCROLL,
	      0,0,725,835,NULL,NULL,hInstance,NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   // Modify Menu
   hMenu = GetMenu(hWnd);
   AppendMenu(hMenu, MF_STRING, 0, TEXT("Load Legacy ["));

   // read dir 
   hMenuPopup = CreateMenu();
   hFind = FindFirstFile(TEXT("C:\\PythonOgre\\nXtank\\mazes\\legacy\\combat\\*"), &ffd);
   if (hFind != INVALID_HANDLE_VALUE) {
      do {
         labelSz = wcslen(ffd.cFileName);
		 if (labelSz >= 6 ) { // filters out ., .., stuff that is not x.maze
            wcsncpy(label, ffd.cFileName+(labelSz - 5), 5);
			if (wcsncmp(TEXT(".maze"),label, 5) == 0) {
               wcsncpy(label, ffd.cFileName, (labelSz - 5));
			   label[(labelSz - 5)] = (TCHAR) 0;
               AppendMenu(hMenuPopup, MF_STRING, (IDM_MAZE+mazecounter), label);
               labelSz = wcslen(label);
               tmpmazelabel = (TCHAR *) malloc((labelSz+1) * sizeof(TCHAR));
			   wcsncpy(tmpmazelabel, label, labelSz+1);
               mazenmlist[mazecounter] = tmpmazelabel;
			   mazenminfo[mazecounter] = labelSz;
			   mazenmtype[mazecounter] = 0;
               //
			   // Check for Commandline Maze
			   //
			   if ((mazeid == -1) && (wcsncmp(szMazeName,tmpmazelabel, labelSz) == 0)) {
                  mazeid = IDM_MAZE+mazecounter;
			   }
               mazecounter++;
			}
		 }
	  } while (FindNextFile(hFind, &ffd) != 0);
   }
   AppendMenu(hMenu, MF_POPUP, (UINT_PTR) hMenuPopup, TEXT("Combat,"));
   // read dir 
   hMenuPopup = CreateMenu();
   hFind = FindFirstFile(TEXT("C:\\PythonOgre\\nXtank\\mazes\\legacy\\war\\*.maze"), &ffd);
   if (hFind != INVALID_HANDLE_VALUE) {
      do {
         labelSz = wcslen(ffd.cFileName);
		 if (labelSz >= 6 ) { // filters out ., .., stuff that is not x.maze
            wcsncpy(label, ffd.cFileName+(labelSz - 5), 5);
			if (wcsncmp(TEXT(".maze"),label, 5) == 0) {
               wcsncpy(label, ffd.cFileName, (labelSz - 5));
			   label[(labelSz - 5)] = (TCHAR) 0;
               AppendMenu(hMenuPopup, MF_STRING, (IDM_MAZE+mazecounter), label);
			   labelSz = wcslen(label);
               tmpmazelabel = (TCHAR *) malloc((labelSz+1) * sizeof(TCHAR));
			   wcsncpy(tmpmazelabel, label, labelSz+1);
               mazenmlist[mazecounter] = tmpmazelabel;
			   mazenminfo[mazecounter] = labelSz;
			   mazenmtype[mazecounter] = 1;
			   //
			   // Check for Commandline Maze
			   //
			   if ((mazeid == -1) && (wcsncmp(szMazeName,tmpmazelabel, labelSz) == 0)) {
                  mazeid = IDM_MAZE+mazecounter;
			   }
               mazecounter++;
			}
		 }
	  } while (FindNextFile(hFind, &ffd) != 0);
   }
   AppendMenu(hMenu, MF_POPUP, (UINT_PTR) hMenuPopup, TEXT("War,"));
   // read dir 
   hMenuPopup = CreateMenu();
   hFind = FindFirstFile(TEXT("C:\\PythonOgre\\nXtank\\mazes\\legacy\\ultimate\\*.maze"), &ffd);
   if (hFind != INVALID_HANDLE_VALUE) {
      do {
         labelSz = wcslen(ffd.cFileName);
		 if (labelSz >= 6 ) { // filters out ., .., stuff that is not x.maze
            wcsncpy(label, ffd.cFileName+(labelSz - 5), 5);
			if (wcsncmp(TEXT(".maze"),label, 5) == 0) {
               wcsncpy(label, ffd.cFileName, (labelSz - 5));
			   label[(labelSz - 5)] = (TCHAR) 0;
               AppendMenu(hMenuPopup, MF_STRING, (IDM_MAZE+mazecounter), label);
			   labelSz = wcslen(label);
               tmpmazelabel = (TCHAR *) malloc((labelSz+1) * sizeof(TCHAR));
			   wcsncpy(tmpmazelabel, label, labelSz+1);
               mazenmlist[mazecounter] = tmpmazelabel;
			   mazenminfo[mazecounter] = labelSz;
			   mazenmtype[mazecounter] = 2;
               //
			   // Check for Commandline Maze
			   //
			   if ((mazeid == -1) && (wcsncmp(szMazeName,tmpmazelabel, labelSz) == 0)) {
                  mazeid = IDM_MAZE+mazecounter;
			   }
               mazecounter++;
			}
		 }
	  } while (FindNextFile(hFind, &ffd) != 0);
   }
   AppendMenu(hMenu, MF_POPUP, (UINT_PTR) hMenuPopup, TEXT("Ultimate,"));
   // read dir 
   hMenuPopup = CreateMenu();
   hFind = FindFirstFile(TEXT("C:\\PythonOgre\\nXtank\\mazes\\legacy\\capture\\*.maze"), &ffd);
   if (hFind != INVALID_HANDLE_VALUE) {
      do {
         labelSz = wcslen(ffd.cFileName);
		 if (labelSz >= 6 ) { // filters out ., .., stuff that is not x.maze
            wcsncpy(label, ffd.cFileName+(labelSz - 5), 5);
			if (wcsncmp(TEXT(".maze"),label, 5) == 0) {
               wcsncpy(label, ffd.cFileName, (labelSz - 5));
			   label[(labelSz - 5)] = (TCHAR) 0;
               AppendMenu(hMenuPopup, MF_STRING, (IDM_MAZE+mazecounter), label);
			   labelSz = wcslen(label);
               tmpmazelabel = (TCHAR *) malloc((labelSz+1) * sizeof(TCHAR));
			   wcsncpy(tmpmazelabel, label, labelSz+1);
               mazenmlist[mazecounter] = tmpmazelabel;
			   mazenminfo[mazecounter] = labelSz;
			   mazenmtype[mazecounter] = 3;
               //
			   // Check for Commandline Maze
			   //
			   if ((mazeid == -1) && (wcsncmp(szMazeName,tmpmazelabel, labelSz) == 0)) {
                  mazeid = IDM_MAZE+mazecounter;
			   }
               mazecounter++;
			}
		 }
	  } while (FindNextFile(hFind, &ffd) != 0);
   }
   AppendMenu(hMenu, MF_POPUP, (UINT_PTR) hMenuPopup, TEXT("Capture,"));
   // read dir
   hMenuPopup = CreateMenu();
   hFind = FindFirstFile(TEXT("C:\\PythonOgre\\nXtank\\mazes\\legacy\\race\\*.maze"), &ffd);
   if (hFind != INVALID_HANDLE_VALUE) {
      do {
         labelSz = wcslen(ffd.cFileName);
		 if (labelSz >= 6 ) { // filters out ., .., stuff that is not x.maze
            wcsncpy(label, ffd.cFileName+(labelSz - 5), 5);
			if (wcsncmp(TEXT(".maze"),label, 5) == 0) {
               wcsncpy(label, ffd.cFileName, (labelSz - 5));
			   label[(labelSz - 5)] = (TCHAR) 0;
               AppendMenu(hMenuPopup, MF_STRING, (IDM_MAZE+mazecounter), label);
			   labelSz = wcslen(label);
               tmpmazelabel = (TCHAR *) malloc((labelSz+1) * sizeof(TCHAR));
			   wcsncpy(tmpmazelabel, label, labelSz+1);
               mazenmlist[mazecounter] = tmpmazelabel;
			   mazenminfo[mazecounter] = labelSz;
			   mazenmtype[mazecounter] = 4;
               //
			   // Check for Commandline Maze
			   //
			   if ((mazeid == -1) && (wcsncmp(szMazeName,tmpmazelabel, labelSz) == 0)) {
                  mazeid = IDM_MAZE+mazecounter;
			   }
               mazecounter++;
			}
		 }
	  } while (FindNextFile(hFind, &ffd) != 0);
   }
   AppendMenu(hMenu, MF_POPUP, (UINT_PTR) hMenuPopup, TEXT("Race"));
   AppendMenu(hMenu, MF_STRING, 0, TEXT("]"));

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	TEXTMETRIC tm;
	static int cxChar, cxCaps, cyChar, iVscrollPos = 0, iHscrollPos = 0;;
	HDC hdc, hdcMem, hdcMask;

	BITMAP img[MAXMAZETYPES+MAXCOLORS];
	static HBITMAP himg[MAXMAZETYPES+MAXCOLORS];

	static int xsrc, ysrc;

	static int xwsz; // Current Window Width in Pixels
	static int ywsz; // Current Window Height in Pixels

    int xoff; // OffSet due to scrollbar
    int yoff; // OffSet due to scrollbar

    HINSTANCE hInstance;
	int x,y,l,l2,l3,i;

    TCHAR filename[MAX_LOADSTRING];

	TCHAR szInfo[MAX_LOADSTRING];		// For Info Scrings 
    char* infostr;					    // Info String

//	TCHAR debug[MAX_LOADSTRING]; 
//    TCHAR* text;
//	int textSz = 0;

	switch (message)
	{
	case WM_CREATE:
        hdc = GetDC(hWnd);
		//
		// Set Text Metrics
		//
        GetTextMetrics (hdc, &tm);
		cxChar = tm.tmAveCharWidth;
		cxCaps = (tm.tmPitchAndFamily & 1 ? 3 : 2) * cxChar / 2;
		cyChar = tm.tmHeight + tm.tmExternalLeading;
        //
		// Scroll Bar
		//
        SetScrollRange(hWnd, SB_VERT, 0, 1024, FALSE);
		SetScrollPos(hWnd, SB_VERT, iVscrollPos, TRUE);
        SetScrollRange(hWnd, SB_HORZ, 0, 1024, FALSE);
		SetScrollPos(hWnd, SB_HORZ, iHscrollPos, TRUE);
        //
        // Load BITMAPS
		//
		hInstance = ((LPCREATESTRUCT) lParam)->hInstance;
		himg[NORMAL] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_FLOOR));
		himg[FUEL] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_FUEL));
		himg[AMMO] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_AMMO));
		himg[ARMOR] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_ARMOR));
		himg[GOAL] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_SAFE));
		himg[OUTPOST] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_OUTPOST));
		himg[SCROLL_N] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_N));
		himg[SCROLL_NE] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_NE));
		himg[SCROLL_E] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_E));
		himg[SCROLL_SE] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_SE));
		himg[SCROLL_S] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_S));
		himg[SCROLL_SW] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_SW));
		himg[SCROLL_W] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_W));
		himg[SCROLL_NW] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_NW));
		himg[SLIP] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_SLIP));
		himg[SLOW] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_STOP));
		himg[START] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_START));
		himg[PEACE] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_PEACE));
		himg[TELEPORT] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_TELEPORT));
		himg[DNORTH] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_DNORTH));
		himg[DWEST] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_DWEST));
		himg[NORTH]= LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_NORTH));
		himg[WEST] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_WEST));
		himg[EMPTY] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_EMPTY));
		himg[OUTOFBOUNDS] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_OUTOFBOUNDS));

		himg[MAXMAZETYPES+GREY] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_GREY));
		himg[MAXMAZETYPES+RED] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_RED));
		himg[MAXMAZETYPES+ORANGE] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_ORANGE));
		himg[MAXMAZETYPES+YELLOW] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_YELLOW));
		himg[MAXMAZETYPES+GREEN] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_GREEN));
		himg[MAXMAZETYPES+BLUE] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_BLUE));
		himg[MAXMAZETYPES+VIOLET] = LoadBitmap(hInstance, MAKEINTRESOURCE(MVB_VIOLET));

		for(i=0; i<MAXMAZETYPES; i++) {
		   GetObject(himg[i], sizeof (BITMAP), &img[i]);
		}
		for(i=MAXMAZETYPES; i<(MAXMAZETYPES+MAXCOLORS); i++) {
		   GetObject(himg[i], sizeof (BITMAP), &img[i]);
		}

//		xsrc = img[EMPTY].bmWidth;
//		ysrc = img[EMPTY].bmHeight;
		xsrc = EMPTY_WIDTH;
		ysrc = EMPTY_HEIGHT;

		ReleaseDC(hWnd, hdc);
		break;
	case WM_SIZE:
		xwsz = LOWORD(lParam);
		ywsz = HIWORD(lParam);
		break;
	case WM_VSCROLL:
		switch (LOWORD(wParam)) {
 		   case SB_LINEUP:
			  iVscrollPos +=1;
			  break;
		   case SB_LINEDOWN:
			  iVscrollPos -=1;
			  break;
		   case SB_PAGEUP:
			  iVscrollPos -=ywsz/5;
			  break;
		   case SB_PAGEDOWN:
			  iVscrollPos +=ywsz/5;
			  break;
		   case SB_THUMBPOSITION:
			  iVscrollPos = HIWORD(wParam);
			  break;
		   default:
			  break;
		}
		iVscrollPos = max(0, min(iVscrollPos, 1024));

		if (iVscrollPos != GetScrollPos(hWnd, SB_VERT)) {
		   SetScrollPos(hWnd, SB_VERT, iVscrollPos, TRUE);
		   InvalidateRect(hWnd, NULL, TRUE);
		} 
        break;
	case WM_HSCROLL:
		switch (LOWORD(wParam)) {
 		   case SB_LINEUP:
			  iHscrollPos +=1;
			  break;
		   case SB_LINEDOWN:
			  iHscrollPos -=1;
			  break;
		   case SB_PAGEUP:
			  iHscrollPos -=xwsz/5;
			  break;
		   case SB_PAGEDOWN:
			  iHscrollPos +=xwsz/5;
			  break;
		   case SB_THUMBPOSITION:
			  iHscrollPos = HIWORD(wParam);
			  break;
		   default:
			  break;
		}
		iHscrollPos = max(0, min(iHscrollPos, 1024));

		if (iHscrollPos != GetScrollPos(hWnd, SB_HORZ)) {
		   SetScrollPos(hWnd, SB_HORZ, iHscrollPos, TRUE);
		   InvalidateRect(hWnd, NULL, TRUE);
		} 
        break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
	        if ((wmId >= IDM_MAZE) && (wmId <= IDM_MAZE+MAX_OF_MAZETYPE)) {
			   switch(mazenmtype[wmId-IDM_MAZE]) {
		          case 1:
					 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\war\\\\"), 44);
                     wcsncpy(filename+44, mazenmlist[wmId-IDM_MAZE], mazenminfo[wmId-IDM_MAZE]);
                     wcsncpy(filename+44+mazenminfo[wmId-IDM_MAZE], TEXT(".maze\0"), 6);
					 maze.load(filename);
					 break;
		          case 2:
					 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\ultimate\\\\"), 49);
                     wcsncpy(filename+49, mazenmlist[wmId-IDM_MAZE], mazenminfo[wmId-IDM_MAZE]);
                     wcsncpy(filename+49+mazenminfo[wmId-IDM_MAZE], TEXT(".maze\0"), 6);
					 maze.load(filename);
					 break;
		          case 3:
					 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\capture\\\\"), 48);
                     wcsncpy(filename+48, mazenmlist[wmId-IDM_MAZE], mazenminfo[wmId-IDM_MAZE]);
                     wcsncpy(filename+48+mazenminfo[wmId-IDM_MAZE], TEXT(".maze\0"), 6);
					 maze.load(filename);
					 break;
		          case 4:
					 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\race\\\\"), 45);
                     wcsncpy(filename+45, mazenmlist[wmId-IDM_MAZE], mazenminfo[wmId-IDM_MAZE]);
                     wcsncpy(filename+45+mazenminfo[wmId-IDM_MAZE], TEXT(".maze\0"), 6);
					 maze.load(filename);
					 break;
				  default:
					 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\combat\\\\"), 47);
                     wcsncpy(filename+47, mazenmlist[wmId-IDM_MAZE], mazenminfo[wmId-IDM_MAZE]);
                     wcsncpy(filename+47+mazenminfo[wmId-IDM_MAZE], TEXT(".maze\0"), 6);
					 maze.load(filename);
				     break;
			   }

			   InvalidateRect(hWnd, NULL, TRUE);

               return 0;
	        }
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:

	    if (mazeid != -1) {
		   switch(mazenmtype[mazeid-IDM_MAZE]) {
		      case 1:
				 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\war\\\\"), 44);
                 wcsncpy(filename+44, mazenmlist[mazeid-IDM_MAZE], mazenminfo[mazeid-IDM_MAZE]);
                 wcsncpy(filename+44+mazenminfo[mazeid-IDM_MAZE], TEXT(".maze\0"), 6);
				 maze.load(filename);
				 break;
		      case 2:
				 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\ultimate\\\\"), 49);
                 wcsncpy(filename+49, mazenmlist[mazeid-IDM_MAZE], mazenminfo[mazeid-IDM_MAZE]);
                 wcsncpy(filename+49+mazenminfo[mazeid-IDM_MAZE], TEXT(".maze\0"), 6);
				 maze.load(filename);
				 break;
		      case 3:
				 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\capture\\\\"), 48);
                 wcsncpy(filename+48, mazenmlist[mazeid-IDM_MAZE], mazenminfo[mazeid-IDM_MAZE]);
                 wcsncpy(filename+48+mazenminfo[mazeid-IDM_MAZE], TEXT(".maze\0"), 6);
				 maze.load(filename);
				 break;
		      case 4:
				 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\race\\\\"), 45);
                 wcsncpy(filename+45, mazenmlist[mazeid-IDM_MAZE], mazenminfo[mazeid-IDM_MAZE]);
                 wcsncpy(filename+45+mazenminfo[mazeid-IDM_MAZE], TEXT(".maze\0"), 6);
				 maze.load(filename);
				 break;
			  default:
				 wcsncpy(filename, TEXT("C:\\\\PythonOgre\\\\nXtank\\\\mazes\\\\legacy\\\\combat\\\\"), 47);
                 wcsncpy(filename+47, mazenmlist[mazeid-IDM_MAZE], mazenminfo[mazeid-IDM_MAZE]);
                 wcsncpy(filename+47+mazenminfo[mazeid-IDM_MAZE], TEXT(".maze\0"), 6);
		    	 maze.load(filename);
			     break;
		   }
		   mazeid = -1;
		   InvalidateRect(hWnd, NULL, TRUE);
		}

		hdc = BeginPaint(hWnd, &ps);
		// 
        // Adjust Offsets
		//
		xoff = -iHscrollPos;
        yoff = -iVscrollPos;

        hdcMem = CreateCompatibleDC(hdc);
		if ((maze.getWidth() > 0) && (maze.getHeight() > 0)) {
		   for (y=0; y<maze.getHeight(); y++) {
		      for (x=0; x<maze.getWidth(); x++) {
  				 mapInfo* mi = maze.getBox(y+(x*maze.getWidth()));
				 if (mi->inmaze == TRUE) {
   			        SelectObject(hdcMem, himg[EMPTY]);
				 }else {
				    SelectObject(hdcMem, himg[OUTOFBOUNDS]);
				 }
 	             BitBlt(hdc, (x*xsrc)+xoff, (y*ysrc)+yoff+60, xsrc, ysrc, hdcMem, 0, 0, SRCCOPY);
				 if (mi != NULL) {
					if (mi->north == -1) {
			           SelectObject(hdcMem, himg[NORTH]);
		               BitBlt(hdc, (x*xsrc)+2+xoff, (y*ysrc)+yoff+60, xsrc-2, 2, hdcMem, 0, 0, SRCCOPY);

					} else if (mi->north > 0) {
			           SelectObject(hdcMem, himg[DNORTH]);
		               BitBlt(hdc, (x*xsrc)+2+xoff, (y*ysrc)+yoff+60, xsrc-2, 2, hdcMem, 0, 0, SRCCOPY);
					}
					if (mi->west == -1) {
			           SelectObject(hdcMem, himg[WEST]);
		               BitBlt(hdc, (x*xsrc)+xoff, (y*ysrc)+2+yoff+60, 2, ysrc-2, hdcMem, 0, 0, SRCCOPY);
					} else if (mi->west > 0) {
			           SelectObject(hdcMem, himg[DWEST]);
		               BitBlt(hdc, (x*xsrc)+xoff, (y*ysrc)+2+yoff+60, 2, ysrc-2, hdcMem, 0, 0, SRCCOPY);
					}
                    //
				    // Set owner of tile
                    //					
					if ((mi->owner >= 1) && (mi->owner <= MAXCOLORS)) {
					   SelectObject(hdcMem, himg[MAXMAZETYPES+mi->owner]);
		               BitBlt(hdc, (x*xsrc)+0+xoff, (y*ysrc)+yoff+60, xsrc, ysrc, hdcMem, 0, 0, SRCAND);
					}

					if ((mi->inmaze == TRUE) || (mi->loctype > 0)) {
			              SelectObject(hdcMem, himg[mi->loctype]);
		                  BitBlt(hdc, (x*xsrc)+2+xoff, (y*ysrc)+2+yoff+60, xsrc-2, ysrc-2, hdcMem, 0, 0, 0x220326);
					}
                    //
					//
					// This is a cludge till can get transparent images working.
					//
					if ((mi->val > 0) && ((mi->loctype == START) || (mi->loctype == TELEPORT) || 
						(mi->loctype == GOAL) || (mi->loctype == OUTPOST))) {
                       szInfo[0] = (TCHAR) L'0'+((mi->val)&0x00FF);
					   szInfo[1] = (TCHAR) 0;
 				       TextOut (hdc, (x*xsrc)+2+xoff, (y*ysrc)+2+yoff+60, szInfo,2);
					}
					//
					// End of the cludge
					//

					//
					// Name and Author
					//
                    infostr = maze.getMazeName();
					if (infostr != NULL) {
					   l = strlen(infostr);
					   wcsncpy(szInfo, TEXT("Name: "), 6);
   					   l2=6;
					   l3=0;
					   for (i=0; ((i<l)&&(i<MAX_LOADSTRING)); i++) {
                           szInfo[i+l2] = (TCHAR) (((char) infostr[i])&0x00FF);
					       l3++;
					   }
					   l2+=l3;
					   if (l+11<MAX_LOADSTRING) {
                          infostr = maze.getMazeAuthor();
					      if (infostr != NULL) {
					         l = strlen(infostr);
					         wcsncpy(szInfo+l2, TEXT(" - Author: "), 11);
 							 l2+=11;
							 l3=0;
					         for (i=0; ((i<l) && (i<MAX_LOADSTRING)); i++) {
                                szInfo[i+l2] = (TCHAR) (((char) infostr[i])&0x00FF);
								l3++;
					         }
							 l2+=l3;
						  }
					   }
					   TextOut (hdc, 0+xoff, 0+yoff, szInfo,l2);
					}
					//
					// Size and Type
					//
  			        wcsncpy(szInfo, TEXT("Size: "), 6);
					l2=6;
					if (maze.getWidth() > 100) {
                       szInfo[l2++] = (TCHAR) L'0'+((maze.getWidth()/100)&0x00FF);
					}
					if (maze.getWidth() > 10) {
                       szInfo[l2++] = (TCHAR) L'0'+(((maze.getWidth()/10)%10)&0x00FF);
					}
                    szInfo[l2++] = (TCHAR) L'0'+((maze.getWidth()%10)&0x00FF);
  			        szInfo[l2++] = (TCHAR) L'x';
					if (maze.getHeight() > 100) {
                       szInfo[l2++] = (TCHAR) L'0'+((maze.getHeight()/100)&0x00FF);
					}
					if (maze.getHeight() > 10) {
                       szInfo[l2++] = (TCHAR) L'0'+(((maze.getHeight()/10)%10)&0x00FF);
					}
                    szInfo[l2++] = (TCHAR) L'0'+((maze.getHeight()%10)&0x00FF);
  			        wcsncpy(szInfo+l2, TEXT(" - "), 3);
					l2+=3;
					switch (maze.getMazeType()) {
					   case 0:
					      wcsncpy(szInfo+l2, TEXT("Type: COMBAT"), 12);
						  l2+=12;
						  break;
					   case 1:
						  wcsncpy(szInfo+l2, TEXT("Type: WAR"), 9);
						  l2+=9;
						  break;
					   case 2:
						  wcsncpy(szInfo+l2, TEXT("Type: ULTIMATE"), 14);
						  l2+=14;
						  break;
					   case 3:
						  wcsncpy(szInfo+l2, TEXT("Type: CAPTURE"), 13);
						  l2+=13;
						  break;
					   case 4:
						  wcsncpy(szInfo+l2, TEXT("Type: RACE"), 10);
						  l2+=10;
						  break;
					}
					TextOut (hdc, 0+xoff, 20+yoff, szInfo,l2);

                    infostr = maze.getMazeDesc();
					if (infostr != NULL) {
					   l = strlen(infostr);
					   wcsncpy(szInfo, TEXT("Desc: "), 6);
   					   l2=6;
					   l3=0;
					   for (i=0; ((i<l)&&(i<MAX_LOADSTRING)); i++) {
                           szInfo[i+l2] = (TCHAR) (((char) infostr[i])&0x00FF);
					       l3++;
					   }
					   l2+=l3;
	   				   TextOut (hdc, 0+xoff, 40+yoff, szInfo,l2);
					}
				 }
		      }
		   }
		}
		DeleteDC (hdcMem);
        //
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		for(int i=0; i<MAXMAZETYPES; i++) {
		   DeleteObject(himg[i]);
		}
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
