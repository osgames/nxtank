//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by mazeViewer.rc
//

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_MAZEVIEWER_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_MAZEVIEWER			107
#define IDI_SMALL				108
#define IDC_MAZEVIEWER			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif

#define IDM_MAZE				2000    // Range: 2000 - 4000

//
//
#define MVB_AMMO			500
#define MVB_ARMOR			501
#define MVB_DNORTH		502
#define MVB_DWEST			503
#define MVB_E			504
#define MVB_EMPTY			505
#define MVB_FLOOR			506
#define MVB_FUEL			507
#define MVB_N			508
#define MVB_NE			509
#define MVB_NORTH			510
#define MVB_NW			511
#define MVB_OUTPOST		512
#define MVB_PEACE			513
#define MVB_S			514
#define MVB_SAFE			515			// GOAL
#define MVB_SE			516
#define MVB_SLIP			517
#define MVB_START			518
#define MVB_STOP			519
#define MVB_SW			520
#define MVB_TELEPORT		521
#define MVB_W			522
#define MVB_WEST			523
#define MVB_OUTOFBOUNDS		524

#define MVB_GREY		700
#define MVB_RED		701
#define MVB_ORANGE	702
#define MVB_YELLOW	703
#define MVB_GREEN		704
#define MVB_BLUE		705
#define MVB_VIOLET	706




