#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NORMAL            0
#define FUEL              1
#define AMMO              2
#define ARMOR             3
#define GOAL              4
#define OUTPOST           5
#define PEACE             6
#define TELEPORT          7
#define SCROLL_N          8
#define SCROLL_NE         9
#define SCROLL_E          10
#define SCROLL_SE         11
#define SCROLL_S          12
#define SCROLL_SW         13
#define SCROLL_W          14
#define SCROLL_NW         15
#define SLIP              16
#define SLOW              17
#define START             18
//
// Used in Viewer/Editor Only
//
#define DNORTH            19
#define DWEST             20
#define NORTH             21
#define WEST              22
#define EMPTY             23
#define OUTOFBOUNDS       24
//
#define MAXMAZETYPES	  25

//
// Colors and Team Colors
//
#define GREY		  0
#define RED			  1	
#define ORANGE		  2
#define YELLOW		  3
#define GREEN		  4
#define BLUE		  5
#define VIOLET		  6

#define MAXCOLORS	  7

typedef struct {
   int type;     // Unique Value
   HBITMAP *img;  // Image associated with that Value
} mapTile;

typedef struct {
   int x;        // Coordinates
   int y;        // Coordinates
   int north;    // 0 = no, -1 = indestructable, anything else is destructable damage
   int west;     // 0 = no, -1 = indestructable, anything else is destructable damage
   int owner;    // team number, -1 = no team
   bool inmaze;  // Flag set to show where tanks are allowed to move.
   int loctype;  // Location Type
   int val;      // Location Types TELEPORT an START use this.
   mapTile tile; // 
} mapInfo;

class Maze
{
private:
	HWND hWnd;
	int chartonum(int ch);
protected:
	int type;
	char* name;
	char* author;
    char* desc;
	int width;
	int height;
    mapInfo** map; // Array of MapInfo
public:
	Maze(HWND hWnd);
	Maze(void);
	~Maze(void);
	int load(TCHAR* path);
    int load(char* path);
    int getMazeType(void);
	char* getMazeName(void);
	char* getMazeAuthor(void);
	char* getMazeDesc(void);
	int getWidth(void);
	int getHeight(void);
	mapInfo* getBox(int i);
	mapInfo* getBox(int x, int y);
};
