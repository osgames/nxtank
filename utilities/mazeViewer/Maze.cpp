#include "StdAfx.h"
#include "Maze.h"

Maze::Maze(HWND h)
{
   hWnd = h;
   type = 0;
   name = NULL;
   author = NULL;
   desc = NULL;
   width = 0;
   height = 0;
   map = NULL;
}

Maze::Maze(void)
{
   hWnd = NULL;
   type = 0;
   name = NULL;
   author = NULL;
   desc = NULL;
   width = 0;
   height = 0;
   map = NULL;
}

Maze::~Maze(void)
{
}

//
// convert to 8 bit char path and load.
//
int Maze::load(TCHAR* path)
{
    char buffer[1024];
    int j = 0;

	if (path != NULL) {
       int l = wcslen(path);
	   if (l > 1023)  {
	      l = 1023;
	   }
	   for (j=0; j<l; j++) {
          buffer[j] = (char) ((path[j] & 0x00FF)>>0);
	   }
	   buffer[l] = (char) 0;
       return(this->load(buffer));
	}
	return -1;
}

int Maze::getMazeType(void) {
   return this->type;
}

char* Maze::getMazeName(void) {
   int l = strlen(this->name) + 1;
   char* ret = NULL;
   ret = (char *) malloc(l * sizeof(char));
   if (ret != NULL) {
      strncpy_s(ret, l, this->name, l);
   }
   return ret;
}

char* Maze::getMazeAuthor(void) {
   int l = strlen(this->author) + 1;
   char* ret = NULL;
   ret = (char *) malloc(l * sizeof(char));
   if (ret != NULL) {
      strncpy_s(ret, l, this->author, l);
   }
   return ret;
}

char* Maze::getMazeDesc(void) {
   char* ret = NULL;
   if (this->desc != NULL) {
      int l = strlen(this->desc) + 1;
      ret = (char *) malloc(l * sizeof(char));
      if (ret != NULL) {
         strncpy_s(ret, l, this->desc, l);
      }
   }
   return ret;
}

int Maze::getWidth(void) { 
   return this->width; 
}

int Maze::getHeight(void) { 
   return this->height; 
}

mapInfo* Maze::getBox(int i) {
   mapInfo* ret = NULL;
   if ((i >= 0) && (i < (this->width*this->height))) {
      ret = map[i];
   }      
   return ret;
}
mapInfo* Maze::getBox(int x, int y) {
   int i = -1;
   if ((x >= 0) && (x < this->width)) {
      if ((y >= 0) && (y < this->height)) {
	     i = (y + (this->width*x));
         return getBox(i);
	  }
   }
   return NULL;
}

int Maze::chartonum(int ch) {
	if ((ch == '0') || ((ch >= '1') && (ch <= '9'))) {
		return (int) ((char) ch - '0');
	} else {
       return -1;
	}
}

int Maze::load(char* path)
{
//    TCHAR szBuffer[1024];
    char buffer[1024];
	char* buf;
	FILE *file; 
    bool indata = false; // Checks to see if the data thats being loaded 
	                     // is to be processed as map data. 
	int x;
	int y;
	int idata; // Number of Data items;

	if (path != NULL) {
       int l = strlen(path);
	   if (l > 1023)  {
	      l = 1023;
	   }

//       for (int j=0; j<1023; j++) {
//          szBuffer[j] = (TCHAR) (((char) path[j] & 0x00FF)<<0);
//       }
//	   szBuffer[l] = (TCHAR) 0;
//       MessageBoxW(hWnd, szBuffer, NULL, MB_OK);
      


       if ((file = fopen(path, "r")) == NULL) {
          return -1; 
	   }

       //
       // Read a line a a time
       //
       buf = fgets(buffer, 1024, file);
	   while (buf != NULL) {
		  if (strncmp("Maze Type:", buf, 10) == 0) {  // Load Maze Type
			 buf = strstr(buf, ": ");
             buf = buf+2;
			 if (strncmp("COMBAT", buf, 6) == 0) {
			 this->type = 0;
			 } else if (strncmp("WAR", buf, 3) == 0) {
			    this->type = 1;
			 } else if (strncmp("ULTIMATE", buf, 8) == 0) {
			    this->type = 2;
			 } else if (strncmp("CAPTURE", buf, 7) == 0) {
			    this->type = 3;
			 } else if (strncmp("RACE", buf, 4) == 0) {
			    this->type = 4;
			 }
		  } else if (strncmp("Maze Name:", buf, 10) == 0) {  // Load Maze Name
			 buf = strstr(buf, ": ");
             buf = buf+2;
			 l = strlen(buf);
			 this->name = (char *) malloc((l+1)*sizeof(char));
			 strncpy_s(this->name, l+1, buf, l); 
			 this->name[l-1] = (char) 0; // Strip off newline
		  } else if (strncmp("Maze Designer:", buf, 14) == 0) {  // Load Maze Designer/Author
			 buf = strstr(buf, ": ");
             buf = buf+2;
			 l = strlen(buf);
			 this->author = (char *) malloc(l*sizeof(char));
			 strncpy(this->author, buf, l); 
			 this->author[l-1] = (char) 0; // Strip off newline
		  } else if (strncmp("Maze Desc:", buf, 10) == 0) {  // Load Maze Description
			 buf = strstr(buf, ": ");
             buf = buf+2;
			 l = strlen(buf);
			 this->desc = (char *) malloc(l*sizeof(char));
			 strncpy(this->desc, buf, l); 
			 this->desc[l-1] = (char) 0; // Strip off newline
//			 for (int j=0; j<l; j++) {
//				szBuffer[j+5] = (TCHAR) (((char) this->desc[j] & 0x00FF)<<0);
//				szBuffer[0] = (TCHAR) L'd';
//				szBuffer[1] = (TCHAR) L'e';
//				szBuffer[2] = (TCHAR) L's';
//				szBuffer[3] = (TCHAR) L'c';
//				szBuffer[4] = (TCHAR) L'[';
//				szBuffer[l+4] = (TCHAR) L']';
//				szBuffer[l+5] = (TCHAR) 0;
//           }
//           MessageBoxW(hWnd, szBuffer, NULL, MB_OK);
		 } else if (strncmp("Maze Width:", buf, 11) == 0) {  // Load Maze Width
			 buf = strstr(buf, ": ");
             buf = buf+2;
			 this->width=atoi(buf);
		  } else if (strncmp("Maze Height:", buf, 12) == 0) {  // Load Maze Height
			 buf = strstr(buf, ": ");
             buf = buf+2;
			 this->height=atoi(buf);
          } else if (strncmp("DATA:", buf, 5) == 0) {  // Start of data Block
              map = (mapInfo**) malloc((this->width*this->height)*sizeof(mapInfo*));
			  for (int i=0; i<(this->width*this->height); i++) {
                 map[i] = (mapInfo*) malloc(sizeof(mapInfo));
				 map[i]->x=-1;
				 map[i]->y=-1;
				 map[i]->inmaze = false;
				 map[i]->north = 0;
				 map[i]->west = 0;
				 map[i]->owner = 0;
				 map[i]->loctype = NORMAL;
 				 map[i]->val = 0;
				 map[i]->tile.img = NULL;
				 map[i]->tile.type = -1;
			  }
			  indata = true;
			  idata = 0;
			  x = 0;
			  y = 0;
		  } else if (strncmp(";", buf, 1) == 0) {  // End of Data Block
             indata = false;
 		  } else if (indata == true) {  // Load Data
			 char* buf2 = NULL;
			 //
			 // Advance the pointer to the next comma if there is one.
			 //
             buf2 = strtok_s(buf, ",", &buf);
			 while (buf2 != NULL) {
                l = strlen(buf2);
				char ch = buf2[0];
				int num = 0;
                //
				// Read the non whitespace characters
				//
				while (ch != (char) 0) {
					if ((ch != ' ') && (ch != '\t') && (ch != '\r') && (ch != '\n')) {
						if (idata < (this->width * this->height)) {
				           switch (ch) {
					          case 'i':
                                 map[idata]->inmaze = true;
  						         map[idata]->loctype = NORMAL; 
					             break;
					          case '.':
                                 map[idata]->inmaze = false;
 						         map[idata]->loctype = NORMAL; 
					             break;
					          case 'n':
                                 map[idata]->north = 1;
					             break;
					          case 'N':
                                 map[idata]->north = -1;
					             break;
					          case 'w':
                                 map[idata]->west = 1;
					             break;
					          case 'W':
                                 map[idata]->west = -1;
				        	     break;
					          case 'F':
						         map[idata]->loctype = FUEL; 
					             break;
					          case 'A':
                                 map[idata]->loctype = ARMOR; 
					             break;
					          case 'a':
                                 map[idata]->loctype = AMMO;
					             break;
					          case 'O':
                                 map[idata]->loctype = OUTPOST;
								 ch = *(buf2++); // Peek at next character
								 buf2--;
  								 num = chartonum(ch);
								 if (num != -1) {
 						            map[idata]->val = num;
								 } else {
  						            map[idata]->val = 0;
								 }
					             break;
					          case 'G':
                                 map[idata]->loctype = GOAL;
								 ch = *(buf2++); // Peek at next character
								 buf2--;
  								 num = chartonum(ch);
								 if (num != -1) {
 						            map[idata]->val = num;
								 } else {
  						            map[idata]->val = 0;
								 }
					             break;
					          case 'P':
                                 map[idata]->loctype = PEACE; 
					             break;
					          case '~':
                                 map[idata]->loctype = SLIP;
					             break;
					          case 'Z':
                                 map[idata]->loctype = SLOW;
					             break;
					          case 'X':
                                 map[idata]->loctype = START;
								 ch = *(buf2++); // Peek at next character
								 buf2--;
  								 num = chartonum(ch);
								 if (num != -1) {
 						            map[idata]->val = num;
								 } else {
  						            map[idata]->val = 0;
								 }
					             break;
					          case 'T':
                                 map[idata]->loctype = TELEPORT;
								 ch = *(buf2++); // Peek at next character
								 buf2--;
  								 num = chartonum(ch);
								 if (num != -1) {
 						            map[idata]->val = num;
								 } else {
  						            map[idata]->val = 0;
								 }
					             break;
					          case 'S':
 			  					 ch = *(buf2++); // Peek at next character
								 buf2--;
  								 num = chartonum(ch);
    					         switch (num) {
        					        case 1:
                                        map[idata]->loctype = SCROLL_SW;                       
								        break;
        					        case 2:
                                        map[idata]->loctype = SCROLL_S;                        
							        	break;
        					        case 3:
                                        map[idata]->loctype = SCROLL_SE;                         
							        	break;
        				        	case 4:
                                        map[idata]->loctype = SCROLL_W;                          
							        	break;
        				        	case 6:
                                        map[idata]->loctype = SCROLL_E;                          
						        		break;
        				        	case 7:
                                        map[idata]->loctype = SCROLL_NW;                         
							        	break;
        					        case 8:
                                        map[idata]->loctype = SCROLL_N;                           
								        break;
        					        case 9:
                                        map[idata]->loctype = SCROLL_NE;                        
								        break;
						         }
					             break;
					          case 't':
								 ch = *(buf2++); // Peek at next character
								 buf2--;
  								 num = chartonum(ch);
                                 map[idata]->owner = num; // Team
					             break;
				           }
						}
					}
					ch = *(buf2++);
				}
				idata++;
                buf2 = strtok_s(buf, ",", &buf);
			 }

			 if (idata < (this->width * this->height)) {
			    map[idata]->x = x;
			    map[idata]->y = y;
			 }
//		  } else { //Ignore the rest

		  }
          buf = fgets(buffer, 1024, file);
	   }
	}
    fclose(file);
	return 0;
}