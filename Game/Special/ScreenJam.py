#!/usr/bin/env python
# ScreenJam.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Special Equipment: ScreenJam.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import Game.Utility as util
import ogre.renderer.OGRE as ogre
from Game.Special.Base import Special

class ScreenJam(Special):
    '''Special Equipment: ScreenJam
        Launches a smokescreen and jams Radar over an area.
    
    Attributes:
        name       (string): name from Equipment tables
        tank       (Tank)  : tank mounting this Special
        active     (bool)  : on/off
        state      (any)   : custom state information
        name       (string): name for particle system
        posNode    (node)  : node for particle system

    Methods:
        toggle('on'|'off'|None): Toggle active state to on, off, or flip
                                On : Launches ScreenJam
                                Off: Deactivates jammer
        action                 : Countdown: timer to activate jammer
                                 Jamming  : jammer is active
        jam                     : disable radar and fog GunnerCam
        unjam                   : remove jam effects
    '''
    def __init__(self, tank): 
        super(Special, self).__init__()
        self.tank = tank
        self.name = tank.name + "_ScreenJam"
        self.active = False
        self.state = {}
        self.state['stowed'] = True
        self.state['position'] = None
        self.posNode = self.tank.sceneManager.getRootSceneNode().createChildSceneNode()
    
    def spawn(self):
        self.state['countdown'] = 0
        self.state['jamming'] = 0
        self.toggle('off')
        self.state['stowed'] = True
        
    def toggle(self, switch=None):
        if switch is 'on':
            self.active = True
        elif switch is 'off':
            self.active = False
        else:
            self.active = not self.active
        if self.active is True:
            if self.state['stowed']:
                self.state['countdown'] = 30
                self.state['position'] = self.tank.posNode.getPosition()
            elif self.state['jamming'] == 0:
                self.active = False
        else:
            if not self.state['stowed']:
                if self.state['jamming'] == 0:
                    self.tank.sceneManager.destroyParticleSystem(self.name +'_smoke')
                    self.state['stowed'] = False
                    self.active = False
                    for target in self.tank.parent.tankList:
                        if target.status['Jammed'] == self.state['position']:
                            self.unjam(target)
                else:
                    self.active = True

    def action(self):
        if self.state['countdown'] > 0:
            self.state['countdown'] -= 1
            if self.state['countdown'] == 0:
                self.state['stowed'] = False
                self.state['jamming'] = 300
                self.posNode.setPosition(self.state['position'])
                smokePos = self.posNode.getPosition() # to get a copy instead of a pointer
                smokePos.y -= 100
                self.posNode.setPosition(smokePos)
                smoke = self.tank.sceneManager.createParticleSystem(self.name +'_smoke', 'Arena/ScreenJam')
                self.posNode.attachObject(smoke)
        if self.state['jamming'] > 0:
            self.state['jamming'] -= 1
            if self.state['jamming'] == 0:
                self.toggle('off')
            else:
                for target in self.tank.parent.tankList:
                    bearing = self.state['position'] - target.posNode.getPosition()
                    distance = bearing.length()
                    if distance < 400:
                        self.jam(target, distance)
                    elif target.status['Jammed'] == self.state['position']:
                        self.unjam(target)
                        
    def jam(self,target, distance):
        target.status['Jammed'] = self.state['position']
        arena = self.tank.parent
        if target is arena.playerTank:
            if arena.cameraView == 'Gunner':
                self.posNode.setVisible(False)
                arena.sceneManager.setFog (ogre.FOG_LINEAR, (0,0,0), 0, 0, distance)
            else:
                self.posNode.setVisible(True)
                arena.sceneManager.setFog (ogre.FOG_NONE)
    
    def unjam(self,target):
        target.status['Jammed'] = None
        arena = self.tank.parent
        if target is arena.playerTank:
            arena.sceneManager.setFog (ogre.FOG_NONE)
    