#!/usr/bin/env python
# OverDrive.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Special Equipment: OverDrive.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

from Game.Special.Base import Special
import Game.Utility as util

class OverDrive(Special):
    '''Special Equipment: OverDrive
        Boosts speed and acceleration by burning more fuel and generating heat
    
    Attributes:
        name       (string): name from Equipment tables
        tank       (Tank)  : tank mounting this Special
        active     (bool)  : on/off
        state      (any)   : custom state information

    Methods:
        toggle('on'|'off'|None): Toggle active state to on, off, or flip
                                On : Boosts topspeed and acceleration
                                Off: Resets topspeed and acceleration
        action                 : Burns fuel and generates heat
                                When tank overheats, toggle off
    '''
    def __init__(self, tank): 
        super(Special, self).__init__()
        self.tank = tank
        self.active = False

    def spawn(self):
        pass
        
    def toggle(self, switch=None):
        if switch is 'on':
            self.active = True
        elif switch is 'off':
            self.active = False
        else:
            self.active = not self.active
        if self.active is True:
            self.tank.accel *= 3
            self.tank.maxSpeed *= 1.5
            self.tank.newSpeed = self.tank.maxSpeed
        else:
            self.tank.accel /= 3
            self.tank.maxSpeed /= 1.5
            self.tank.setSpeed(self.tank.newSpeed)
    
    def action(self):
        burn = (self.tank.speed**2/(float(self.tank.maxSpeed)/1.5)**2)
        self.tank.fuel = min(max(self.tank.fuel + -burn /10,0),9999)
        self.tank.heat += burn
        if self.tank.heat > 100:
            self.toggle('off')
