#!/usr/bin/env python
# Radar.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Special Equipment: Radar.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

from Game.Special.Base import Special
import Game.Equipment as equip

class Radar(Special):
    '''Special Equipment: Radar
        Radar functions are handled in Tank.scan() and Arena Dashboard update code
    
    Attributes:
        name       (string): name from Equipment tables
        tank       (Tank)  : tank mounting this Special
        active     (bool)  : on/off
        state      (any)   : custom state information

    Methods:
        toggle('on'|'off'|None): Toggle active state to on, off, or flip
    '''
    def __init__(self, tank): 
        super(Special, self).__init__()
        self.tank = tank
        (self.name, self.mass, self.space, self.cost) = equip.special("Radar")
        self.toggle('on')

    def spawn(self):
        pass
        
    def toggle(self, switch=None):
        tank = self.tank
        if switch is 'on':
            self.active = True
        elif switch is 'off':
            self.active = False
        else:
            self.active = not self.active
        if self.active is True:
            tank.scanRange *= 2
        else:
            tank.scanRange /= 2
    
    def action(self):
        pass
