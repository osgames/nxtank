#!/usr/bin/env python
# Sentry.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Special Equipment: Sentry.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import Game.Utility as util
import ogre.renderer.OGRE as ogre
from Game.Special.Base import Special
from Game.Tank import Tank

class Sentry(Special):
    '''Special Equipment: Sentry 
        Drops a sentry gun
        Tank must be stopped to deploy. Deployment takes time. 
    
    Attributes:
        name       (string): name from Equipment tables
        tank       (Tank)  : tank mounting this Special
        active     (bool)  : on/off
        state      (any)   : custom state information

    Methods:
        toggle('on'|'off'|None): Toggle active state to on, off, or flip
                                On : Stops tank and begins deployment
                                Off: Disables Sentry for pickup
        action                 : stowed : inactive
                                slowing : waiting to stop
                                setup   : countdown to spawn
                                deployed: spawn a Sentry
                                shutdown: waiting for pickup
                                pickup  : countdown to stowed
    '''
    def __init__(self, tank): 
        super(Special, self).__init__()
        self.tank = tank
        self.sceneManager = tank.parent.sceneManager
        self.active = False
        self.state = {}
        self.state['stowed'] = True
        self.state['setup'] = 0
        self.state['deployed'] = False
        self.state['shutdown'] = False
        self.state['pickup'] = 0
        self.sentry = None

    def spawn(self):
        pass
        
    def toggle(self, switch=None):
        if switch is 'on':
            self.active = True
        elif switch is 'off':
            self.active = False
        else:
            self.active = not self.active
        if self.active is True and self.state['stowed']:
            self.state['slowing'] = True
            self.tank.setSpeed(0)
        else:
            if self.state['setup'] > 0:
                self.state['setup'] = 0
                self.state['stowed'] = True
            elif self.state['deployed']:
                self.state['deployed'] = False
                self.state['shutdown'] = True
                self.active = True # I'm not dead yet
            elif self.state['pickup'] > 0:
                self.state['pickup'] = 0
                self.state['shutdown'] = True
                self.active = True # I'm not dead yet
                self.sentry.bot.active = False
                
    def action(self):
        tank = self.tank
        if tank.speed == 0:
            if self.state['slowing']:
                self.state['slowing'] = False
                self.state['stowed'] = False
                self.state['setup'] = 50
            elif self.state['setup'] > 0:
                self.state['setup'] -= 1
                if self.state['setup'] == 0:
                    if not self.sentry:
                        self.sentry = Tank(tank.parent, tank.name + "_Sentry", tank.team, 'sentry.tank')
                        from Bots.SentryBot import SentryBot
                        self.sentry.bot = SentryBot(self.sentry)
                        turret = self.sentry.turretList[1]
                        turret.posNode.detachObject(turret.ent)
                        self.sceneManager.destroyEntity(turret.name)
                        weapon = self.sentry.weapon[1]
                        weapon.posNode.setPosition((0,0,0))
                    self.sentry.bot.active = True
                    self.sentry.spawn(tank.posNode.getPosition())
                    self.state['deployed'] = True
                    tank.parent.tankList.append(self.sentry)
            elif self.state['shutdown']:# and tank in same square:
                self.state['shutdown'] = False
                self.state['pickup'] = 50
            elif self.state['pickup'] > 0:
                self.state['pickup'] -= 1
                if self.state['pickup'] == 0:
                    self.sentry.hide()
                    self.state['stowed'] = True
                    self.toggle('off')
                    tank.parent.tankList.remove(self.sentry)
        else:
            if self.state['setup'] > 0 or self.state['pickup'] > 0:
                self.toggle('off')

