#!/usr/bin/env python
# Special.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Special Equipment base class.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import Game.Equipment as equip

class Special(object):
    '''Special Equipment class
    This was the bane of Xtank. Here's where it gets tricksy...
    
    Attributes:
        name       (string): name from Equipment tables
        tank       (Tank)  : tank mounting this Special
        mass       (float) : mass
        space      (float) : space
        cost       (float) : cost
        active     (bool)  : on/off
        state      (any)   : custom state information

    Methods:
        toggle('on'|'off'|None): Toggle active state to on, off, or flip
        action                 : Per-tick effects when this Special is active
    '''
    def __init__(self, name): 
        self.active = False
        self.state = None

    def spawn(self):
        pass
        
    def toggle(self, switch=None):
        if switch is 'on':
            self.active = True
        elif switch is 'off':
            self.active = False
        else:
            self.active = not self.active
    
    def action(self):
        pass
