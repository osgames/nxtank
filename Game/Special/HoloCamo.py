#!/usr/bin/env python
# HoloCamo.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Special Equipment: HoloCamo.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import Game.Utility as util
import ogre.renderer.OGRE as ogre
from Game.Special.Base import Special

class HoloCamo(Special):
    '''Special Equipment: HoloCamo
        Hides tank from sight and Radar
        Requires tank to be stationary, shuts off Radar
    
    Attributes:
        name       (string): name from Equipment tables
        tank       (Tank)  : tank mounting this Special
        active     (bool)  : on/off
        state      (any)   : custom state information

    Methods:
        toggle('on'|'off'|None): Toggle active state to on, off, or flip
                                On : Stops tank
                                Off: Makes tank visible
        action                 : When tank stops, disable Radar and fade until 'Camo' status reached
                                When tank moves or fires, toggle off
    '''
    def __init__(self, tank): 
        super(Special, self).__init__()
        self.tank = tank
        self.active = False

    def spawn(self):
        self.toggle('off')
        
    def toggle(self, switch=None):
        tank = self.tank
        if switch is 'on':
            self.active = True
        elif switch is 'off':
            self.active = False
        else:
            self.active = not self.active
        if self.active is True:
            self.state = 'slowing'
            tank.setSpeed(0)
            tank.setMaterial(camo=True)
            tank.material.getTechnique(0).getPass(0).setSceneBlending(ogre.SBT_TRANSPARENT_ALPHA)
            tank.material.getTechnique(0).getPass(0).setDepthWriteEnabled(False)
        else:
            tank.material.getTechnique(0).getPass(0).setSceneBlending(ogre.SBT_REPLACE)
            tank.material.getTechnique(0).getPass(0).setDepthWriteEnabled(True)
            tank.setMaterial()
            tank.status['Camo'] = False
    
    def action(self):
        tank = self.tank
        if tank.newSpeed or True in tank.firing.values():
            self.toggle('off')
        elif tank.speed < 5:
            if tank.special['Radar'] and tank.special['Radar'].active:
                tank.special['Radar'].toggle('off')
            if self.state is 'slowing':
                self.state = 1
            elif self.state > 0:
                self.state = min(max(self.state + -0.02,0),1)
                red, green, blue = tank.teamColor[tank.team]
                tank.material.getTechnique(0).getPass(0).setDiffuse(red, green, blue, self.state) 
                if self.state == 0:
                    tank.status['Camo'] = True
                

        
