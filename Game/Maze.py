#!/usr/bin/env python
# Maze.py

# Copyright (C) 2008  Daniel McDaniel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Maze class.

Author: Daniel McDaniel snd Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import os
import fileinput
import ogre.renderer.OGRE as ogre
from Game.Wall import Wall
from random import random
from collections import namedtuple
MazeBox = namedtuple('MazeBox','x y north west owner inmaze type val') 

class Maze:
    '''Maze class
    Attributes:
        type       (string)            : Map Type
        name       (string)            : Maze Name
        author     (string)            : Author Name
        desc       (string)            : Description
        width      (int)               : Maze Width
        height     (int)               : Maze Height
        map        (list)              : MapInfo

    Methods:
        readMazeFile(path)             : load map into data structure
        getMazeType()                  : return maze type
        getMazeName()                  : return maze name
        getMazeAuthor()                : return author name
        getMazeDesc()                  : return maze desc
        getMazeWidth()                 : return maze width
        getMazeHeight()                : return maze height
        getMazeBox(i)                  : Get MazeBox number i
        getMazeBoxXY(x, y)             : Get MazeBox at x, y
        getWalls(x,y)                  : return E,S,W,N wall values
        wallBetween(a,b)               : return wall value between two boxes
        updateBox(x, y, side, owner)   : remove wall or change owner
        damageWall(wall, damage)       : roll D50 against damage && remove destructible wall
        interpretMazeData(value)       : Interprets loaded maze box data
        build()                        : creates all visible entities
        buildFloor()                   : creates floor entities
        buildWalls()                   : creates wall entities
        buildWall()                    : creates single wall
    '''
    def __init__(self, name, sceneManager):
        self.name = name
        self.type = None
        self.author = None
        self.desc = None
        self.width = 30
        self.height = 30
        self.sceneManager = sceneManager
        self.map = []
        self.wallList = []
        self.spawnBoxList = []
        self.landmark = {}
        self.landmark["ammo"] = []
        self.landmark["fuel"] = []
        self.landmark["repair"] = []
        self.landmark["start"] = []

    def readMazeFile(self, path):
        indata = False 
        widthNum = 0
        heightNum = 0
        mazeFile = "./mazes/" + path + "/" + self.name + ".maze"    # make this more portable later
        if not os.path.isfile(mazeFile): 
            return False
        for line in fileinput.input(mazeFile):
            if indata is False:
                if line == "\n":
                    print "Blank Line: []"
                else:
                    line = line.strip()
                    if line == "DATA:":
                        indata = True
                    else:
                        param = ""
                        value = ""
                        (param,value) = line.split(":",1)
                        param = param.strip()
                        value = value.strip()
                        if param == "Maze Type":
                            self.type = value
                        if param == "Maze Name":
                            self.name = value
                        if param == "Maze Designer":
                            self.author = value
                        if param == "Maze Desc":
                            self.desc = value
                        if param == "Maze Width":
                            self.width = int(value)
                        if param == "Maze Height":
                            self.height = int(value)
            else:
                if heightNum < self.height:
                    widthNum = 0
                    for value in line.split(","):
                        value = value.strip()
                        boxData = self.interpretMazeData(value, widthNum, heightNum)
                        self.map.append(boxData)
                        widthNum += 1
                    heightNum += 1
        for ypos in range(self.height):
            for xpos in range(self.width):
                mazeBox = self.getMazeBoxXY(xpos,ypos)
                if mazeBox.inmaze == True:
                    self.spawnBoxList.append(mazeBox)
                    if mazeBox.type == "AMMO":
                        self.landmark["ammo"].append((xpos,ypos))
                    elif mazeBox.type == "FUEL":
                        self.landmark["fuel"].append((xpos,ypos))
                    elif mazeBox.type == "ARMOR":
                        self.landmark["repair"].append((xpos,ypos))
                    elif mazeBox.type == "START":
                        self.landmark["start"].append((xpos,ypos))
        print self.spawnBoxList
        if self.landmark["start"]:
            self.spawnBoxList = self.landmark["start"]
        print self.spawnBoxList
        return True

    def getMazeType(self):
        return self.type

    def getMazeName(self):
        return self.name

    def getMazeAuthor(self):
        return self.author

    def getMazeDesc(self):
        return self.desc

    def getMazeWidth(self):
        return self.width

    def getMazeHeight(self):
        return self.height

    def getMazeBox(self, value):
        if (value >= 0) and (value < len(self.map)):
            [xpos,ypos,north,west,owner,inmaze,loctype,val] = self.map[value]
            return MazeBox(xpos,ypos,north,west,owner,inmaze,loctype,val)
        return None
 
    def getMazeBoxXY(self, x, y):
        if x >= 0 and x < self.width:
            if y >= 0 and y < self.height:
                return self.getMazeBox(y * self.width + x)
        return None
    
    def getNeighbors(self, x, y):
        result = []
        box = self.getMazeBoxXY(x,y)
        if not box:
            return None
        if box.west > -1:
            result.append( (x-1,y) )
        if box.north > -1:
            result.append( (x,y-1) )
        eBox = self.getMazeBoxXY(x+1,y)
        if eBox and eBox.west > -1:
            result.append( (x+1,y) )
        sBox = self.getMazeBoxXY(x,y+1)
        if sBox and sBox.north > -1:
            result.append( (x,y+1) )
        return result
    
    def getLandmarks(self, type):
        return self.landmark[type]

    def getWalls(self, x, y):
        north = -1
        west = -1
        south = -1
        east = -1
        box = self.getMazeBoxXY(x,y)
        if box:
            west, north = box.west, box.north
        eBox = self.getMazeBoxXY(x+1,y)
        if eBox:
            east = eBox.west
        sBox = self.getMazeBoxXY(x,y+1)
        if sBox:
            south = sBox.north
        return [east, south, west, north] # index * pi/2
    
    def wallBetween(self, a, b):
        ax, ay, wallAN, wallAW = a.x, a.y, a.north, a.west
        bx, by, wallBN, wallBW = b.z, b.y, b.north, b.west
        if ax == bx:
            if ay < by:
                return wallBN
            else:
                return wallAN
        else:
            if ax < bx:
                return wallBW
            else:
                return wallAW
            
    def updateBox(self, x, y, side=None, owner=None):
        index = y * self.width + x
        [xpos,ypos,north,west,oldOwner,inmaze,loctype,val] = self.map[index]
        if side == "N":
            north = 0
        elif side == "W":
            west = 0
        if not owner:
            owner = oldOwner
        self.map[index] = [xpos,ypos,north,west,owner,inmaze,loctype,val]
        
    def damageWall(self, wall, damage):
        if damage > random() * 50:
            self.sceneManager.destroyEntity(wall.miniMe.name)
            self.sceneManager.destroyEntity(wall.name)
            if wall in self.wallList:    # this shouldn't be necessary
                self.wallList.remove(wall.miniMe)
                self.wallList.remove(wall)
            self.updateBox(wall.x, wall.y, side=wall.side)

    def interpretMazeData(self, value, xpos, ypos):
        inmaze = False
        north = 0
        west = 0
        owner = 0
        loctype = "NORMAL"
        val = 0
        for num in range (0,len(value)):
            if value[num] == "i":
                inmaze = True
                loctype = "NORMAL"
            elif value[num] == ".":
                inmaze = False
                loctype = "NORMAL"
            elif value[num] == "n":
                north = 1
            elif value[num] == "N":
                north = -1
            elif value[num] == "w":
                west = 1
            elif value[num] == "W":
                west = -1
            elif value[num] == "F":
                loctype = "FUEL"
            elif value[num] == "A":
                loctype = "ARMOR"
            elif value[num] == "a":
                loctype = "AMMO"
            elif value[num] == "O":
                loctype = "OUTPOST"
                if len(value) > num+1 and value[num+1].isdigit():
                    val = int(value[num+1])
            elif value[num] == "G":
                loctype = "GOAL"
                if len(value) > num+1 and value[num+1].isdigit():
                    val = int(value[num+1])
            elif value[num] == "P":
                loctype = "PEACE"
            elif value[num] == "~":
                loctype = "SLIP"
            elif value[num] == "Z":
                loctype = "SLOW"
            elif value[num] == "X":
                loctype = "START"
                if len(value) > num+1 and value[num+1].isdigit():
                    val = int(value[num+1])
            elif value[num] == "T":
                loctype = "TELEPORT"
                if len(value) > num+1 and value[num+1].isdigit():
                    val = int(value[num+1])
            elif value[num] == "t":
                if len(value) > num+1 and value[num+1].isdigit():
                    owner = int(value[num+1])
            elif value[num] == "S":
                if len(value) > num+1 and value[num+1].isdigit():
                    if int(value[num+1]) == 1:
                        loctype = "SCROLL_SW"
                    if int(value[num+1]) == 2:
                        loctype = "SCROLL_S"
                    if int(value[num+1]) == 3:
                        loctype = "SCROLL_SE"
                    if int(value[num+1]) == 4:
                        loctype = "SCROLL_W"
                    if int(value[num+1]) == 6:
                        loctype = "SCROLL_E"
                    if int(value[num+1]) == 7:
                        loctype = "SCROLL_NW"
                    if int(value[num+1]) == 8:
                        loctype = "SCROLL_N"
                    if int(value[num+1]) == 9:
                        loctype = "SCROLL_NE"
#        print "[",xpos,ypos,north,west,owner,inmaze,loctype,val,"]"
        return [xpos,ypos,north,west,owner,inmaze,loctype,val]
    
    def build(self, physWorld=None, physSpace=None, minimap=None):
        self.buildFloor(physWorld, physSpace, minimap)
        self.buildWalls(physWorld, physSpace, minimap)
        return True

    def buildFloor(self, physWorld=None, physSpace=None, minimap=None):
        meshManager = ogre.MeshManager.getSingleton()
        plane = ogre.Plane ((0, 1, 0), 0)
        if not minimap:
            meshManager.createPlane ('Ground', 'General', plane, 19200, 19200, 100, 100, True, 1, 100, 100, (0, 0, 1))
            ent = self.sceneManager.createEntity('GroundEntity', 'Ground')
            #ent.setMaterialName ('Arena/Tile/OutsideTile')
            ent.setMaterialName ('Arena/Tile/FloorTile')
            ent.castShadows = False
            posNode = self.sceneManager.getRootSceneNode().createChildSceneNode('Ground', (float(self.width)/2*192,0.0,float(self.height)/2*192)).attachObject(ent)
        meshManager.createPlane ('FloorTile', 'General', plane, 192, 192, 1, 1, True, 1, 1, 1, (0, 0, 1))
        meshManager.createPlane ('Landmark', 'General', plane, 46, 42, 1, 1, True, 1, 1, 1, (0, 0, 1))
        for ypos in range(self.height):
            for xpos in range(self.width):
                mazeBox = self.getMazeBoxXY(xpos,ypos)
                if not minimap and not mazeBox.inmaze:
                    ent = self.sceneManager.createEntity("Floor_at_"+str(xpos)+"x"+str(ypos), "FloorTile")
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode("Floor_at_"+str(xpos)+"x"+str(ypos), (xpos * 192 + 96, 0.1, ypos * 192 + 96))
                    posNode.attachObject (ent)
                    posNode.yaw(ogre.Degree(180))
                    ent.castShadows = False
                    posNode.translate(0,30,0)
                    ent.setMaterialName ("Arena/Tile/BlockedTile")  
                materialName = None
                if mazeBox.type == "AMMO":
                    materialName = "Arena/Tile/AmmoDoor"
                    self.landmark["ammo"].append((xpos,ypos))
                elif mazeBox.type == "FUEL":
                    materialName = "Arena/Tile/FuelDoor"
                    self.landmark["fuel"].append((xpos,ypos))
                elif mazeBox.type == "ARMOR":
                    materialName = "Arena/Tile/ArmorDoor"
                    self.landmark["repair"].append((xpos,ypos))
                if materialName:
                    name = "landmark_at_"+str(xpos)+"x"+str(ypos)
                    type = "Landmark"
                    if minimap:
                        name = "Mini_" + name
                        type = "FloorTile"
                        materialName = materialName.replace("Door","Mini")
                    ent = self.sceneManager.createEntity(name, type)
                    ent.setMaterialName(materialName)  
                    ent.castShadows = False
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode(name, (xpos * 192 + 96, 0.2, ypos * 192 + 96))
                    posNode.attachObject (ent)
                    posNode.yaw(ogre.Degree(180))
                    if minimap:
                        posNode.translate(0,-10000,0)
        return True

    def buildWalls(self, physWorld=None, physSpace=None, minimap=None):
        self.buildWall(0, 0,"N", -3, self.width, physWorld, physSpace, minimap)
        self.buildWall(0, 0,"W", -3, self.height, physWorld, physSpace, minimap)
        self.buildWall(0, self.height,"N", -3, self.width, physWorld, physSpace, minimap)
        self.buildWall(self.width, 0,"W", -3, self.height, physWorld, physSpace, minimap)
        for ypos in range(self.height):
            start = 0
            stop = 0
            for xpos in range(self.width):
                mazeBox = self.getMazeBoxXY(xpos,ypos)
                if mazeBox.north == -1:
                    if not start:
                        start = xpos
                    stop = xpos
                elif start:
                    self.buildWall(start, ypos,"N", -1, stop-start+1, physWorld, physSpace, minimap)
                    start = 0
                    stop = 0
                if mazeBox.north and mazeBox.north != -1:
                    self.buildWall(xpos, ypos,"N", mazeBox.north, 1, physWorld, physSpace, minimap)                    
        for xpos in range(self.width):
            start = 0
            stop = 0
            for ypos in range(self.height):
                mazeBox = self.getMazeBoxXY(xpos,ypos)
                if mazeBox.west == -1:
                    if not start:
                        start = ypos
                    stop = ypos
                elif start:
                    self.buildWall(xpos, start,"W", -1, stop-start+1, physWorld, physSpace, minimap)
                    start = 0
                    stop = 0
                if mazeBox.west and mazeBox.west != -1:
                    self.buildWall(xpos, ypos,"W", mazeBox.west, 1, physWorld, physSpace, minimap)                    
        
        print len(self.wallList)
        return True
    
    def buildWall(self, xpos, ypos, side, strength=-1, length=1, physWorld=None, physSpace=None, minimap=None):
        if xpos == 0 and ypos > 0 and side == "W" or ypos == 0 and xpos > 0 and side == "N":
            return
        if strength > 0:
            type = "DEST"
        elif strength == -2:
            type = "HALF"
        elif strength == -3:
            type = "BORDER"
        else:
            type = "NORM"
        wall = Wall(self, self.sceneManager, xpos, ypos, side, type, length, physWorld, physSpace, minimap)
        self.wallList.append(wall)
        if minimap:
            for realWall in self.wallList:
                if realWall.name + "_mini" == wall.name:
                    realWall.miniMe = wall
                    break
        return True
