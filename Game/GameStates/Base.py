#!/usr/bin/env python
# Base.py

# Copyright (C) 2008  Colin McCulloch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This Module contains various base clases from which the game states
will inherit funtionality

Author: Colin "Zyzle" McCulloch

Last Edit By: "$Author$"
"""

__version__ = "$Revision$"
__date__ = "$Date$"

from math import *

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI

from Game.Core import GameManager

class AbstractGameState(object):
    """This abstractgamestate class will be inherited by all 
    game state classes as it defines the methods required for 
    them to function properly. All of these are empty methods
    with the exception of frameStarted and frameEnded which
    return True, to do otherwise would cause the game to shut
    down in the event that this method was never overriden"""
    def __init__(self):
        self.keyboard = GameManager().keyboard
        self.mouse = GameManager().mouse
    
    def enter(self):
        pass
    
    def exit(self):
        pass
    
    def pause(self):
        pass
    
    def resume(self):
        pass
    
    def keyPressed(self, evt):
        pass
    
    def keyReleased(self, evt):
        pass
    
    def frameStarted(self, evt):
        return True
    
    def frameEnded(self, evt):
        return True
    
    def mouseMoved(self, evt):
        pass
    
    def mousePressed(self, evt, buttonID):
        pass
    
    def mouseReleased(self, evt, buttonID):
        pass
            
    def convertButton(self, oisID):
        ''' Convert mouse buttons from OIS to CEGUI '''
        if oisID == "MB_Left":
            return CEGUI.LeftButton
        elif oisID == "MB_Right":
            return CEGUI.RightButton
        elif oisID == "MB_Middle":
            return CEGUI.MiddleButton
        else:
            return CEGUI.LeftButton

    def strBoundingBox(self,entity): 
        '''Returns a string of the min-max corners of an entity's Axis Aligned Bounding Box'''
        box = entity.getBoundingBox() 
        x = int(box.getMinimum().x)
        y = int(box.getMinimum().y)
        z = int(box.getMinimum().z)
        X = int(box.getMaximum().x)
        Y = int(box.getMaximum().y)
        Z = int(box.getMaximum().z)
        return str(x) +" "+ str(y) +" "+ str(z) +" "+ str(X) +" "+ str(Y) +" "+ str(Z)

    def worldBoundingBox(self,entity): 
        position = entity.parentSceneNode._getDerivedPosition()
        relbox = entity.getBoundingBox()
        minbox = relbox.getMinimum() + position
        maxbox = relbox.getMaximum() + position
        return ogre.AxisAlignedBox(minbox, maxbox)

    def updateStatistics(self):
        ''' Fills in debug stats '''
        statistics = self.renderWindow
        self.setGuiCaption('Arena/AverageFps', 'Average FPS: %d' % statistics.getAverageFPS())
        self.setGuiCaption('Arena/CurrFps', 'Current FPS: %d' % statistics.getLastFPS())
        self.setGuiCaption('Arena/BestFps',
                             'Best FPS: %d %d ms' % (statistics.getBestFPS(), statistics.getBestFrameTime()))
        self.setGuiCaption('Arena/WorstFps',
                             'Worst FPS: %d %d ms' % (statistics.getWorstFPS(), statistics.getWorstFrameTime()))
        self.setGuiCaption('Arena/NumTris', 'Triangle Count: %d' % statistics.getTriangleCount())
        self.setGuiCaption('Arena/DebugText', self.debugText)

    def setGuiCaption(self, elementName, text):
        element = ogre.OverlayManager.getSingleton().getOverlayElement(elementName, False)
        element.setCaption(text) 

    #def quit(self, evt):
    #    self.quit = True
    #    return True

