#!/usr/bin/env python
# Menu.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the nXtank Main Menu.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI
import ogre.sound.OgreAL as OgreAL
from collections import namedtuple

from Game.Core import GameManager
from Game.MetaClasses import Singleton
from Game.GameStates.Base import AbstractGameState

class MainMenu(AbstractGameState):
    '''MainMenu class
    '''
    __metaclass__ = Singleton
        
    def __init__(self):
        super(MainMenu, self).__init__()
        self.root = ogre.Root.getSingleton()
        self.soundManager = OgreAL.SoundManager()
        self.sceneManager = self.root.createSceneManager(ogre.ST_GENERIC)
        self.renderWindow = self.root.getAutoCreatedWindow()
        self.renderer = CEGUI.OgreCEGUIRenderer(self.renderWindow, ogre.RENDER_QUEUE_OVERLAY, False, 3000, self.sceneManager)
        self.system = CEGUI.System(self.renderer)
        CEGUI.SchemeManager.getSingleton().loadScheme("TaharezLookSkin.scheme")
        self.system.setDefaultMouseCursor("TaharezLook", "MouseTarget")
        self.system.setDefaultFont("BlueHighway-12")
        CEGUI.ImagesetManager.getSingleton().createImageset("HELP.imageset")
        CEGUI.ImagesetManager.getSingleton().createImageset("Chrome.imageset")
        CEGUI.ImagesetManager.getSingleton().createImageset("HUD_icons.imageset")
        self.windowManager = CEGUI.WindowManager.getSingleton()
        self.gui = self.windowManager.loadWindowLayout("MainMenu.layout")
        DebugTuple = namedtuple('DebugTuple','materials physics particles AI') 
        self.debugging = DebugTuple(materials=False, physics=False, particles=False, AI=False)
    
    def enter(self):
        self.system.setGUISheet(self.gui)
        self.credits = self.windowManager.getWindow( "MainMenu/Credits")
        self.credits.hide()
        self.showHelp = False

        # subscribe events for clicked buttons
        instantButton = self.windowManager.getWindow("MainMenu/InstantButton")
        instantButton.subscribeEvent(CEGUI.PushButton.EventClicked, self, "instantLaunch")
        arenaButton = self.windowManager.getWindow("MainMenu/ArenaButton")
        arenaButton.subscribeEvent(CEGUI.PushButton.EventClicked, self, "arenaLaunch")
        tankDesignButton = self.windowManager.getWindow("MainMenu/TankDesignButton")
        tankDesignButton.subscribeEvent(CEGUI.PushButton.EventClicked, self, "tankDesignLaunch")
        mazeDesignButton = self.windowManager.getWindow("MainMenu/MazeDesignButton")
        mazeDesignButton.subscribeEvent(CEGUI.PushButton.EventClicked, self, "mazeDesignLaunch")
        creditsButton = self.windowManager.getWindow("MainMenu/CreditsButton")
        creditsButton.subscribeEvent(CEGUI.PushButton.EventClicked, self, "creditsLaunch")
        quitButton = self.windowManager.getWindow("MainMenu/QuitButton")
        quitButton.subscribeEvent(CEGUI.PushButton.EventClicked, self, "quitLaunch")

        self.camera = self.sceneManager.createCamera("Camera")
        self.viewport = self.renderWindow.addViewport(self.camera)

        self.quit = False

        print "Entered MainMenu."
        
    def exit(self):
        self.sceneManager.clearScene()
        self.sceneManager.destroyAllCameras()
        self.renderWindow.removeAllViewports()
        
    def resume(self):
        self.viewport.setBackgroundColour(ogre.ColourValue(0.0, 0.0, 1.0))
        
    def frameStarted(self, evt):
        if self.quit:
            return False
        return True

    def frameEnded(self, evt):
        return True

    def keyPressed(self, evt):
        """Handle key pressed events, again note the last minute
        calling of state imports, again this was to prevent import
        loop problems"""
        # CEGUI injection
        ceguiSystem = CEGUI.System.getSingleton()
        ceguiSystem.injectKeyDown(evt.key)
        ceguiSystem.injectChar(evt.text)
        if evt.key == OIS.KC_ESCAPE:
            self.quit = True

    def keyReleased(self, evt):
        try:
            key = evt.key
            CEGUI.System.getSingleton().injectKeyUp(evt.key)
        except:
            pass

    def mousePressed(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonDown(self.convertButton(id))

    def mouseReleased(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonUp(self.convertButton(id))

    def mouseMoved(self, evt):
        state = evt.get_state()
        CEGUI.System.getSingleton().injectMouseMove(evt.get_state().X.rel, evt.get_state().Y.rel)

    def mouseDragged(self, evt):
        pass

    def convertButton(self, oisID):
        ''' Convert mouse buttons from OIS to CEGUI '''
        if oisID == "MB_Left":
            return CEGUI.LeftButton
        elif oisID == "MB_Right":
            return CEGUI.RightButton
        elif oisID == "MB_Middle":
            return CEGUI.MiddleButton
        else:
            return CEGUI.LeftButton

    def instantLaunch(self, evt):
        from Game.GameStates.Arena import Arena
        GameManager().changeState(Arena(self.renderer, self.sceneManager, self.soundManager, "Demo", self.debugging))

    def arenaLaunch(self, evt):
        # this will go to an Arena settings window prior to launch
        from Game.GameStates.Arena import Arena
        GameManager().changeState(Arena(self.renderer, self.sceneManager, self.soundManager, "Team Combat", self.debugging))

    def tankDesignLaunch(self, evt):
        from Game.GameStates.TankDesign import TankDesign
        GameManager().changeState(TankDesign(self.renderer, self.sceneManager, self.soundManager, self.debugging))

    def mazeDesignLaunch(self, evt):
        from Game.GameStates.MazeDesign import MazeDesign
        GameManager().changeState(MazeDesign(self.renderer, self.sceneManager, self.soundManager, self.debugging))
    
    def creditsLaunch(self, evt):
        if self.showHelp:
            self.credits.hide()
        else:                
            self.credits.show()
        self.showHelp = not self.showHelp
    
    def quitLaunch(self, evt):
        self.quit = True
        return True

