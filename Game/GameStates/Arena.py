#!/usr/bin/env python
# Arena.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the nXtank Arena.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

from random import choice, random
from math import pi, copysign, sin, cos, atan2
from time import sleep
#import timeit
#import gc
#from sys import getrefcount

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI
import ogre.physics.OgreOde as OgreOde

import Game.Utility as util
from Game.Core import GameManager
from Game.MetaClasses import Singleton
from Game.GameStates.Base import AbstractGameState
from Game.Tank import Tank
from Game.Maze import Maze
from Game.Jukebox import SFX
from Game.Listeners import GameCollisionListener
#from Game.Bots.MOITest import MOITest
from Game.Bots.TankBot import TankBot

class Arena(AbstractGameState):
    '''This is the nXtank Arena.

    Attributes:
        root           (singleton): root singleton
        sceneManager   (sceneMgr) : Scene manager
        renderWindow   (window)   : SceneManager render window
        soundManager   (soundMgr) : Sound Manager
        sfx            (sound)    : Sound effect object
        
        renderer       (singleton): CEGUI renderer
        ceguiSystem    (singleton): CEGUI system singleton
        windowManager  (singleton): CEGUI window manager
        
        dashboard      (sheet)    : Dashboard CEGUI sheet
        dashConsole    (window)   : Dashboard Console window
        dashRadar      (window)   : Dashboard Radar window
        dashArmor      (dict)     : Dashboard Armor windows
        dashPlayer     (window)   : Dashboard Player window
        dashVehicle    (window)   : Dashboard Vehicle window
        dashSpecial    (dict)     : Dashboard Specials windows
        dashWeapon     (dict)     : Dashboard Weapons windows
        dashIconSet    (imageset) : Icon image set
        dashNumSet     (imageset) : NumberLights image set
        dashCompassSet (imageset) : Compass image set
        dashBlipSet    (imageset) : Blip image set
        dashBarSet     (imageset) : ColorBar image set
        dashBarVSet    (imageset) : ColorBar vertical image set
        dashArmorSet   (imageset) : Armor image set
        dashMapSet     (imageset) : Map image set
        dashBlips      (list)     : Blip overlaid on radar display
        dashCompass    (window)   : Compass overlaid on radar display
        dashScore      (window)   : Dashboard Score window
        dashKills      (window)   : Dashboard Kills window
        dashDeaths     (window)   : Dashboard Deaths window
        dashFuel       (window)   : Dashboard Fuel window
        dashHeat       (window)   : Dashboard Heat window
        dashFPS        (window)   : Dashboard FPS window
        dashMessage    (window)   : Dashboard Message window
        dashHelp       (window)   : Dashboard Help window
        dashMap        (window)   : Dashboard Minimap window
        playerChanged  (bool)     : flag to update GUI
        guiFlash       (bool)     : toggle flashing GUI elements
        
        toolTip        (tooltip)  : CEGUI tooltip
                
        statsOverlay   (sheet)    : debug stats overlay sheet

        tankList       (list)     : list of combatants
        playerTank     (Tank)     : Player tank object
        entity         (dict)     : entity names -> objects for collision tracking
        spawnBoxList   (list)     : list of spawnable mazeBoxes
        
        OHCamera       (camera)   : Overhead camera
        OHCamNode      (node)     : Position of OHcamera
        OHPitchNode    (node)     : Pitch of OHcamera
        cameraView     (string)   : main window camera view mode
        gunCamera      (camera)   : Gunner camera
        viewport       (viewport) : OH viewport
        viewHeight     (float)    : Height of viewport in pixels
        viewWidth      (float)    : Width of viewport in pixels
        OHXoffset      (float)    : Pixels to offset center of OHcamera
        mapCamera      (camera)   : Minimap camera
        mapCamNode     (node)     : Minimap camera node
        mapCamY        (float)    : position of minimap camera
        mapTexture     (texture)  : RenderTexture containing minimap image
        
        quit           (bool)     : Trigger self.exit()
        timeStandStill (bool)     : Pause without menu
        
        physTicksPerSec(float)    : Physics ticks per second
        gameTimerReset (float)    : Timer for game ticks
        gameTimer      (float)    : Countdown for next game tick
        gameTicks      (float)    : Counter for game ticks
        keyTimerReset  (float)    : Timer for repeatable keys
        keyTimer       (float)    : Countdown for next key repeat
        keyDown        (dict)     : Dict of keyPressed events
        mouseTimerReset(float)    : Timer for repeatable mouse buttons
        mouseTimer     (float)    : Countdown for next mouse repeat
        mouseDown      (dict)     : Dict of mousePressed toggles
        dashTimerReset (float)    : Timer for Dashboard updates
        dashTimer      (float)    : Countdown for next Dashboard update
        demoTimerReset (float)    : Timer for demo mode camera shift
        demoTimer      (float)    : Countdown for next camera shift
        botTimerReset  (float)    : Timer for AI ticks
        botTimer       (float)    : Countdown for next AI tick
        debugTimerReset(float)    : Timer for refreshing debug overlay
        debugTimer     (float)    : Countdown to next debug refresh
        debugText      (str)      : Debug text console output
        ConsoleText    (str)      : Dashboard console text output
        FPSthrottle    (float)    : set frame delay for testing
        
        showDebug      (bool)     : Toggle debug overlay
        showHelp       (bool)     : Toggle Help window
        sfxMute        (bool)     : Toggle sound
        pretty         (bool)     : Toggle visual quality

        gameType       (str)      { Game type
        shotBatch      (int)      : number of shots to batch into one projectile
        shotScale      (int)      : shot.posNode scaling for OHcam visibility
        
        collTanks      (list)     : tank-tank collisions
        collShotTank   (list)     : shot-tank collisions
        collShotWall   (list)     : shot-wall collisions
        collTankWall   (list)     : tank-wall collisions

        physWorld        (world)   : ODE world
        physSpace        (space)   : ODE space
        collisionListener(listener): ODE collision listener
        physStepper      (stepper) : ODE stepper
        raySceneQuery    (RSQ)     : LOS query

    Methods:
        enter                 : Enter the Arena
        exit                  : Leave the arena
        framestarted          : Per-frame game logic updates
        keyPressed/keyReleased: Player keyboard controls
        keyRepeat             : Repeatable key commands
        mousePressed/Released/
            Moved/Dragged     : Player mouse controls
        statuscolor(status)   : Returns red,green = CEGUI(red..yellow..green) for (0<= status <=1)
        dashTextColor
            (window, status)  : Uses status value to set color of text in window
        dashBarColor
            (window, status)  : Uses status value to set color of image in window
        dashBarImage
            (window, status, style='green'): Uses status value to set image in window
        spawnTank(tank)       : Places tank on valid maze location
        setCamera(string)     : Switches viewport camera: "OH", "Gunner" or "toggle"
    '''

    __metaclass__ = Singleton
        
    def __init__(self, renderer, sceneManager, soundManager, gameType, debugging):
        super(Arena, self).__init__()
        self.root = ogre.Root.getSingleton()
        self.soundManager = soundManager
        self.renderer = renderer
        self.debugging = debugging
        self.ceguiSystem = CEGUI.System.getSingleton()
        self.sceneManager = sceneManager
        self.sfx = SFX(self.soundManager) # could inherit this also
        self.timeStandStill = False
        self.gameTimerReset = 1.0/15
        self.gameTimer = self.gameTimerReset
        self.gameTicks = 0
        self.demoTimerReset = 10
        self.demoTimer = 0
        self.botTimerReset = 3.0/15
        self.botTimer = 0
        self.dashTimerReset = 5.0/15
        self.dashTimer = 0
        self.debugTimerReset = 1.0/15
        self.debugTimer = 0
        self.debugText = 0
        self.consoleText = ""
        self.keyTimerReset = self.gameTimerReset
        self.keyTimer = 0
        self.mouseTimerReset = self.gameTimerReset
        self.mouseTimer = 0
        self.physTicksPerSec = 60
        self.physTimerReset = 1.0 / self.physTicksPerSec
        self.physTimer = 0
        self.FPSthrottle = 0 # 1.0/15
        self.showDebug = False
        self.showHelp = False
        self.sfxMute = True
        self.pretty = False
        self.keyDown = {}
        self.mouseDown = {}
        self.dashBlips = []
        self.gameType = gameType
        self.dashArmor = {}
        self.dashSpecial = {}
        self.dashWeapon = {}
        self.shotBatch = 3
        self.shotScale = 1.75
        self.collTanks = []
        self.collShotTank = []
        self.collShotWall = []
        self.collTankWall = []
        # CEGUI
        self.renderWindow   = self.root.getAutoCreatedWindow() 
        self.windowManager  = CEGUI.WindowManager.getSingleton()
        self.dashIconSet    = CEGUI.ImagesetManager.getSingleton().getImageset("HUD_icons")
        self.dashNumSet     = CEGUI.ImagesetManager.getSingleton().createImageset("NumberLights.imageset")
        self.dashCompassSet = CEGUI.ImagesetManager.getSingleton().createImageset("Compass.imageset")
        self.dashBlipSet    = CEGUI.ImagesetManager.getSingleton().createImageset("Blip.imageset")
        self.dashBarSet     = CEGUI.ImagesetManager.getSingleton().createImageset("ColorBars.imageset")
        self.dashBarVSet    = CEGUI.ImagesetManager.getSingleton().createImageset("ColorBarsVertical.imageset")
        self.dashArmorSet   = CEGUI.ImagesetManager.getSingleton().createImageset("ArmorDiagram.imageset")
        self.dashboard = CEGUI.WindowManager.getSingleton().loadWindowLayout("Dashboard.layout")           
        self.mapTexture = ogre.TextureManager.getSingleton().createManual( "RttTex", "General", ogre.TextureType.TEX_TYPE_2D, 512, 512, 0, ogre.PixelFormat.PF_R8G8B8, ogre.TU_RENDERTARGET ).getBuffer().getRenderTarget()
        self.mapTexture.setAutoUpdated(False)
        ceguiTexture = self.renderer.createTexture("RttTex") 
        self.dashMapSet = CEGUI.ImagesetManager.getSingleton().createImageset("MapImageset", ceguiTexture)
        self.dashMapSet.defineImage("MapImage", CEGUI.Rect(0.0, 0.0, ceguiTexture.getWidth(), ceguiTexture.getHeight()), CEGUI.Vector2(0.0, 0.0)) 
        # Setup Tooltips
        self.ceguiSystem.setDefaultTooltip("TaharezLook/Tooltip")
        self.toolTip = self.ceguiSystem.getDefaultTooltip()
        #tip.displayTime=0.80
        self.toolTip.hoverTime = 0.5
        self.toolTip.fadeTime = 0.2
        
    def enter(self):
        self.quit = False
        # CEGUI
        self.ceguiSystem.setGUISheet(self.dashboard)
        self.dashRadar      = self.windowManager.getWindow( "Dashboard/Radar")
        self.dashMap        = self.windowManager.getWindow( "Dashboard/Radar/Map")
        self.dashCompass    = self.windowManager.getWindow( "Dashboard/Radar/Compass")
        self.dashArmor['F'] = self.windowManager.getWindow( "Dashboard/Console/Armor/F")
        self.dashArmor['A'] = self.windowManager.getWindow( "Dashboard/Console/Armor/A")
        self.dashArmor['P'] = self.windowManager.getWindow( "Dashboard/Console/Armor/P")
        self.dashArmor['S'] = self.windowManager.getWindow( "Dashboard/Console/Armor/S")
        self.dashArmor['T'] = self.windowManager.getWindow( "Dashboard/Console/Armor/T")
        self.dashArmor['B'] = self.windowManager.getWindow( "Dashboard/Console/Armor/B")
        self.dashScore      = self.windowManager.getWindow( "Dashboard/Console/Player/Score")
        self.dashKills      = self.windowManager.getWindow( "Dashboard/Console/Player/Kills")
        self.dashDeaths     = self.windowManager.getWindow( "Dashboard/Console/Player/Deaths")
        self.dashFuel       = self.windowManager.getWindow( "Dashboard/Console/Vehicle/Fuel")
        self.dashSpeed      = self.windowManager.getWindow( "Dashboard/Console/Vehicle/Speed")
        self.dashHeat       = self.windowManager.getWindow( "Dashboard/Weapons/Heat")
        self.dashFPS        = self.windowManager.getWindow( "Dashboard/FPS")
        self.dashMessage    = self.windowManager.getWindow( "Dashboard/Message")
        self.dashHelp       = self.windowManager.getWindow( "Dashboard/Help")
        self.dashHelp.hide()
        self.dashMap.setProperty("Image", CEGUI.PropertyHelper.imageToString(self.dashMapSet.getImage('MapImage')))
        for special in range(7,11):
            for field in ('Number','Special'):
                self.dashSpecial[special, field] = self.windowManager.getWindow\
                    ( "Dashboard/Console/Specials/" + str(special) +"/"+ str(field))
        for weapon in range(1,7):
            for field in ('Number','Weapon','Ammo','Mount'):
                self.dashWeapon[weapon,field] = self.windowManager.getWindow( "Dashboard/Weapons/" + str(weapon) +"/"+ str(field))
            self.dashWeapon[weapon, 'Mount'].setProperty("TextColours", \
                            CEGUI.PropertyHelper.colourToString(CEGUI.colour(0.4,0.4,0.4)))
        self.playerChanged = True
        self.guiFlash = True
        # debug stats overlay
        #self.statsOverlay = ogre.OverlayManager.getSingleton().getByName('Arena/StatsOverlay')
        #self.statsOverlay.hide()
        # Physics
        self.physWorld = OgreOde.World(self.sceneManager)
        self.physWorld.setGravity(ogre.Vector3(0,-9.80665,0)) 
        self.physWorld.setCFM(10e-5)
        self.physWorld.setERP(0.8)
        self.physWorld.setAutoSleep(True)
        self.physWorld.setAutoSleepAverageSamplesCount(10)
        self.physWorld.setContactCorrectionVelocity(1.0)
        self.physSpace = self.physWorld.getDefaultSpace()
        self.collisionListener = GameCollisionListener(self.physWorld, self)
        timeStep = 1.0 / self.physTicksPerSec
        timeScale = 1.0
        maxFrameTime = 1.0/10
        self.physStepper = OgreOde.StepHandler(self.physWorld, OgreOde.StepHandler.QuickStep, 
                                           timeStep, maxFrameTime, timeScale)
        if self.debugging.physics:
            self.physWorld.setShowDebugGeometries(True)
        self.raySceneQuery = self.sceneManager.createRayQuery(ogre.Ray())
        self.raySceneQuery.setSortByDistance(True)
        # init map
        self.maze = Maze("IronCross", self.sceneManager)
        #self.maze = Maze("City", self.sceneManager)
        if not self.maze.readMazeFile("combat"):
            print "MAZE NOT LOADED"
        self.maze.build(self.physWorld, self.physSpace)
        self.physPlane = OgreOde.InfinitePlaneGeometry(ogre.Plane(ogre.Vector3(0,1,0),0),self.physWorld, self.physSpace)
        # light
        light = self.sceneManager.createLight ('DirectionalLight')
        light.type = ogre.Light.LT_DIRECTIONAL
        light.diffuseColour = (1, 1, 1)
        light.specularColour = (0.75, 0.75, 0.75)
        light.direction = (1, -1, 1)
        self.sceneManager.ambientLight = (0.2, 0.2, 0.2)
        # sky
        self.sceneManager.setSkyDome (True, "Arena/Sky/OrionSky", 2, 4)
        # shadows
        #self.sceneManager.shadowTechnique = ogre.SHADOWTYPE_STENCIL_ADDITIVE #ogre.SHADOWTYPE_NONE
        self.sceneManager.shadowTechnique = ogre.SHADOWTYPE_NONE
        # init tanks
        self.tankList = []
        tank = Tank(self, "F1 for HELP", 1, "testplayer.tank", self.debugging, self.physWorld, self.physSpace)
        self.playerTank = None
        self.spawnTank(tank)
        self.playerTank = tank
        self.playerTank.bot = TankBot(self.playerTank, self.maze)
        if self.gameType == "Demo":
            self.playerTank.bot.active = True
        else:
            self.playerTank.bot.active = False
        self.tankList.append(tank)
        for tankNum in range(2,13):
            tankfile = "testarena" +str(int(random()*6)) +".tank"
            tank = Tank(self, "Player" + str(tankNum), int(float(tankNum)/2), tankfile, self.debugging, self.physWorld, self.physSpace)
            self.spawnTank(tank)
            tank.bot = TankBot(tank, self.maze)
            tank.bot.active = True
            self.tankList.append(tank)
        tankMass = []
        for tank in self.tankList:
            tankMass.append(tank.mass)
        maxMass = float(max(tankMass)) / 10 # this should actually be scaled from 0.1 to 10
        for tank in self.tankList:
            tank.setMass(tank.mass / maxMass)
        # create the overhead camera node/pitch node
        self.OHCamera = self.sceneManager.createCamera("OHCamera")
        self.OHCamNode = self.playerTank.posNode.createChildSceneNode("OHCamNode")
        self.OHCamNode.setInheritOrientation(False)
        self.OHCamPitchNode = self.OHCamNode.createChildSceneNode("OHPitchNode")
        self.OHCamPitchNode.attachObject(self.OHCamera)
        self.OHCamera.nearClipDistance = 5
        # create the gunner camera
        self.gunCamera = self.sceneManager.createCamera("gunCamera")
        self.playerTank.gunCamNode.attachObject(self.gunCamera)
        self.gunCamera.nearClipDistance = 5
        position = self.playerTank.posNode.getPosition()
        position.x += 100
        self.gunCamera.lookAt(position)
        # viewport
        self.viewport = self.renderWindow.addViewport(self.OHCamera)
        self.viewHeight = self.viewport.getActualHeight()
        self.viewWidth = self.viewport.getActualWidth()      
        self.OHXoffset = 0 #(self.viewWidth - self.viewHeight)/4 # assuming width >= height
        self.OHCamNode.setPosition(self.OHXoffset, 900, 1)
        position = self.playerTank.posNode.getPosition()
        position.x += self.OHXoffset
        self.OHCamera.lookAt(position)
        if self.gameType == "Demo":
            self.setCamera("Gunner")
        else:
            self.setCamera("OH")
        # Minimap
        self.maze.build(minimap=True)
        self.mapCamera = self.sceneManager.createCamera("MapCamera")
        self.mapCamNode = self.sceneManager.getRootSceneNode().createChildSceneNode("MapCamNode")
        mapCamPitchNode = self.mapCamNode.createChildSceneNode("MapPitchNode")
        mapCamPitchNode.attachObject(self.mapCamera)
        self.mapCamNode.setPosition(0,1000,0)
        self.mapCamera.setAspectRatio(1)
        self.mapCamera.nearClipDistance = 5
        self.mapCamera.lookAt(0,0,-1)
        viewport = self.mapTexture.addViewport(self.mapCamera)
        viewport.setOverlaysEnabled(False)
        viewport.setClearEveryFrame(True)
        viewport.setBackgroundColour(ogre.ColourValue().Black)
        self.mapCamY = 3400 - 10000
        self.mapCamLR = True
        
        print "Entered Arena."
        
        self.spawndist = 0
        self.spawnpoint = self.playerTank.posNode._getDerivedPosition()
        self.spawnpoint.y = 0
        self.drivedist = 0
        self.driveratio = 0
        self.unstickFlag = False
        
    def exit(self):
        #from sys import getrefcount
        #import gc
        del self.maze
        for tank in self.tankList:
            tank.destroy()
        del tank
        del self.tankList
        del self.playerTank
        self.sceneManager.clearScene()
        self.sceneManager.destroyAllCameras()
        self.mapTexture.removeAllViewports()
        self.renderWindow.removeAllViewports()
        self.physWorld.clearContacts()
        del self.collisionListener
        del self.physStepper
        del self.physPlane
        del self.physSpace
        #gc.collect()
        #print getrefcount(self.physWorld)
        #print gc.get_referrers(self.physWorld)        
        del self.physWorld
        print "Exited Arena"
        
    def frameStarted(self, evt):
        ''' All the action happens here '''
        time = evt.timeSinceLastFrame
        self.ceguiSystem.getSingleton().injectTimePulse(time)
        if self.timeStandStill:
            return True
        if time < self.FPSthrottle:
            sleep(self.FPSthrottle - time)
            time = self.FPSthrottle
        if self.unstickFlag:
            self.unstick(self.playerTank)
            self.unstickFlag = False

        player = self.playerTank
        if player.debug:
            self.debugText = str(player.debug)
        #
        #if 0 < player.bugDriven < 10000:
        #    spawnDist = int((player.bugPosition - player.posNode._getDerivedPosition()).squaredLength())
        #    driveratio = spawnDist / player.bugDriven #**2
        #    self.debugText = str(spawnDist) +" / "+ str(player.bugDriven) +" = "+ str(driveratio)
        #self.debugText = str(player.speed)+" = "+str(player.bugSpeed)

        # timeit.Timer('doStuff', 'import stuff; stuff').timeit(1000)
        #time1 = timeit.Timer('junk = util.diffAngle(ogre.Vector3(1,0,0),vector)', 'import ogre.renderer.OGRE as ogre; import Game.Utility as util; vector = ogre.Vector3(2,0,5)').timeit(10000)
        #time2 = timeit.Timer('junk = util.vectorToAngle(vector)', 'import ogre.renderer.OGRE as ogre; import Game.Utility as util; vector = ogre.Vector3(2,0,5)').timeit(10000)
        #self.debugText = str(time2 / time1)
        
        # Shift cameras for Demo mode
        self.demoTimer += time
        if self.gameType == "Demo" and self.demoTimer >= self.demoTimerReset:
            self.demoTimer -= self.demoTimerReset
            self.playerTank.posNode.removeChild(self.OHCamNode)
            self.playerTank.gunCamNode.detachObject(self.gunCamera)
            self.playerTank = choice(self.tankList)
            self.playerTank.posNode.addChild(self.OHCamNode)
            self.playerTank.gunCamNode.attachObject(self.gunCamera)
            self.playerChanged = True
        # Animate every frame
        for tank in self.tankList:
            tank.animate(time, self.shotBatch)
        # Physics
        self.physTimer += time
        while self.physTimer >= self.physTimerReset:
            self.physTimer -= self.physTimerReset
            if self.physStepper.step(self.physTimerReset):
                self.physWorld.synchronise()
            # Drive in physics time
            for tank in self.tankList:
                for weapon in tank.weapon[1:]:
                    if weapon.particle:
                        size = self.shotScale * weapon.particleSize
                        weapon.particle.setDefaultDimensions(size,size)
                for shot in tank.shotList:
                    shot.move(self.physTimerReset)
                    shot.posNode.setScale(ogre.Vector3(self.shotScale,self.shotScale,self.shotScale))
                if tank.respawn:
                    self.spawnTank(tank)
                if tank.alive: 
                    tank.move(self.physTimerReset)
            # stabilize GunnerCam
            self.playerTank.turretList[0].posNode.yaw(-self.playerTank.turnRate * self.physTimerReset)
        # Collision resolution
        while len(self.collTanks):
            tank1 = self.collTanks.pop(0)
            tank2 = self.collTanks.pop(0)
            # Tank-Tank collisions go here
            del tank1, tank2
        while len(self.collShotTank):
            shot = self.collShotTank.pop(0)
            tank = self.collShotTank.pop(0)
            shooter = shot.tank
            if tank.team is shooter.team and tank.team is not 0: # Team Deathmatch
                pass
            elif shot.alive and not tank.burning:
                vector = shot.posNode.getPosition() - tank.posNode.getPosition()
                hitAngle = atan2(vector.z,vector.x) - tank.direction
                tank.damage(shot.damage, hitAngle, shooter, shot.batch, shot.type)
                shot.hide()
                if not tank.alive:
                    shot.tank.kills += 1
                    shot.tank.score += tank.cost
                    tank.deaths += 1
                    tank.score -= tank.cost
                    if not self.sfxMute:
                        self.sfx.shotFired("explosion.ogg", tank.posNode.getPosition())
            del shot, tank, shooter
        while len(self.collShotWall):
            shot = self.collShotWall.pop(0)
            wall = self.collShotWall.pop(0)
            # Half walls go here?
            if wall.type == "DEST":
                self.maze.damageWall(wall, shot.damage)
            if shot.alive:
                shot.hide()
            del shot, wall
        while len(self.collTankWall):
            tank = self.collTankWall.pop(0)
            wall = self.collTankWall.pop(0)
            # Destructible walls go here
            # Tank-Wall collisions go here
            del tank, wall
        # these should already be empty
        self.collTanks = []
        self.collShotTank = []
        self.collShotWall = []
        self.collTankWall = []
        # move turrets every frame
        for tank in self.tankList:
            if tank.alive: 
                tank.moveTurrets(time)
        # Tick-based actions
        self.gameTimer += time
        while self.gameTimer >= self.gameTimerReset:
            self.gameTimer -= self.gameTimerReset
            self.gameTicks += 1
            for tank in self.tankList:
                tank.update()
                if not tank.alive and not tank.burning:
                    self.spawnTank(tank)
                # Update Compass on gametimer rather than dashtimer
                if self.cameraView == "OH":
                    heading = self.playerTank.direction
                else:
                    heading = self.playerTank.turretList[0].newAim
                headingSign = copysign(1,heading)
                self.dashCompass.setProperty("Image", 
                        CEGUI.PropertyHelper.imageToString(self.dashCompassSet.getImage(str(int(heading/pi*8+0.5*headingSign)))))
                # landmark actions
                position = tank.posNode._getDerivedPosition()
                mazeBox = self.maze.getMazeBoxXY(int(position.x/192), int(position.z/192))
                if mazeBox:
                    tank.mazeBox = mazeBox
                    if mazeBox.type == "AMMO" and tank.speed <5:
                        for weapon in tank.weapon[1:]:
                            if not weapon.armed and self.gameTicks % weapon.refillSpeed == 0:
                                if weapon.ammo < weapon.maxAmmo:
                                    tank.score -= weapon.ammoCost
                                    if tank == self.playerTank:
                                        self.consoleText = "UNREP: REARMING"
                                weapon.ammo = min(weapon.ammo +1, weapon.maxAmmo)
                    elif mazeBox.type == "ARMOR" and tank.speed <5 and self.gameTicks % 3 == 0:
                        for side in ('F','A','P','S','T','B'):
                            if tank.armor[side] < tank.maxArmor[side]:
                                tank.score -= tank.armorType.cost
                                if tank == self.playerTank:
                                    self.consoleText = "UNREP: REPAIRING"
                            tank.armor[side] = min(tank.armor[side] +1, tank.maxArmor[side])
                    elif mazeBox.type == "FUEL" and tank.speed <5:
                        if tank.fuel < tank.maxFuel:
                            tank.score -= tank.engine.fuelCost
                            if tank == self.playerTank:
                                self.consoleText = "UNREP: REFUELING"
                        tank.fuel = min(tank.fuel +1, tank.maxFuel)
                if not mazeBox or not mazeBox.inmaze:
                    self.spawnTank(tank)
        # bot AI
        self.botTimer += time
        if self.botTimer >= self.botTimerReset:
            self.botTimer -= self.botTimerReset
            # LOS
            tankList = self.tankList[:]
            while tankList:
                tank = tankList.pop()
                LOSmast = tank.LOSNode._getDerivedPosition()
                dir = tank.newDirection
                self.raySceneQuery.Ray = ogre.Ray(LOSmast,ogre.Vector3(cos(dir),0,sin(dir)))
                tank.LOSNav = 19200
                for result in self.raySceneQuery.execute():
                    name = result.movable.getName()
                    if name.find("Wall") > -1:
                        tank.LOSNav = result.distance
                        tank.LOSNavType = result.movable.getUserObject().getUserObject().type
                        break
                for target in tankList:
                    tank.LOS[target] = False
                    target.LOS[tank] = False
                    bearing = target.posNode.getPosition() - LOSmast
                    self.raySceneQuery.Ray = ogre.Ray(LOSmast,bearing)
                    for result in self.raySceneQuery.execute():
                        name = result.movable.getName()
                        if name.find("Wall") > -1:
                            break
                        elif name.find("Tank_"+target.name) > -1:
                            tank.LOS[target] = True
                            target.LOS[tank] = True
                            break
            for tank in self.tankList:
                if tank.bot and tank.bot.active and not tank.burning and not tank.spawning:
                    tank.bot.think(self.botTimerReset)
        elif self.playerTank.bot and self.playerTank.bot.active and not self.playerTank.burning and not self.playerTank.spawning:
            # for testing realtime AI, and a smoother camera
            self.playerTank.bot.think(time)                
        # Update Dashboard
        self.dashTimer += time
        if self.dashTimer >= self.dashTimerReset:
            self.dashTimer -= self.dashTimerReset
            player = self.playerTank
            self.guiFlash = not self.guiFlash
            if self.playerChanged:
                self.windowManager.getWindow( "Dashboard/Console/Player/Name").setText(self.playerTank.name)
                self.windowManager.getWindow( "Dashboard/Console/Player/Vehicle").setText(self.playerTank.vehicle)
                for wepNum in range(1,len(self.playerTank.weapon)):
                    for field in ('Number','Weapon','Ammo','Mount'):
                        self.dashWeapon[wepNum,field].show()
                    self.dashWeapon[wepNum,'Number'].setText(str(wepNum))
                    self.dashWeapon[wepNum,'Weapon'].setText(self.playerTank.weapon[wepNum].abbrev)
                    self.dashWeapon[wepNum,'Weapon'].tooltipText = self.playerTank.weapon[wepNum].name
                    self.dashWeapon[wepNum,'Mount'].setText(self.playerTank.weapon[wepNum].mount)
                for wepNum in range(len(player.weapon),7):
                    for field in ('Number','Weapon','Ammo','Mount'):
                        self.dashWeapon[wepNum,field].hide()
                for specialNum in range(7,11):
                    if specialNum in self.playerTank.specialName:
                        for field in ('Number','Special'):
                            self.dashSpecial[specialNum, field].show()
                        self.dashSpecial[specialNum, 'Number'].setText(str(specialNum))
                        self.dashSpecial[specialNum, 'Special'].setText(self.playerTank.specialName[specialNum])
                        if specialNum is 10:
                            self.dashSpecial[10, 'Number'].setText('0')
                    else:
                        for field in ('Number','Special'):
                            self.dashSpecial[specialNum, field].hide()                                        
                self.playerChanged = False
            self.dashFPS.setText('FPS: ' + str(int(self.renderWindow.getLastFPS())))
            self.consoleText = ""
            if self.debugText:
                self.consoleText = "Debug: "+str(self.debugText)
            elif player.spawning:
                self.consoleText = "Tactical airdrop deployment in progress..."
            elif player.bot.active and not self.consoleText:
                self.consoleText = "Tank AI active, press 'B' to disable."
                if self.gameType == "Demo":
                    self.consoleText = "DEMO MODE"
            elif player.mazeBox.type == "AMMO" and tank.speed <5:
                for weapon in player.weapon[1:]:
                    if weapon.armed:
                        self.consoleText = "Press '=' to toggle weapons for reloading."
            self.dashMessage.setText(self.consoleText)
            self.consoleText = "Press 'U' if your Tank is stuck."
            for side in ('F','A','P','S','T','B'):
                if player.maxArmor[side] > 0:
                    status = float(player.armor[side]) / player.maxArmor[side]
                else:
                    status = 0
                self.dashBarColor(self.dashArmor[side], status)
                if player.highestArmor > 0:
                    ratio = float(player.armor[side]) / player.highestArmor
                else:
                    ratio = 0
                if side == 'P' or side == 'S':
                    barSizeX = 1 
                    barSizeY = status * ratio
                    barPosX = 0
                    barPosY = float(1 - status * ratio) /2
                else:
                    barSizeX = status * ratio
                    barSizeY = 1
                    barPosX = float(1 - status * ratio) /2
                    barPosY = 0
                self.dashArmor[side].setPosition(CEGUI.UVector2(CEGUI.UDim(barPosX, 0), CEGUI.UDim(barPosY, 0)))
                self.dashArmor[side].setSize(CEGUI.UVector2(CEGUI.UDim(barSizeX, 0), CEGUI.UDim(barSizeY, 0)))
            for specialNum in range(7, 11):
                if specialNum in player.specialName:
                    if player.special[player.specialName[specialNum]].active:
                        colour = 'Green'
                    else:
                        colour = 'White'
                    if specialNum == 10:
                        lightNum = 0
                    else:
                        lightNum = specialNum
                    self.dashSpecial[specialNum,'Number'].setProperty("Image", \
                                CEGUI.PropertyHelper.imageToString(self.dashNumSet.getImage(colour+str(lightNum))))
            for wepNum in range(1,len(player.weapon)):
                weapon = player.weapon[wepNum]
                status = float(weapon.ammo) / weapon.maxAmmo
                self.dashBarImage(self.dashWeapon[wepNum,'Ammo'], status)
                if not weapon.armed:
                    colour = 'White'
                else:
                    colour = 'Green'
                if weapon.ammo == 0 and not self.guiFlash:
                    self.dashWeapon[wepNum,'Number'].hide()
                else:
                    self.dashWeapon[wepNum,'Number'].show()
                    self.dashWeapon[wepNum,'Number'].setProperty("Image", \
                                    CEGUI.PropertyHelper.imageToString(self.dashNumSet.getImage(colour+str(wepNum))))
                del weapon
            self.dashScore.setText(str(int(player.score/1000)) +' K')
            self.dashKills.setText(str(int(player.kills)))
            self.dashDeaths.setText(str(int(player.deaths)))
            status = float(player.fuel) / player.engine.fuelCap
            self.dashBarImage(self.dashFuel, status, 'green')
            self.dashBarImage(self.dashHeat, float(player.heat) / 100, 'redVertical')
            status = abs(float(player.speed) / player.maxSpeed)
            self.dashBarImage(self.dashSpeed, status, 'green')
            # Sensor display
            position = player.posNode.getPosition()
            camPosition = ogre.Vector3(position.x, self.mapCamY, position.z)
            self.mapCamNode.setPosition(camPosition)
            self.mapTexture.update()
            # Clear all old blips
            for blip in self.dashBlips:
                self.windowManager.destroyWindow(blip)
            self.dashBlips = []
            blips = player.scan()
            for blip in blips:
                self.dashBlips.append(str(blip))
                if blip.team == -1:
                    blipChoice = choice(['RedSkull','BlueSkull'])
                    blipIcon = self.dashIconSet.getImage(blipChoice)
                elif blip.team is None:
                    blipIcon = self.dashBlipSet.getImage('BlipYellow')
                elif blip.team != player.team or blip.team is 0:
                    blipIcon = self.dashBlipSet.getImage('BlipRed')
                else:
                    blipIcon = self.dashBlipSet.getImage('BlipGreen')
                if self.mapCamLR:
                    scanScale = 4
                else:
                    scanScale = 2
                dot = self.windowManager.createWindow("TaharezLook/StaticImage", str(blip))
                dot.setSize(CEGUI.UVector2(CEGUI.UDim(blip.size, 0), CEGUI.UDim(blip.size, 0)))
                dotPosX = 0.5 + (blip.bearing.x / player.scanRange) / scanScale - blip.size / 2
                dotPosY = 0.5 + (blip.bearing.z / player.scanRange) / scanScale - blip.size / 2
                dot.setPosition(CEGUI.UVector2(CEGUI.UDim(dotPosX, 0), CEGUI.UDim(dotPosY, 0)))
                dot.setProperty("Image", CEGUI.PropertyHelper.imageToString(blipIcon))
                dot.setProperty("FrameEnabled","false")
                self.dashRadar.addChildWindow(dot)
        # Debug
        if self.showDebug:
            self.debugTimer += time
            if self.debugTimer >= self.debugTimerReset:
                self.debugTimer -= self.debugTimerReset
                self.updateStatistics()
        # Key Repeat
        self.keyTimer += time
        while self.keyTimer >= self.keyTimerReset:
            for key in self.keyDown.keys():
                self.keyRepeat(key)
            self.keyTimer -= self.keyTimerReset
        self.mouseTimer += time
        while self.mouseTimer >= self.mouseTimerReset:
            for key in self.mouseDown.keys():
                if self.mouseDown[key]:
                    self.mouseRepeat(key)
            self.mouseTimer -= self.mouseTimerReset
        if self.quit:
            del player, tank
            from Game.GameStates.MainMenu import MainMenu
            GameManager().changeState(MainMenu())
        return True

    def frameEnded(self, evt):
        return True

    def keyPressed(self, evt):
        # CEGUI injection
        ceguiSystem = CEGUI.System.getSingleton()
        ceguiSystem.injectKeyDown(evt.key)
        ceguiSystem.injectChar(evt.text)
        tank = self.playerTank
        if self.showHelp:
            if evt.key == OIS.KC_ESCAPE or evt.key == OIS.KC_F1:
                self.timeStandStill = False
                self.dashHelp.hide()
                self.showHelp = False            
        elif evt.key == OIS.KC_F1:
            self.timeStandStill = True
            self.dashHelp.show()
            self.showHelp = True
        elif evt.key == OIS.KC_ESCAPE:
            #from MenuStates import PauseState
            #GameManager().pushState(PauseState())
            self.quit = True
        elif evt.key == OIS.KC_P:
            self.timeStandStill = not self.timeStandStill
        elif evt.key == OIS.KC_C:
            self.setCamera("toggle")
        elif evt.key == OIS.KC_T:
            self.playerTank.turretLock = not self.playerTank.turretLock
        elif evt.key == OIS.KC_M:
            self.mapCamY = -14900 - self.mapCamY
            self.mapCamLR = not self.mapCamLR
        # testing below
        elif evt.key == OIS.KC_O:
            if self.showDebug:
                self.statsOverlay.hide()
            else:                
                self.statsOverlay.show()
            self.showDebug = not self.showDebug
        elif evt.key == OIS.KC_K and not tank.burning and OIS.KC_LSHIFT in self.keyDown:
            tank.burn()
            tank.score -= tank.cost
        elif evt.key == OIS.KC_K:
            self.renderWindow.writeContentsToTimestampedFile("nXtank_",".jpg")
        elif evt.key == OIS.KC_N:
            self.demoTimer = self.demoTimerReset
        elif evt.key == OIS.KC_U:
            self.unstickFlag = True
        elif evt.key == OIS.KC_Q:
            self.pretty = not self.pretty
            if self.pretty:
                self.sceneManager.shadowTechnique = ogre.SHADOWTYPE_STENCIL_MODULATIVE
            else:
                self.sceneManager.shadowTechnique = ogre.SHADOWTYPE_NONE
        # testing above
        elif evt.key == OIS.KC_X:
            tank.setSpeed(tank.newSpeed * -1)
        elif evt.key == OIS.KC_Z or evt.key == OIS.KC_SPACE:
            tank.setSpeed(0)
        elif evt.key == OIS.KC_V:
            self.sfxMute = not self.sfxMute
        elif evt.key == OIS.KC_B and tank.bot:
            tank.bot.active = not tank.bot.active
        elif evt.key == OIS.KC_EQUALS:
            for wep in range(1, len(tank.weapon)):
                tank.weapon[wep].toggle()
        elif 1 < int(evt.key) < 8 and OIS.KC_LSHIFT in self.keyDown: # key name doesn't match numeric value in OIS
            tank.weapon[int(evt.key)-1].toggle()
        elif 7 < int(evt.key) < 12: # key name doesn't match numeric value in OIS
            specialNum = int(evt.key)-1
            if specialNum in tank.specialName:
                tank.special[tank.specialName[specialNum]].toggle()
        # non-repeating keys above
        else:
            if evt.key not in self.keyDown:
                self.keyRepeat(evt.key)
            self.keyDown[evt.key] = evt
            self.keyTimer -= self.keyTimerReset

    def keyRepeat(self,key):
        if key == OIS.KC_F:
            for tank in self.tankList[1:]:
                tank.firing['all'] = True
        # testing above
        tank = self.playerTank
        turret = tank.turretList[0]
        aim = turret.newAim
        if not tank.burning:
            if key == OIS.KC_W:
                tank.setSpeed(tank.newSpeed + tank.maxSpeed / 4)
            elif key == OIS.KC_S:
                tank.setSpeed(tank.newSpeed - tank.maxSpeed / 4)
            elif key == OIS.KC_A:
                tank.setDirection(tank.direction - tank.turnMax * self.keyTimerReset)
            elif key == OIS.KC_D:
                tank.setDirection(tank.direction + tank.turnMax * self.keyTimerReset)
            elif key == OIS.KC_LEFT:
                tank.setAim(aim - turret.turnMax /5 * self.keyTimerReset)
            elif key == OIS.KC_RIGHT:
                tank.setAim(aim + turret.turnMax /5 * self.keyTimerReset)
            elif key == OIS.KC_UP:
                tank.setAim(tank.direction)
            elif key == OIS.KC_DOWN:
                tank.setAim(tank.direction + pi)
            elif key == OIS.KC_RCONTROL:
                tank.firing['all'] = True
            elif 1 < int(key) < 8: # key name doesn't match numeric value in OIS
                tank.firing[int(key)-1] = True

    def keyReleased(self, evt):
        CEGUI.System.getSingleton().injectKeyUp(evt.key)
        if evt.key == OIS.KC_RCONTROL:
            self.playerTank.firing['all'] = False
        elif 1 < int(evt.key) < 8: # key name doesn't match numeric value in OIS
            self.playerTank.firing[int(evt.key)] = False
        if evt.key in self.keyDown:
            del self.keyDown[evt.key]

    def mousePressed(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonDown(self.convertButton(id))
        #if id == "MB_Middle":
        #    for wep in range(1, len(self.playerTank.weapon)):
        #        self.playerTank.weapon[wep].toggle()
        #else:
        self.mouseDown[id] = True
        self.mouseRepeat(id)
        self.mouseTimer -= self.mouseTimerReset

    def mouseRepeat(self, id):
        if not self.playerTank.burning:
            if self.cameraView is "Gunner":
                newDirection = self.playerTank.turretList[0].newAim
            else:
                mousePos = CEGUI.MouseCursor.getSingleton().getPosition()
                newDirection = ogre.Vector3(mousePos.d_x - self.viewWidth/2 + self.OHXoffset,\
                        0,mousePos.d_y - self.viewHeight/2) # 2D->3D
                newDirection = atan2(newDirection.z,newDirection.x)
            if id == "MB_Left":
                self.playerTank.firing['all'] = True
            if id == "MB_Right":
                self.playerTank.setDirection(newDirection)

    def mouseReleased(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonUp(self.convertButton(id))
        self.mouseDown[id] = False

    def mouseMoved(self, evt):
        ''' Mousewheel controls speed '''
        state = evt.get_state()
        speed = self.playerTank.newSpeed + state.Z.rel / 120 * self.playerTank.maxSpeed / 20
        self.playerTank.setSpeed(speed)
        if self.cameraView is "Gunner":
            turret = self.playerTank.turretList[0]
            aim = turret.newAim
            self.playerTank.setAim(aim + state.X.rel * turret.turnMax / 3000)
        else:
            if not self.playerTank.turretLock:
                mousePos = CEGUI.MouseCursor.getSingleton().getPosition()
                newAim = ogre.Vector3(mousePos.d_x - self.viewWidth/2 + self.OHXoffset,\
                        0,mousePos.d_y - self.viewHeight/2) # 2D->3D
                newAim.normalise()
                self.playerTank.setAim(atan2(newAim.z,newAim.x))
            CEGUI.System.getSingleton().injectMouseMove(evt.get_state().X.rel, evt.get_state().Y.rel)

    def mouseDragged(self, evt):
        pass

    def statusColor(self, status):
        r = min(max(float(1 - status) / 0.5, 0), 1)
        g = min(max(float(status) / 0.5, 0), 1)
        return r,g,0.0
        
    def dashTextColor(self, window, status):
        r,g,b = self.statusColor(status)
        window.setProperty("TextColours", CEGUI.PropertyHelper.colourToString(CEGUI.colour(r,g,0)))

    def dashBarColor(self, window, status):
        r,g,b = self.statusColor(status)
        window.setProperty("ImageColours", CEGUI.PropertyHelper.colourToString(CEGUI.colour(r,g,0)))

    def dashBGColor(self, window, status):
        r,g,b = self.statusColor(status)
        window.setProperty("BackgroundColours", CEGUI.PropertyHelper.colourToString(CEGUI.colour(r,g,0)))

    def dashBarImage(self, window, status, style='green'):
        if style.find('green') > -1:
            barStyle = 'ScaledGreen'
        elif style.find('red') > -1:
            barStyle = 'ScaledRed'
        else:
            barStyle = 'Solid'
        if style.find('Vertical') > -1:
            set = 'ColorBarsVertical'
            status = 1 - status # imageset needs fixin
        else:
            set = 'ColorBars'
        barImage = barStyle + str(min(max(int(status * 20) * 5, 0), 100))
        window.setProperty("Image", "set:" + set + " image:" + barImage)

    def spawnTank(self, tank):
        startBox = choice(self.maze.spawnBoxList)
        x = startBox[0] * 192 + 30 + random()*132
        z = startBox[1] * 192 + 30 + random()*132
        y = 100
        tank.spawn((x,y,z))

    def setCamera(self, camera):
        if camera == "toggle":
            if self.cameraView == "Gunner":
                camera = "OH"
            else:
                camera = "Gunner"
        if camera == "Gunner":
            self.cameraView = "Gunner"
            self.renderWindow.removeAllViewports()
            #self.viewport = self.renderWindow.addViewport(self.mapCamera)
            self.viewport = self.renderWindow.addViewport(self.gunCamera)
            CEGUI.MouseCursor.getSingleton().setPosition\
                (CEGUI.Vector2(self.viewWidth * 0.5, self.viewHeight * 0.3))
            self.shotScale = 1
        elif camera == "OH":
            self.cameraView = "OH"
            self.renderWindow.removeAllViewports()
            self.viewport = self.renderWindow.addViewport(self.OHCamera)
            self.shotScale = 1.75

    def unstick(self, tank):
        name = tank.name
        print "Unsticking "+name
        team = tank.team
        botActive = tank.bot.active
        tankFile = tank.tankFile
        if tank == self.playerTank:
            self.playerTank = None
            playerTank = True
        else:
            playerTank = False
        tank.posNode.removeChild(self.OHCamNode)
        tank.cleanup()
        tank.destroy()
        self.tankList.remove(tank)
        del tank
        newTank = Tank(self, name, team, tankFile, self.debugging, self.physWorld, self.physSpace)
        self.tankList.append(newTank)
        tankMass = []
        for tank in self.tankList:
            tankMass.append(tank.mass)
        maxMass = float(max(tankMass)) / 10
        newTank.setMass(newTank.mass / maxMass)
        newTank.bot = TankBot(newTank, self.maze)
        newTank.bot.active = botActive
        if playerTank:
            self.playerTank = newTank
            self.playerTank.gunCamNode.attachObject(self.gunCamera)
            self.playerTank.posNode.addChild(self.OHCamNode)
        self.spawnTank(newTank)


