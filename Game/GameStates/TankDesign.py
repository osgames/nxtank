#!/usr/bin/env python
# TankDesign.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Tank Designer.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import os as os
import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI
import ogre.physics.OgreOde as OgreOde
import Game.Utility as util
import Game.Equipment as equip
from math import pi
from Game.Core import GameManager
from Game.MetaClasses import Singleton
from Game.GameStates.Base import AbstractGameState
from Game.Weapon import Weapon
from Game.Tank import Tank
from Game.Jukebox import SFX
#import gc
#from sys import getrefcount

class TankDesign(AbstractGameState):
    '''This is the nXtank Arena.

    Attributes:
        root           (singleton): root singleton
        sceneManager   (sceneMgr) : Scene manager
        renderWindow   (window)   : SceneManager render window
        soundManager   (soundMgr) : Sound Manager
        sfx            (sound)    : Sound effect object
        
        renderer       (singleton): CEGUI renderer
        ceguiSystem    (singleton): CEGUI system singleton
        windowManager  (singleton): CEGUI window manager
        
        gui            (sheet)    : CEGUI sheet        
        guiArmorType   (str)      : GUI armorType window
        guiArmor       (dict)     : GUI Armor windows
        guiWeapon      (dict)     : GUI Weapon windows
        guiSpecial     (dict)     : GUI Special windows
        toolTip        (tooltip)  : CEGUI tooltip
        
        statsOverlay   (sheet)    : debug stats overlay sheet

        playerTank     (Tank)     : Player tank object
        entity         (dict)     : entity names -> objects
        designProblem  (bool)     : Design problem requiring correction
        designChanged  (bool)     : Flag to reload Tank
        chooseWeaponNum (int)     : which weapon is being chosen
        chooseSpecialNum(int)     : which special is being chosen
        
        camera         (camera)   : Overhead camera
        camNode        (node)     : Position of OHcamera
        camPitchNode   (node)     : Pitch of OHcamera
        
        quit           (bool)     : Trigger self.exit()
        timeStandStill (bool)     : Pause without menu
        
        timeScale      (float)    : Time dilation factor
        physTicksPerSec(float)    : Physics ticks per second
        gameTimerReset (float)    : Timer for game ticks
        gameTimer      (float)    : Countdown for next game tick
        guiTimerReset  (float)    : Timer for GUI updates
        guiTimer       (float)    : Countdown for next GUI update
        debugTimerReset(float)    : Timer for refreshing debug overlay
        debugTimer     (float)    : Countdown to next debug refresh
        debugText      (str)      : Debug overlay text output
        
        showDebug      (bool)     : Toggle debug overlay
        showHelp       (bool)     : Toggle Help window
        sfxMute        (bool)     : Toggle sound

        guiChooseBody       (window): 
        guiChooseEngine     (window):
        guiChooseHeatsinks  (window):
        guiChooseSuspension (window):
        guiChooseTreads     (window):
        guiChooseBumpers    (window):
        guiChooseArmor      (window):
        guiChooseWeapons    (window):
        guiChooseSpecials   (window):

    Methods:
        enter                 : Enter TankDesign
        exit                  : Leave TankDesign
        framestarted          : Per-frame game logic updates
        keyPressed/keyReleased: Player keyboard controls
        keyRepeat             : Repeatable key commands
        mousePressed/Released/
            Moved/Dragged     : Player mouse controls
        updateDesign          : Updates Tank design values with player input
        updateGUI             : Redraws scene and windows
        chooseHide            : Hide all choice windows
        EquipTable            : Build equip table with createWindow
        createWindow          : create a CEGUI window
        updateDesign          : change design values
        updateGUI             : replace Tank with another based on new design values and update displayed data
        statusTextRedGreen    : sets a staticText element to red or green to indicate status
        statusTextColor       : sets a staticText element to color to indicate status
        mouseInColor          : returns color for statucText to simulate button mouseover color changes
        mouseOutColor         : returns color for statucText to simulate button mouseover color changes
        chooseWindow          : show chooser window based on selected equipment type
        balanceArmor          : balances current armor total among all facings
        maxArmor              : scales current armor ratios to max available armor values
        loadTankFile          : event handler to call loadTank
        loadTank              : calls setScene and creates a new Tank
        saveTankFile          : event handler to call saveTank
        saveTank              : writes design data to .tank file
        setScene              : clears scene, sets lighting and camera
    '''    
    __metaclass__ = Singleton
        
    def __init__(self, renderer, sceneManager, soundManager, debugging):
        super(TankDesign, self).__init__()
        self.soundManager = soundManager
        self.renderer = renderer
        self.ceguiSystem = CEGUI.System.getSingleton()
        self.sceneManager = sceneManager
        self.debugging = debugging
        self.sfx = SFX(self.soundManager) # could inherit this also
        self.root = ogre.Root.getSingleton()
        self.renderWindow = self.root.getAutoCreatedWindow()
        self.quit = False
        self.timeStandStill = False
        self.timeScale = 0.25
        self.gameTimerReset = 1.0/15 / self.timeScale
        self.gameTimer = self.gameTimerReset
        self.guiTimerReset = 1.0 / 2
        self.guiTimer = self.guiTimerReset
        self.debugTimerReset = 1.0 / 5
        self.debugTimer = self.debugTimerReset
        self.debugText = ""
        self.showDebug = True
        self.mouseLook = False
        self.cameraRotate = 1
        self.sfxMute = True
        self.guiArmor = {}
        self.guiSpecial = {}
        self.guiWeapon = {}
        self.designProblem = False
        self.entity = {}
        self.designChanged = False
        self.buttonColor = CEGUI.PropertyHelper.colourToString(CEGUI.colour(4.0/16,12.0/16,12.0/16))
        self.tankFile = "testplayer.tank"
        # CEGUI
        self.windowManager = CEGUI.WindowManager.getSingleton()
        self.guiIconSet   = CEGUI.ImagesetManager.getSingleton().getImageset("HUD_icons")
        self.gui = CEGUI.WindowManager.getSingleton().loadWindowLayout("TankDesign.layout")
        # Setup Tooltips
        self.ceguiSystem.setDefaultTooltip("TaharezLook/Tooltip")
        self.toolTip = self.ceguiSystem.getDefaultTooltip()
        #tip.displayTime=0.80
        self.toolTip.hoverTime = 0.5
        self.toolTip.fadeTime = 0.2
        self.guiName       = self.windowManager.getWindow("TankDesign/Internal/Name")
        self.guiDesigner   = self.windowManager.getWindow("TankDesign/Internal/Designer")
        self.guiBody       = self.windowManager.getWindow("TankDesign/Internal/Body")
        self.guiEngine     = self.windowManager.getWindow("TankDesign/Internal/Engine")
        self.guiHeatsinks  = self.windowManager.getWindow("TankDesign/Internal/Heatsinks")
        self.guiSuspension = self.windowManager.getWindow("TankDesign/Internal/Suspension")
        self.guiTreads     = self.windowManager.getWindow("TankDesign/Internal/Treads")
        self.guiBumpers    = self.windowManager.getWindow("TankDesign/Internal/Bumpers")
        for weapon in range(1,7):
            for field in ('Weapon','MountSelect'):
                self.guiWeapon[weapon,field] = self.windowManager.getWindow("TankDesign/Weapons/" + str(weapon) +"/"+ str(field))
        for special in range(7,11):
                self.guiSpecial[special, 'Special'] = self.windowManager.getWindow\
                    ( "TankDesign/Specials/" + str(special))
        self.guiArmorType  = self.windowManager.getWindow("TankDesign/Armor/Type")
        for side in ('F','A','P','S','T','B'):
            self.guiArmor[side] = self.windowManager.getWindow("TankDesign/Armor/"+side)
        self.guiArmorLeft  = self.windowManager.getWindow("TankDesign/Armor/Leftover")
        self.guiCost       = self.windowManager.getWindow("TankDesign/Stats/Cost")
        self.guiMass       = self.windowManager.getWindow("TankDesign/Stats/Mass")
        self.guiSpace      = self.windowManager.getWindow("TankDesign/Stats/Space")
        self.guiMaxSpeed   = self.windowManager.getWindow("TankDesign/Stats/MaxSpeed")
        self.guiAccel      = self.windowManager.getWindow("TankDesign/Stats/Accel")
        self.guiHandling   = self.windowManager.getWindow("TankDesign/Stats/Handling")
        self.guiCooling    = self.windowManager.getWindow("TankDesign/Stats/Cooling")
        # Choices
        self.guiChooseBody       = self.windowManager.getWindow("TankDesign/Choose/Body")
        self.guiChooseEngine     = self.windowManager.getWindow("TankDesign/Choose/Engine")
        self.guiChooseHeatsinks  = self.windowManager.getWindow("TankDesign/Choose/Heatsinks")
        self.guiChooseSuspension = self.windowManager.getWindow("TankDesign/Choose/Suspension")
        self.guiChooseTreads     = self.windowManager.getWindow("TankDesign/Choose/Treads")
        self.guiChooseBumpers    = self.windowManager.getWindow("TankDesign/Choose/Bumpers")
        self.guiChooseArmor      = self.windowManager.getWindow("TankDesign/Choose/Armor")
        self.guiChooseWeapons    = self.windowManager.getWindow("TankDesign/Choose/Weapons")
        self.guiChooseSpecials   = self.windowManager.getWindow("TankDesign/Choose/Specials")
        # Generate equipment tables
        fields = ['name', 'size', 'mass', 'massLimit', 'spaceLimit', 'drag', 'handling', 'turrets', 'cost']
        self.equipTable("body", self.guiChooseBody, "TankDesign/Choose/Body/", fields)
        self.equipTable("engine", self.guiChooseEngine, "TankDesign/Choose/Engine/", fontSize=10)
        self.equipTable("suspension", self.guiChooseSuspension, "TankDesign/Choose/Suspension/")
        self.equipTable("tread", self.guiChooseTreads, "TankDesign/Choose/Treads/")
        self.equipTable("bumper", self.guiChooseBumpers, "TankDesign/Choose/Bumpers/")
        self.equipTable("armor", self.guiChooseArmor, "TankDesign/Choose/Armor/")
        self.equipTable("special", self.guiChooseSpecials, "TankDesign/Choose/Special/")
        fields = ['name', 'damage', 'ammo', 'range', 'speed', 'mass', 'space', 'heat', 'ammoCost', 'cost']
        self.equipTable("weapon", self.guiChooseWeapons, "TankDesign/Choose/Weapon/", fields, 10)
        
    def enter(self):
        self.quit = False
        self.playerTank = None
        self.statsOverlay = ogre.OverlayManager.getSingleton().getByName('Arena/StatsOverlay')
        #self.statsOverlay.show()
        self.ceguiSystem.setGUISheet(self.gui)
        self.chooseHide()
        # Cameras and shadows
        self.sceneManager.shadowTechnique = ogre.SHADOWTYPE_STENCIL_MODULATIVE
        self.camera = self.sceneManager.createCamera("camera")
        self.camera.nearClipDistance = 5
        self.viewport = self.renderWindow.addViewport(self.camera)
        # Physics
        self.physTicksPerSec = 60
        self.physTimerReset = 1.0 / self.physTicksPerSec
        self.physTimer = 0
        self.physWorld = OgreOde.World(self.sceneManager)
        self.physWorld.setCFM(10e-5)
        self.physWorld.setERP(0.8)
        self.physWorld.setAutoSleep(True)
        self.physWorld.setAutoSleepAverageSamplesCount(10)
        self.physWorld.setContactCorrectionVelocity(1.0)
        self.physSpace = self.physWorld.getDefaultSpace()
        timeStep = 1.0 / self.physTicksPerSec
        self.timeScale = 0.25 # slower to see Shots
        maxFrameTime = 1.0/10
        self.physStepper = OgreOde.StepHandler(self.physWorld, OgreOde.StepHandler.QuickStep, 
                                           timeStep, maxFrameTime, self.timeScale)
        self.updateGUI()
        # subscribe events for GUI actions
        #self.subscribeEvent("TankDesign/Internal/LoadButton", "BUTTON", "loadTankFile")
        #self.subscribeEvent("TankDesign/Internal/SaveButton", "BUTTON", "saveTankFile")
        self.subscribeEvent("TankDesign/Internal/Name", "TEXT", "updateDesign")
        self.subscribeEvent("TankDesign/Internal/Designer", "TEXT", "updateDesign")
        for button in ("Body","Engine","Heatsinks","Suspension","Treads","Bumpers"):
            self.subscribeEvent("TankDesign/Internal/"+button+"Button", "BUTTON", "chooseWindow")
        for weapon in range(1,7):
            self.subscribeEvent("TankDesign/Weapons/Weapon"+str(weapon)+"Button", "BUTTON", "chooseWindow")
            self.subscribeEvent("TankDesign/Weapons/"+str(weapon)+"/MountSelect", "SPINNER", "updateDesign")
            self.subscribeEvent("TankDesign/Weapons/"+str(weapon)+"/Weapon", "CLICK", "updateDesign")
        for special in range(7,11):
            self.subscribeEvent("TankDesign/Specials/Special"+str(special)+"Button", "BUTTON", "chooseWindow")
            self.subscribeEvent("TankDesign/Specials/"+str(special), "CLICK", "updateDesign")
        self.subscribeEvent("TankDesign/Armor/ArmorButton", "BUTTON", "chooseWindow")
        self.subscribeEvent("TankDesign/Internal/Heatsinks", "SPINNER", "updateDesign")
        self.subscribeEvent("TankDesign/Armor/ArmorBalButton", "BUTTON", "balanceArmor")
        self.subscribeEvent("TankDesign/Armor/ArmorMaxButton", "BUTTON", "maxArmor")
        for side in ('F','A','P','S','T','B'):
            self.subscribeEvent("TankDesign/Armor/"+side, "SPINNER", "updateDesign")
        for body in equip.bodyList():
            self.subscribeEvent("TankDesign/Choose/Body/"+body+"/name", "CLICK", "updateDesign")
        for engine in equip.engineList():
            self.subscribeEvent("TankDesign/Choose/Engine/"+engine+"/name", "CLICK", "updateDesign")
        for suspension in equip.suspensionList():
            self.subscribeEvent("TankDesign/Choose/Suspension/"+suspension+"/name", "CLICK", "updateDesign")
        for tread in equip.treadList():
            self.subscribeEvent("TankDesign/Choose/Treads/"+tread+"/name", "CLICK", "updateDesign")
        for bumper in equip.bumperList():
            self.subscribeEvent("TankDesign/Choose/Bumpers/"+bumper+"/name", "CLICK", "updateDesign")
        for armor in equip.armorList():
            self.subscribeEvent("TankDesign/Choose/Armor/"+armor+"/name", "CLICK", "updateDesign")
        for weapon in equip.weaponList():
            self.subscribeEvent("TankDesign/Choose/Weapon/"+weapon+"/name", "CLICK", "updateDesign")
        for special in equip.specialList():
            self.subscribeEvent("TankDesign/Choose/Special/"+special+"/name", "CLICK", "updateDesign")
        print "Entered TankDesign."
        
    def exit(self):
        self.playerTank.cleanup()
        self.playerTank.destroy()
        del self.playerTank
        self.sceneManager.clearScene()
        self.sceneManager.destroyAllCameras()
        self.renderWindow.removeAllViewports()
        self.physWorld.clearContacts()
        del self.physStepper
        del self.physSpace
        #gc.collect()
        #print getrefcount(self.physWorld)
        #print gc.get_referrers(self.physWorld)
        del self.physWorld
        print "Exited TankDesign"
                
    def frameStarted(self, evt):
        time = evt.timeSinceLastFrame
        self.ceguiSystem.getSingleton().injectTimePulse(time)
        self.camera.lookAt(8,0,-15)
        if self.timeStandStill:
            return True
        if self.designChanged:
            self.updateGUI()
            self.designChanged = False
        tank = self.playerTank
        # Move entities
        tank.setAim(tank.turretList[0].aim + tank.turretList[0].turnMax)
        tank.moveTurrets(time * self.timeScale)
        for turret in tank.turretList:
            turret.posNode.yaw(ogre.Degree(-240) * time * self.timeScale)
        # Physics
        self.physTimer += time
        while self.physTimer >= self.physTimerReset:
            self.physTimer -= self.physTimerReset
            tank.physBody.setAngularVelocity(ogre.Vector3(0,pi * self.timeScale,0))
            tank.physBody.setPosition(ogre.Vector3(0,0,0))
            if self.physStepper.step(self.physTimerReset):
                self.physWorld.synchronise()
            tank.animate(time)
        # Tick-based actions
        self.gameTimer += time
        while self.gameTimer >= self.gameTimerReset:
            self.gameTimer -= self.gameTimerReset
            self.shotBatch = 1
            tank.spawning = False
            tank.firing['all'] = False # physics problem clearing old shots
            tank.update()
            tank.heat = 0
            for weapon in tank.weapon[1:]:
                weapon.ammo = weapon.maxAmmo
                weapon.armed = True
        # Debug
        self.debugTimer -= time
        if self.debugTimer < 0:
            self.debugTimer = self.debugTimerReset
            self.updateStatistics()
            #self.guiChoose.setText(self.debugText)
        if self.quit:
            del tank
            from Game.GameStates.MainMenu import MainMenu
            GameManager().changeState(MainMenu())
        return True

    def frameEnded(self, evt):
        return True
            
    def keyPressed(self, evt):
        """Handle key pressed events, again note the last minute
        calling of state imports, again this was to prevent import
        loop problems"""
        # CEGUI injection
        ceguiSystem = CEGUI.System.getSingleton()
        ceguiSystem.injectKeyDown(evt.key)
        ceguiSystem.injectChar(evt.text)
        if evt.key == OIS.KC_ESCAPE:
            self.quit = True
        if evt.key == OIS.KC_P:
            self.timeStandStill = not self.timeStandStill
        elif evt.key == OIS.KC_K:
            self.renderWindow.writeContentsToTimestampedFile("nXtank_",".jpg")
        if evt.key == OIS.KC_L:
            self.mouseLook = not self.mouseLook
        if evt.key == OIS.KC_O:
            if self.showDebug:
                self.statsOverlay.hide()
            else:                
                self.statsOverlay.show()
            self.showDebug = not self.showDebug

    def keyReleased(self, evt):
        try:
            CEGUI.System.getSingleton().injectKeyUp(evt.key)
        except:
            pass

    def mousePressed(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonDown(self.convertButton(id))

    def mouseReleased(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonUp(self.convertButton(id))

    def mouseMoved(self, evt):
        state = evt.get_state()
        CEGUI.System.getSingleton().injectMouseMove(evt.get_state().X.rel, evt.get_state().Y.rel)
        if self.mouseLook:
            self.camNode.translate(0, self.cameraRotate * state.Z.rel/10, self.cameraRotate * state.Y.rel)

    def mouseDragged(self, evt):
        pass

    def convertButton(self, oisID):
        ''' Convert mouse buttons from OIS to CEGUI '''
        if oisID == "MB_Left":
            return CEGUI.LeftButton
        elif oisID == "MB_Right":
            return CEGUI.RightButton
        elif oisID == "MB_Middle":
            return CEGUI.MiddleButton
        else:
            return CEGUI.LeftButton

    def subscribeEvent(self, window, eventType, function):
        if eventType == "BUTTON":
            event = CEGUI.PushButton.EventClicked
        elif eventType == "TEXT":
            event = CEGUI.Editbox.EventTextAccepted
        elif eventType == "SPINNER":
            event = CEGUI.Spinner.EventValueChanged
        elif eventType == "CLICK":
            event = CEGUI.Window.EventMouseClick        
        elif eventType == "MOUSEIN":
            event = CEGUI.Window.EventMouseEnters        
        elif eventType == "MOUSEOUT":
            event = CEGUI.Window.EventMouseLeaves        
        input = self.windowManager.getWindow(window)
        input.subscribeEvent(event, self, function)

    def equipTable(self, equipType, window, windowPrefix, fields=None, fontSize=12):
        equipList = getattr(equip, equipType + "List")()
        equipFunc = getattr(equip,equipType)
        if not fields:
            fields = equipFunc(equipList[0])._fields
        headerName = windowPrefix + "Header"
        headerWindow = self.createWindow("TEXT", headerName, window, \
                                         0, 0, 1, 1.0/(len(equipList)+1), fontSize)
        for field in fields:
            index = fields.index(field)
            if field == "name":
                text = ""
            else:
                text = field.title()
            self.createWindow("TEXT", headerName +"/"+ field, headerWindow, \
                              index*1.0/len(fields), 0, 1.0/len(fields), 1, fontSize, text)
        lineY = 0
        for equipName in equipList:
            lineY += 1
            lineName = windowPrefix + equipName
            lineWindow = self.createWindow("TEXT", lineName, window, \
                         0, lineY*1.0/(len(equipList)+1), 1, 1.0/(len(equipList)+1), fontSize)
            equipItem = equipFunc(equipName)
            for field in fields:
                index = fields.index(field)
                text = str(getattr(equipItem,field))
                color = None
                type = "TEXT"
                if field == "name":
                    text = text.title()
                    color = self.buttonColor
                    if equipType == "weapon":
                        text = text.replace("Light","Lt")
                        text = text.replace("Heavy","Hv")
                self.createWindow(type, lineName +"/"+ field, lineWindow, \
                                  index*1.0/len(fields), 0, 1.0/len(fields), 1, fontSize, text, color)

    def createWindow(self, type, name, parent, posX, posY, sizeX, sizeY, fontSize, text=None, color=None):
        if type == "BUTTON":
            type = "TaharezLook/Button"
        else:
            type = "TaharezLook/StaticText"
        window = self.windowManager.createWindow(type, name)
        parent.addChildWindow(name)
        window.setPosition(CEGUI.UVector2(CEGUI.UDim(posX, 0), CEGUI.UDim(posY, 0)))
        window.setSize(CEGUI.UVector2(CEGUI.UDim(sizeX, 0), CEGUI.UDim(sizeY, 0)))
        window.setFont("BlueHighway-"+str(fontSize))
        if type == "TaharezLook/Button":
            window.setProperty("NormalTextColour",color)
        else:
            if color:
                window.setProperty("TextColours",color)
                self.subscribeEvent(name, "MOUSEIN", "mouseInColor")
                self.subscribeEvent(name, "MOUSEOUT", "mouseOutColor")
            window.setProperty("HorzFormatting","HorzCentred")
            window.setProperty("FrameEnabled","False")
        if text:
            window.setText(text)
        return window

    def updateDesign(self, evt=None):
        window = str(evt.window.name).split("/")
        tank = self.playerTank
        if window[1] == "Armor":
            side = window[2]
            armor = int(self.guiArmor[side].getCurrentValue())
            if armor != tank.maxArmor[side]:
                tank.maxArmor[side] = armor
        elif window[2] == "Name":
            tank.vehicle = str(evt.window.getText())
        elif window[2] == "Designer":
            tank.designer = str(evt.window.getText())
        elif window[2] == "Heatsinks":
            heatSinks = int(self.guiHeatsinks.getCurrentValue())
            if heatSinks != tank.heatSinks.num:
                tank.heatSinks = equip.heatSink(heatSinks)
        elif window[2] == "Body":
            body = window[3]
            tank.body = equip.body(str(body))
            for wepNum in range(1,len(tank.weapon)):
                weapon = tank.weapon[wepNum]
                if weapon.mount > tank.body.turrets:
                    weapon.mount = "T"+str(tank.body.turrets)
        elif window[2] == "Engine":
            engine = window[3]
            tank.engine = equip.engine(str(engine))
        elif window[2] == "Armor":
            armor = window[3]
            tank.armorType = equip.armor(str(armor))
        elif window[2] == "Suspension":
            suspension = window[3]
            tank.suspension = equip.suspension(str(suspension))
        elif window[2] == "Treads":
            tread = window[3]
            tank.tread = equip.tread(str(tread))
        elif window[2] == "Bumpers":
            bumper = window[3]
            tank.bumpers = equip.bumper(str(bumper))
        elif window[2] == "Weapon":
            wepName = window[3]
            wepNum = self.chooseWeaponNum
            if wepNum >= len(tank.weapon):
                wepNum = len(tank.weapon)
                tank.weapon.append(None)
                mount = "T1"
            else:
                mount = tank.weapon[wepNum].mount
            if tank.weapon[wepNum]:
                tank.weapon[wepNum].destroy()
            tank.weapon[wepNum] = Weapon(wepName, wepNum, mount, tank, self.debugging)
        elif window[1] == "Weapons" and window[3] == "MountSelect":
            wepNum = int(window[2])
            mount = int(self.guiWeapon[wepNum,'MountSelect'].getCurrentValue())
            if len(tank.weapon) > wepNum:
                tank.weapon[wepNum].mount = "T" + str(mount)
        elif window[1] == "Weapons" and window[3] == "Weapon":
            wepNum = int(window[2])
            if wepNum < len(tank.weapon):
                del tank.weapon[wepNum]
        elif window[1] == "Specials":
            specialNum = int(window[2])
            specialName = str(evt.window.getText())
            if specialName in tank.special:
                del tank.special[specialName]
                del tank.specialName[specialNum]
        elif window[2] == "Special":
            specialName = window[3]
            if not specialName in tank.specialName.values():
                tank.specialName[self.chooseSpecialNum] = specialName
                tank.special[specialName] = tank.loadSpecial(specialName)
        self.designChanged = True
        
    def updateGUI(self):
        tankOrientation = None
        #turretOrientation = None
        if self.playerTank:
            self.saveTank()
            tankOrientation = self.playerTank.physBody.getOrientation()
            turretOrientation = self.playerTank.turretList[0].posNode.getOrientation()
            self.playerTank.cleanup()
            self.playerTank.destroy()
        self.setScene()
        self.loadTankFile()
        tank = self.playerTank
        if tankOrientation:
            tank.physBody.setOrientation(tankOrientation)
            for turret in tank.turretList:
                turret.posNode.setOrientation(turretOrientation)
        self.guiName.setText(tank.vehicle)
        self.guiDesigner.setText(tank.designer)
        self.guiBody.setText(tank.body.name)
        self.guiEngine.setText(tank.engine.name)
        self.guiHeatsinks.setText(str(tank.heatSinks.num))
        totalHeat = 0.001
        for weapon in tank.weapon[1:]:
            totalHeat += weapon.heat / weapon.reload
        heatRate = (float(tank.heatSinks.num) /5) / totalHeat
        self.statusTextColor(self.guiCooling, heatRate )
        self.guiCooling.setText("%3d %%" % (heatRate * 100))
        self.guiSuspension.setText(tank.suspension.name)
        self.guiTreads.setText(tank.treads.name)
        self.guiBumpers.setText(tank.bumpers.name)
        for wepNum in range(1,7):
            self.guiWeapon[wepNum,'MountSelect'].setMaximumValue(tank.body.turrets)
        for wepNum in range(1,len(tank.weapon)):
            weapon = tank.weapon[wepNum]
            self.guiWeapon[wepNum,'Weapon'].show()
            self.guiWeapon[wepNum,'Weapon'].setText(weapon.name)
            if weapon.mount.find("T") > -1:
                (junk, turretNum) = weapon.mount.split("T",1)
            self.guiWeapon[wepNum,'MountSelect'].setCurrentValue(int(turretNum))
        for wepNum in range(len(tank.weapon),7):
            self.guiWeapon[wepNum,'Weapon'].hide()
        for specialNum in range(7,11):
            if specialNum in self.playerTank.specialName:
                self.guiSpecial[specialNum, 'Special'].show()
                self.guiSpecial[specialNum, 'Special'].setText(self.playerTank.specialName[specialNum])
            else:
                self.guiSpecial[specialNum, 'Special'].hide()
        self.guiArmorType.setText(str(tank.armorType.name))
        totalArmor = 0
        for side in ('F','A','P','S','T','B'):
            self.guiArmor[side].setText(str(tank.maxArmor[side]))
            totalArmor += tank.maxArmor[side]
        armorMassLeft = (tank.body.massLimit - tank.mass) / (tank.armorType.mass * tank.body.size)
        armorSpaceLeft = (tank.body.spaceLimit - tank.space) / (tank.armorType.space * tank.body.size)
        armorLeft = min(armorMassLeft, armorSpaceLeft)
        self.guiArmorLeft.setText(str(armorLeft))
        self.guiCost.setText(str(tank.cost))
        if tank.mass > tank.body.massLimit:
            self.designProblem = True
        self.statusTextRedGreen(self.guiMass, 1- float(tank.mass)/(tank.body.massLimit +1) )
        self.guiMass.setText(str(tank.mass) +" / "+ str(tank.body.massLimit))
        if tank.space > tank.body.spaceLimit:
            self.designProblem = True
        self.statusTextRedGreen(self.guiSpace, 1- float(tank.space)/(tank.body.spaceLimit +1) )
        self.guiSpace.setText(str(tank.space) +" / "+ str(tank.body.spaceLimit))
        self.guiMaxSpeed.setText("%3.2f" % tank.maxSpeed)
        self.guiAccel.setText("%3.2f" % tank.accel)
        self.guiHandling.setText(str(tank.handling))
        
    def statusTextRedGreen(self, window, status):
        if status > 0:
            r,g = (0,1)
        else:
            r,g = (1,0)
        window.setProperty("TextColours", CEGUI.PropertyHelper.colourToString(CEGUI.colour(r,g,0)))

    def statusTextColor(self, window, status):
        r = min(max(float(1 - status) / 0.5, 0), 1)
        g = min(max(float(status) / 0.5, 0), 1)
        window.setProperty("TextColours", CEGUI.PropertyHelper.colourToString(CEGUI.colour(r,g,0)))
        
    def mouseInColor(self, evt):
        evt.window.setProperty("TextColours",CEGUI.PropertyHelper.colourToString(CEGUI.colour(1,1,1)))

    def mouseOutColor(self, evt):
        evt.window.setProperty("TextColours",self.buttonColor)

    def chooseHide(self):
        self.guiChooseBody.hide()
        self.guiChooseEngine.hide()
        self.guiChooseHeatsinks.hide()
        self.guiChooseSuspension.hide()
        self.guiChooseTreads.hide()
        self.guiChooseBumpers.hide()
        self.guiChooseArmor.hide()
        self.guiChooseWeapons.hide()
        self.guiChooseSpecials.hide()        

    def chooseWindow(self, evt):
        window = str(evt.window.name).split("/")[2]
        self.chooseHide()
        if window == "BodyButton":
            self.guiChooseBody.show()
        elif window == "EngineButton":
            self.guiChooseEngine.show()
        elif window == "HeatsinksButton":
            self.guiChooseHeatsinks.show()
        elif window == "SuspensionButton":
            self.guiChooseSuspension.show()
        elif window == "TreadsButton":
            self.guiChooseTreads.show()
        elif window == "BumpersButton":
            self.guiChooseBumpers.show()
        elif window == "ArmorButton":
            self.guiChooseArmor.show()
        elif window.find("Weapon") > -1:
            self.chooseWeaponNum = int(window.strip("WeaponButton"))
            #del self.playerTank.weapon[self.chooseWeaponNum]
            #self.designChanged = True
            self.guiChooseWeapons.show()
        elif window.find("Special") > -1:
            self.chooseSpecialNum = int(window.strip("SpecialButton"))
            self.guiChooseSpecials.show()
        else:
            print str(evt.window.name)

    def balanceArmor(self, evt):
        tank = self.playerTank
        totalArmor = 0
        for side in ('F','A','P','S'):
            totalArmor += tank.maxArmor[side]
        for side in ('F','A','P','S'):
            tank.maxArmor[side] = int(totalArmor/4)
        totalArmor = 0
        for side in ('T','B'):
            totalArmor += tank.maxArmor[side]
        for side in ('T','B'):
            tank.maxArmor[side] = int(totalArmor/2)
        self.designChanged = True
            
    def maxArmor(self, evt):
        tank = self.playerTank
        totalArmor = 0
        for side in ('F','A','P','S','T','B'):
            totalArmor += tank.maxArmor[side]
        if totalArmor == 0:
            return
        armorMassLeft = (tank.body.massLimit - tank.mass) / (tank.armorType.mass * tank.body.size)
        armorSpaceLeft = (tank.body.spaceLimit - tank.space) / (tank.armorType.space * tank.body.size)
        maxArmor = min(armorMassLeft, armorSpaceLeft) + totalArmor
        for side in ('F','A','P','S','T','B'):
            tank.maxArmor[side] = int(maxArmor * tank.maxArmor[side]/totalArmor)
        self.designChanged = True

    def loadTankFile(self, evt=None):
        self.loadTank(self.tankFile)
    
    def loadTank(self, tankFile):
        self.setScene()
        tank = Tank(self, "Tank", 1, tankFile, self.debugging, self.physWorld, self.physSpace)
        tank.spawn((0, 0, 0))
        self.playerTank = tank
        
    def saveTankFile(self, evt):
        self.saveTank()

    def saveTank(self):
        tank = self.playerTank
        os.chdir('tanks')
        tankFile = open(self.tankFile, 'w')
        tankFile.write('Vehicle:    '+ tank.vehicle +'\n')
        tankFile.write('Designer:   '+ tank.designer +'\n')
        tankFile.write('Body:       '+ tank.body.name +'\n')
        tankFile.write('Engine:     '+ tank.engine.name +'\n')
        for wepNum in range(1,len(tank.weapon)):
            weapon = tank.weapon[wepNum]
            tankFile.write('W#' + str(wepNum) +':        '+ weapon.mount +' '+ weapon.name +'\n')
        tankFile.write('Armor Type: '+ tank.armorType.name +'\n')
        tankFile.write('Front:      '+ str(tank.maxArmor["F"]) +'\n')
        tankFile.write('Back:       '+ str(tank.maxArmor["A"]) +'\n')
        tankFile.write('Left:       '+ str(tank.maxArmor["P"]) +'\n')
        tankFile.write('Right:      '+ str(tank.maxArmor["S"]) +'\n')
        tankFile.write('Top:        '+ str(tank.maxArmor["T"]) +'\n')
        tankFile.write('Bottom:     '+ str(tank.maxArmor["B"]) +'\n')
        for specialNum in range(7,11):
            if specialNum in self.playerTank.specialName:
                tankFile.write('Special:    '+ self.playerTank.specialName[specialNum] +'\n')
        tankFile.write('Heat sinks: '+ str(tank.heatSinks.num) +'\n')
        tankFile.write('Suspension: '+ tank.suspension.name +'\n')
        tankFile.write('Treads:     '+ tank.treads.name +'\n')
        tankFile.write('Bumpers:    '+ tank.bumpers.name +'\n')
        tankFile.close()
        os.chdir('..')
    
    def setScene(self):
        self.sceneManager.clearScene()
        light = self.sceneManager.createLight ('DirectionalLight')
        light.type = ogre.Light.LT_DIRECTIONAL
        light.diffuseColour = (1, 1, 1)
        light.specularColour = (.75, .75, .75)
        light.direction = (0, -1, 1)
        self.sceneManager.ambientLight = (0, 0, 0)
        self.camNode = self.sceneManager.getRootSceneNode().createChildSceneNode("camNode", (0,50,-75))
        self.camPitchNode = self.camNode.createChildSceneNode("camPitchNode")
        self.camPitchNode.attachObject(self.camera)