#!/usr/bin/env python
# MazeDesign.py

# Copyright (C) 2008  Daniel McDaniel
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the nXtank Maze Designer.

Author: Daniel McDaniel

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS
import ogre.gui.CEGUI as CEGUI

from Game.Core import GameManager
from Game.MetaClasses import Singleton
from Game.GameStates.Base import AbstractGameState
from Game.Maze import Maze
from Game.Jukebox import SFX

class MazeDesign(AbstractGameState):
    '''This is the nXtank MazeDesign.

    Attributes:
        root           (singleton): root singleton
        sceneManager   (sceneMgr) : Scene manager
        renderWindow   (window)   : SceneManager render window
        soundManager   (soundMgr) : Sound Manager
        sfx            (sound)    : Sound effect object
        
        renderer       (singleton): CEGUI renderer
        ceguiSystem    (singleton): CEGUI system singleton
        windowManager  (singleton): CEGUI window manager
                
        guiSheet       (sheet)    : CEGUI sheet
        guiIconSet     (imageset) : Icon image set
        guiTileSet     (imageset) : Tile image set for Help menu
        guiHelp        (window)   : GUI Help window
        guiFPS         (window)   : GUI FPS window
        mouseX         (int)      : GUI mouse location
        mouseY         (int)      : GUI mouse location
        cursorX        (float)    : Maze location
        cursorY        (float)    : Maze location
        cursorXoffset  (float)    : Maze location fraction
        cursorYoffset  (float)    : Maze location fraction

        currentMaze    (Maze)     : Maze Object

        helpLabel      (window)   : Maze Help Label
        nameLabel      (window)   : Maze Name Label
        name           (window)   : Maze Name
        authorLabel    (window)   : Maze Author Label
        author         (window)   : Maze Author
        mazeSizeLabel  (window)   : Maze Size Label
        mazeSize       (window)   : Maze Size
        mazeTypeLabel  (window)   : Maze Type Label
        mazeType       (window)   : Maze Type
        mazeDescLabel  (window)   : Maze Desc Label
        mazeDesc       (window)   : Maze Desc

        toolTip        (tooltip)  : CEGUI tooltip
        
        statsOverlay   (sheet)    : debug stats overlay sheet

        OHCamera       (camera)   : Overhead camera
        OHCamNode      (node)     : Position of OHcamera
        OHPitchNode    (node)     : Pitch of OHcamera
        cameraView     (string)   : main window camera view mode
        viewport       (viewport) : OH viewport
        viewHeight     (float)    : Height of viewport in pixels
        viewWidth      (float)    : Width of viewport in pixels
        OHXoffset      (float)    : Pixels to offset center of OHcamera
        
        quit           (bool)     : Trigger self.exit()
        timeStandStill (bool)     : Pause without menu
        
        keyTimerReset  (float)    : Timer for repeatable keys
        keyTimer       (float)    : Countdown for next key repeat
        keyDown        (dict)     : Dict of keyPressed events
        mouseTimerReset(float)    : Timer for repeatable mouse buttons
        mouseTimer     (float)    : Countdown for next mouse repeat
        mouseDown      (dict)     : Dict of mousePressed toggles
        debugTimerReset(float)    : Timer for refreshing debug overlay
        debugTimer     (float)    : Countdown to next debug refresh
        debugText      (str)      : Debug overlay text output
        guiTimerReset  (float)    : Timer for GUI updates
        guiTimer       (float)    : Countdown for next GUI update
        
        showDebug      (bool)     : Toggle debug overlay
        showHelp       (bool)     : Toggle Help window
        sfxMute        (bool)     : Toggle sound
        
        gameType       (str)      : Game type
        shotBatch      (int)      : number of shots to batch into one projectile

    Methods:
        enter                 : Enter the MazeDesign
        exit                  : Leave the MazeDesign
        framestarted          : Per-frame game logic updates
        keyPressed/keyReleased: Player keyboard controls
        keyRepeat             : Repeatable key commands
        mousePressed/Released/
            Moved/Dragged     : Player mouse controls
    '''

    __metaclass__ = Singleton
        
    def __init__(self, renderer, sceneManager, soundManager, debugging):
        super(MazeDesign, self).__init__()
        self.soundManager = soundManager
        self.renderer = renderer
        self.ceguiSystem = CEGUI.System.getSingleton()
        self.sceneManager = sceneManager
        self.sfx = SFX(self.soundManager) # could inherit this also
        self.timeStandStill = False
        self.debugTimerReset = 0.1
        self.debugTimer = self.debugTimerReset
        self.debugText = ""
        self.keyTimerReset = 0.10 
        self.keyTimer = self.keyTimerReset
        self.mouseTimerReset = 0.20 # 1/5 sec
        self.mouseTimer = self.mouseTimerReset
        self.guiTimerReset = 0.10
        self.guiTimer = self.guiTimerReset
        self.showDebug = False
        self.showHelp = False
        self.sfxMute = True
        self.keyDown = {}
        self.keyDown[OIS.KC_LSHIFT] = None
        self.mouseDown = {}
        self.mouseX = 0
        self.mouseY = 0
        self.cursorX = 0
        self.cursorY = 0
        self.cursorXoffset = 0
        self.cursorYoffset = 0
        self.currentMaze = None

        self.root = ogre.Root.getSingleton()

        # CEGUI
        self.renderWindow  = self.root.getAutoCreatedWindow() 
        self.windowManager = CEGUI.WindowManager.getSingleton()
        self.guiIconSet   = CEGUI.ImagesetManager.getSingleton().getImageset("HUD_icons")
        self.guiTileSet = CEGUI.ImagesetManager.getSingleton().createImageset("MazeDesign.imageset")
        self.guiSheet = CEGUI.WindowManager.getSingleton().loadWindowLayout("MazeDesign.layout")

    def enter(self):
        self.quit = False

        self.ceguiSystem.setGUISheet(self.guiSheet)
        self.guiMouseX     = self.windowManager.getWindow( "MazeDesign/Console/Maze/MouseX")
        self.guiMouseY     = self.windowManager.getWindow( "MazeDesign/Console/Maze/MouseY")
        self.guiFPS        = self.windowManager.getWindow( "MazeDesign/FPS")
        self.guiHelp       = self.windowManager.getWindow( "MazeDesign/Help")
        self.guiHelp.hide()

        # Setup Dashboard
        self.helpLabel     = self.windowManager.getWindow( "MazeDesign/Console/Maze/Help/Label")
        self.nameLabel     = self.windowManager.getWindow( "MazeDesign/Console/Maze/Name/Label")
        self.name          = self.windowManager.getWindow( "MazeDesign/Console/Maze/Name")
        self.authorLabel   = self.windowManager.getWindow( "MazeDesign/Console/Maze/Author/Label")
        self.author        = self.windowManager.getWindow( "MazeDesign/Console/Maze/Author")
        self.mazeSizeLabel = self.windowManager.getWindow( "MazeDesign/Console/Maze/Size/Label")
        self.mazeSize      = self.windowManager.getWindow( "MazeDesign/Console/Maze/Size")
        self.mazeTypeLabel = self.windowManager.getWindow( "MazeDesign/Console/Maze/Type/Label")
        self.mazeType      = self.windowManager.getWindow( "MazeDesign/Console/Maze/Type")
        self.mazeDescLabel = self.windowManager.getWindow( "MazeDesign/Console/Maze/Desc/Label")
        self.mazeDesc      = self.windowManager.getWindow( "MazeDesign/Console/Maze/Desc")

        # Setup Tooltips
        self.ceguiSystem.setDefaultTooltip("TaharezLook/Tooltip")
        self.toolTip = self.ceguiSystem.getDefaultTooltip()
        #tip.displayTime=0.80
        self.toolTip.hoverTime = 0.5
        self.toolTip.fadeTime=0.2
        
        # debug stats overlay
        #self.statsOverlay = ogre.OverlayManager.getSingleton().getByName('MazeDesign/StatsOverlay')
        #self.statsOverlay.hide()

        # Maze Info
        self.helpLabel.setText("F1 for HELP")
        self.nameLabel.setText("Name:")
        self.authorLabel.setText("Author:")
        self.mazeSizeLabel.setText("Size:")
        self.mazeTypeLabel.setText("Type:")
        self.mazeDescLabel.setText("Desc:")

#        self.currentMaze = Maze("arena")
        self.currentMaze = Maze("simple", self.sceneManager)
        if not self.currentMaze.readMazeFile("combat"):
            self.name.setText(str(self.currentMaze.getMazeName()))
            self.author.setText("**NOT LOADED**")
            self.mazeSize.setText("30 x 30")
            self.mazeType.setText("**NOT LOADED**")
            self.mazeDesc.setText("**NOT LOADED**")
        else:
            self.name.setText(str(self.currentMaze.getMazeName()))
            self.author.setText(str(self.currentMaze.getMazeAuthor()))
            self.mazeSize.setText(str(int(self.currentMaze.getMazeWidth()))+" x "+str(int(self.currentMaze.getMazeHeight())))
            self.mazeType.setText(str(self.currentMaze.getMazeType()))
            self.mazeDesc.setText(str(self.currentMaze.getMazeDesc()))      

        # init map
        # Setup a ground plane.
        plane = ogre.Plane ((0, 1, 0), 0)
        meshManager = ogre.MeshManager.getSingleton ()
        meshManager.createPlane ('Ground', 'General', plane, 5760, 5760, 20, 20, True, 1, 30, 30, (0, 0, 1))
        ent = self.sceneManager.createEntity('GroundEntity', 'Ground')
        self.sceneManager.getRootSceneNode().createChildSceneNode().attachObject (ent)
        ent.setMaterialName ('Arena/Tile/OutOfBounds')
        ent.castShadows = False
        
        # light
        light = self.sceneManager.createLight ('DirectionalLight')
        light.type = ogre.Light.LT_DIRECTIONAL
        light.diffuseColour = (1, 1, 1)
        light.specularColour = (.75, .75, .75)
        light.direction = (0, -1, 1)
        # fog

        # shadows
        #self.sceneManager.ambientLight = (0.01, 0.01, 0.01)
        self.sceneManager.ambientLight = (0, 0, 0)
        self.sceneManager.shadowTechnique = ogre.SHADOWTYPE_STENCIL_MODULATIVE

        # Create Location Tiles
        genericTile = ogre.Plane ((0, 1, 0), 0)
        meshManager.createPlane ('FloorTile', 'General', genericTile, 192, 192, 1, 1, True, 1, 1, 1, (0, 0, 1))
        meshManager.createPlane ('AmmoTile', 'General', genericTile, 192, 192, 1, 1, True, 1, 1, 1, (0, 0, 1))
        meshManager.createPlane ('FuelTile', 'General', genericTile, 192, 192, 1, 1, True, 1, 1, 1, (0, 0, 1))
        meshManager.createPlane ('ArmorTile', 'General', genericTile, 192, 192, 1, 1, True, 1, 1, 1, (0, 0, 1))

        #
        # Draw Map: Includes walls currently...
        #
        for gridY in range(0,self.currentMaze.getMazeHeight()):
            for gridX in range(0,self.currentMaze.getMazeWidth()):            
                mazeData = self.currentMaze.getMazeBoxXY(gridX,gridY)
                xpos = mazeData[0]    # X Position
                ypos = mazeData[1]    # Y Position
                north = mazeData[2]   # North Wall, indestructable = -1, 1+ is armor for wall
                west = mazeData[3]    # West Wall, indestructable = -1, 1+ is armor for wall
                owner = mazeData[4]   # Who owns the tile
                inmaze = mazeData[5]  # In the maze?
                loctype = mazeData[6]
                val = mazeData[7]
                if north != 0:
                    ent = self.sceneManager.createEntity("wall_N_"+str(xpos)+"x"+str(ypos), "wall.mesh")
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode("wall_N_"+str(xpos)+"x"+str(ypos), ((xpos * 192)-(int(self.currentMaze.getMazeWidth()) * 96),0,(ypos * 192)-(int(self.currentMaze.getMazeHeight()) * 96)))
                    posNode.attachObject (ent)
                    ent.setMaterialName ("Arena/Wall")
                if west != 0:
                    ent = self.sceneManager.createEntity("wall_W_"+str(xpos)+"x"+str(ypos), "wall.mesh")
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode("wall_W_"+str(xpos)+"x"+str(ypos), ((xpos * 192)-(int(self.currentMaze.getMazeWidth()) * 96),0,((ypos+1) * 192)-(int(self.currentMaze.getMazeHeight()) * 96)))
                    posNode.attachObject (ent)
                    posNode.yaw(ogre.Degree(90))
                    ent.setMaterialName ("Arena/Wall")   
                if loctype == "AMMO":
                    ent = self.sceneManager.createEntity("ammo_at_"+str(xpos)+"x"+str(ypos), "AmmoTile")
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode("ammo_at_"+str(xpos)+"x"+str(ypos), ((xpos * 192)-(int(self.currentMaze.getMazeWidth()-1) * 96),.5,(ypos * 192)-(int(self.currentMaze.getMazeHeight()-1) * 96)))
                    posNode.attachObject (ent)
                    posNode.yaw(ogre.Degree(180))
                    ent.setMaterialName ("Arena/Tile/Ammo")  
                    ent.castShadows = False
                elif loctype == "FUEL":
                    ent = self.sceneManager.createEntity("fuel_at_"+str(xpos)+"x"+str(ypos), "FuelTile")
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode("fuel_at_"+str(xpos)+"x"+str(ypos), ((xpos * 192)-(int(self.currentMaze.getMazeWidth()-1) * 96),.5,(ypos * 192)-(int(self.currentMaze.getMazeHeight()-1) * 96)))
                    posNode.attachObject (ent)
                    posNode.yaw(ogre.Degree(180))
                    ent.setMaterialName ("Arena/Tile/Fuel")  
                    ent.castShadows = False
                elif loctype == "ARMOR":
                    ent = self.sceneManager.createEntity("armor_at_"+str(xpos)+"x"+str(ypos), "ArmorTile")
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode("armor_at_"+str(xpos)+"x"+str(ypos), ((xpos * 192)-(int(self.currentMaze.getMazeWidth()-1) * 96),.5,(ypos * 192)-(int(self.currentMaze.getMazeHeight()-1) * 96)))
                    posNode.attachObject (ent)
                    posNode.yaw(ogre.Degree(180))
                    ent.setMaterialName ("Arena/Tile/Armor")  
                    ent.castShadows = False
                elif inmaze == True:
                    ent = self.sceneManager.createEntity("Floor_at_"+str(xpos)+"x"+str(ypos), "FloorTile")
                    posNode = self.sceneManager.getRootSceneNode().createChildSceneNode("Floor_at_"+str(xpos)+"x"+str(ypos), ((xpos * 192)-(int(self.currentMaze.getMazeWidth()-1) * 96),.5,(ypos * 192)-(int(self.currentMaze.getMazeHeight()-1) * 96)))
                    posNode.attachObject (ent)
                    posNode.yaw(ogre.Degree(180))
                    ent.setMaterialName ("Arena/Tile/Floor")  
                    ent.castShadows = False

        # create the overhead camera node/pitch node
        self.OHCamera = self.sceneManager.createCamera("OHCamera")
        self.OHCamNode = self.sceneManager.getRootSceneNode().createChildSceneNode("OHCamNode")
        self.OHCamPitchNode = self.OHCamNode.createChildSceneNode("OHPitchNode")
        self.OHCamPitchNode.attachObject(self.OHCamera)
        self.OHCamera.nearClipDistance = 5
        self.cameraView = "OH"

        # viewport
        self.viewport = self.renderWindow.addViewport(self.OHCamera)
        self.viewHeight = self.viewport.getActualHeight()
        self.viewWidth = self.viewport.getActualWidth()      
        self.OHXoffset = 0 # (self.viewWidth - self.viewHeight)/4 # assuming width >= height
        #
        # Switch the 1 to a zero for a intersting effect, might use this for elevation.
        #
        self.OHCamNode.setPosition(self.OHXoffset, 3600, 1) # Normal Size 900 want to pull cameria in/out with mouse wheel
        position = (self.OHXoffset,0,0)
        self.OHCamera.lookAt(position)

        print "Entered MazeDesign."
        
    def exit(self):
        self.sceneManager.clearScene()
        self.sceneManager.destroyAllCameras()
        self.renderWindow.removeAllViewports()
        
    def resume(self):
        self.viewport.setBackgroundColour(ogre.ColourValue(0.0, 0.0, 1.0))
        
    def frameStarted(self, evt):
        ''' All the action happens here '''
        time = evt.timeSinceLastFrame
        self.ceguiSystem.getSingleton().injectTimePulse(time)
        if self.timeStandStill:
            return True
        # Update cursor XY to transform into rough grid coordinates
        camPos = self.OHCamNode.getPosition()
        if ((camPos.x * 100)%192 == 0):
            self.cursorXoffset = 0
        elif ((camPos.x * 100)%192 == 17):
            self.cursorXoffset = 32
        elif ((camPos.x * 100)%192 == 33):
            self.cursorXoffset = 64
        elif ((camPos.x * 100)%192 == 50):
            self.cursorXoffset = 96
        elif ((camPos.x * 100)%192 == 67):
            self.cursorXoffset = 128
        elif ((camPos.x * 100)%192 == 83):
            self.cursorXoffset = 160
        if ((camPos.z * 100)%192 == 0):
            self.cursorYoffset = 0
        elif ((camPos.z * 100)%192 == 17):
            self.cursorYoffset = 32
        elif ((camPos.z * 100)%192 == 33):
            self.cursorYoffset = 64
        elif ((camPos.z * 100)%192 == 50):
            self.cursorYoffset = 96
        elif ((camPos.z * 100)%192 == 67):
            self.cursorYoffset = 128
        elif ((camPos.z * 100)%192 == 83):
            self.cursorYoffset = 160
        self.cursorX = ((((camPos.x + (192 - self.cursorXoffset))/192)+14) + ((self.mouseX - self.viewWidth/2)/81.32))
        self.cursorY = ((((camPos.z + (192 - self.cursorYoffset) +1)/192)+14) + ((self.mouseY - self.viewHeight/2)/65.63))
        if self.cursorY < 0:
            self.cursorY = 0
        elif self.cursorY > 29:
            self.cursorY = 29

        # Debug
        if self.showDebug:
            self.debugTimer -= time
            if self.debugTimer < 0:
                self.debugTimer = self.debugTimerReset
                self.updateStatistics()
        # Update GUI
        self.guiTimer -= time
        if self.guiTimer < 0:
            self.guiTimer = self.guiTimerReset
            self.guiFPS.setText(str(int(self.renderWindow.getLastFPS())) + ' FPS')
            self.guiMouseX.setText(str(int(self.cursorX)))
            self.guiMouseY.setText(str(int(self.cursorY)))

        # Key Repeat
        self.keyTimer -= time
        if self.keyTimer < 0:
            for key in self.keyDown.keys():
                self.keyRepeat(key)
            self.keyTimer = self.keyTimerReset
        self.mouseTimer -= time
        if self.mouseTimer < 0:
            for key in self.mouseDown.keys():
                if self.mouseDown[key]:
                    self.mouseRepeat(key)
            self.mouseTimer = self.mouseTimerReset
        if self.quit:
            from Game.GameStates.MainMenu import MainMenu
            GameManager().changeState(MainMenu())
        return True

    def frameEnded(self, evt):
        return True

    def keyPressed(self, evt):
        # CEGUI injection
        ceguiSystem = CEGUI.System.getSingleton()
        ceguiSystem.injectKeyDown(evt.key)
        ceguiSystem.injectChar(evt.text)
        if self.showHelp:
            if evt.key == OIS.KC_ESCAPE or evt.key == OIS.KC_F1:
                self.timeStandStill = False
                self.guiHelp.hide()
                self.showHelp = False            
        elif evt.key == OIS.KC_F1:
            self.timeStandStill = True
            self.guiHelp.show()
            self.showHelp = True
        elif evt.key == OIS.KC_ESCAPE:
            #from MenuStates import PauseState
            #GameManager().pushState(PauseState())
            self.quit = True
        elif evt.key == OIS.KC_P:
            self.timeStandStill = not self.timeStandStill
        elif evt.key == OIS.KC_V:
            self.sfxMute = not self.sfxMute
        # non-repeating keys above
        else:
            if evt.key not in self.keyDown:
                self.keyRepeat(evt.key)
            self.keyDown[evt.key] = evt
            self.keyTimer = self.keyTimerReset

    def keyRepeat(self,key):
        camPos = self.OHCamNode.getPosition()
        if key == OIS.KC_UP:
            if (camPos.z > -1407):
                self.OHCamNode.translate(0,0,-32)
        elif key == OIS.KC_DOWN:
            if (camPos.z < 1633):
                self.OHCamNode.translate(0,0,32)
        elif key == OIS.KC_LEFT:
            if (camPos.x > -896):
                self.OHCamNode.translate(-32,0,0)
        elif key == OIS.KC_RIGHT:
            if (camPos.x < 896):
                self.OHCamNode.translate(32,0,0)
        
    def keyReleased(self, evt):
        CEGUI.System.getSingleton().injectKeyUp(evt.key)
        if evt.key in self.keyDown:
            del self.keyDown[evt.key]

    def mousePressed(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonDown(self.convertButton(id))
        self.mouseDown[id] = True
        self.mouseRepeat(id)
        self.mouseTimer = self.mouseTimerReset

    def mouseRepeat(self, id):
        mousePos = CEGUI.MouseCursor.getSingleton().getPosition()
        if id == "MB_Left":
            pass
        if id == "MB_Middle":
            pass
        if id == "MB_Right":
            pass

    def mouseReleased(self, evt, buttonID):
        id =  buttonID.name
        CEGUI.System.getSingleton().injectMouseButtonUp(self.convertButton(id))
        self.mouseDown[id] = False

    def mouseMoved(self, evt):
        state = evt.get_state()
        CEGUI.System.getSingleton().injectMouseMove(evt.get_state().X.rel, evt.get_state().Y.rel)
        mousePos = CEGUI.MouseCursor.getSingleton().getPosition()
        self.mouseX = mousePos.d_x
        self.mouseY = mousePos.d_y

    def mouseDragged(self, evt):
        pass

