#!/usr/bin/env python
# Jukebox.py

# Copyright (C) 2008  Ryan Ashmore
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the sound effects generator.

Author: Ryan Ashmore

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

class SFX(object):
    def __init__(self, soundManager):
        self.soundManager = soundManager

    def shotFired(self, soundFile, position):
        s = self.soundManager.createSound("shot", soundFile, False)
        s.setPosition(position)
        s.play()
        self.soundManager.destroySound(s)
        self.soundManager._releaseSource(s)
