#!/usr/bin/env python
# Core.py

# Copyright (C) 2008  Colin McCulloch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This module will contain the main game code, including that for the 
game manager which is responsible for the main application startup, 
as well as the switching of game states. 

Does this violate the one job per class principal?

Author: Colin "Zyzle" McCulloch

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import ogre.io.OIS as OIS

from Game.MetaClasses import Singleton
from Game.Listeners import GameFrameListener, GameKeyListener, \
                            GameMouseListener

class GameManager(object):
    """This game manager class is responsible for the startup of the
    application, including the creation of frame and keyboard
    listeners, the transition of state changes and the forwarding
    of the frame and keyboard events to the game states.
    
    Personally I think this class is doing to much, think we need to 
    encapsulate the behaviour elsewhere.
    """
    __metaclass__ = Singleton
    
    def __init__(self):
        """Basic constructor for the Game manager, creates dummy root
        and inputmanager objects and sets up the states stack list"""
        self.root = 0
        self.inputManager = 0
        self.states = []

    def __del__(self):
        """Destructor for the GameManager, this needs proper
        implementation"""
        pass
    
    def start(self, state):
        """This method is the basic startup sequence for the game this 
        creates the ogre root object calls methods to create the
        render system and resource defenition, also makes the frame and
        key listener objects, then changes to the start state and
        begins rendering... phew!"""
        
        self.root = ogre.Root()
        
        self.defineResources()
        self.setupRenderSystem()
        
        self.frameListener = GameFrameListener(self)
        self.root.addFrameListener(self.frameListener)
        
        self.keyListener = GameKeyListener(self, self.RenderWindow)
        self.mouseListener = GameMouseListener(self, self.RenderWindow)
        
        self.mouse = self.mouseListener.mouse
        self.keyboard = self.keyListener.keyboard
        
        self.changeState(state())
        self.root.startRendering()
        
    def changeState(self, state):
        """calls .exit on any state currently at top of the states
        stack, appends the given state to the stack and calls its
        .enter method"""
        if len(self.states):
            oldState = self.states.pop()
            oldState.exit()
        
        self.states.append(state)
        print self.states
        state.enter()
        
    def pushState(self, state):
        """Like change state only it calls .pause on the stack top
        state rather than .exit"""
        if len(self.states):
            self.states[-1].pause()
            
        self.states.append(state)
        print self.states
        state.enter()
        
    def popState(self):
        """removes a state from stack top, calls its .exit then
        .resumes the new stack top state"""
        if len(self.states):
            oldState = self.states.pop()
            oldState.exit()
            
        print self.states
        
        if len(self.states):
            self.states[-1].resume()
            
    def keyPressed(self, evt):
        """forward key events to current stack top state"""
        self.states[-1].keyPressed(evt)
    
    def keyReleased(self, evt):
        """forward key events to current stack top state"""
        self.states[-1].keyReleased(evt)
    
    def frameStarted(self, evt):
        """forward frame events to current stack top state. also
        makes a call to the keyboard objects capture method"""
        self.keyboard.capture()
        self.mouse.capture()
        
        successfulFrame = self.states[-1].frameStarted(evt)
        return successfulFrame
    
    def frameEnded(self, evt):
        """forward frame events to current stack top state"""
        successfulFrame = self.states[-1].frameEnded(evt)
        return successfulFrame
        
    def mouseMoved(self, evt):
        self.states[-1].mouseMoved(evt)
    
    def mousePressed(self, evt, buttonID):
        self.states[-1].mousePressed(evt, buttonID)
    
    def mouseReleased(self, evt, buttonID):
        self.states[-1].mouseReleased(evt, buttonID)
    
    def setupRenderSystem(self):
        """Displays the default ogre config dialog.  exits game if
        user cancels"""
        if not self.root.restoreConfig():
            if not self.root.showConfigDialog():
                raise Exception("User cancelled config dialog")
        
        self.RenderWindow = self.root.initialise(True, "GameWindow")
        ogre.ResourceGroupManager.getSingleton().initialiseAllResourceGroups()
            
    def defineResources(self):
        """Loads in the ogre config file and processes it giving ogre
        all the defined resource locations"""
        conf = ogre.ConfigFile()
        conf.load("resources.cfg")
        seci = conf.getSectionIterator()
        
        while seci.hasMoreElements():
            sectionName = seci.peekNextKey()
            settings = seci.getNext()
            
            for item in settings:
                typeName = item.key
                archName = item.value
                ogre.ResourceGroupManager.getSingleton().addResourceLocation \
                (archName, typeName, sectionName)
