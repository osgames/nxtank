#!/usr/bin/env python
# Weapon.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Weapon class.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import Game.Equipment as equip
from Game.Shot import Shot
from math import sqrt

class Weapon:
    '''Weapon class
    Attributes:
        name       (string): weapon name from Equipment tables
        material   (material): weapon material for team colors
        posNode    (node)  : current position
        turretOffset(tuple): turret mount point
        muzzleNode (node)  : end of barrel
        particle (particle): particle system attached to muzzle
        particleRate(float): rate of particle emission
        particleSize(float): size of particles
        ent        (entity): entity for self.mesh
        entName    (entity): unique entity name
        number     (int)   : weapon number from .tank file
        tank       (Tank)  : tank mounting this weapon
        turret     (Turret): turret mounting this weapon
        damage     (float) : damage points
        range      (float) : range in pixels
        ammo       (float) : current ammo load
        maxAmmo    (float) : starting ammo load
        reload     (float) : reload time in ticks
        reloading  (float) : reload timer in ticks
        speed      (float) : Shot speed, feet per second
        weight     (float) : weapon mass
        space      (float) : weapon space
        frames     (float) : lifetime in ticks, we shouldn't need this
        heat       (float) : heat generated per shot
        ammoCost   (float) : cost per shot reloaded by an ammo landmark
        cost       (float) : weapon cost
        refillSpeed(float) : time to reload 1 ammo in ticks
        height     (string): launch height, determines where target is struck
        length     (float) : weapon length
        armed      (bool)  : armed/disarmed
		shotMesh   (string): Mesh to use for projectile
		abbrev     (string): weapon's abbreviated name
        physWorld  (world) : ODE world
        physSpace  (space) : ODE space
        reShotList (list)  : list of recyclable shots

    Methods:
        fire()                 : Fire weapon, spawn shot if necessary and set it in motion
        toggle('on'|'off'|None): Toggle armed state to on, off, or flip
    '''
    def __init__(self, name, number, mount, tank, debugging): 
        self.name = name
        self.number = number
        self.mount = mount
        self.tank = tank
        self.debugging = debugging
        self.turret = None
        self.reShotList = []
        # inherit equip table stats
        stat = equip.weapon(name)
        self.damage = stat.damage
        self.type = stat.type
        self.range = stat.range
        self.maxAmmo = stat.ammo
        self.ammo = self.maxAmmo
        self.reload = stat.reload
        self.reloading = 0
        self.speed = stat.speed
        self.mass = stat.mass
        self.space = stat.space
        self.frames = stat.frames
        self.heat = stat.heat
        self.ammoCost = stat.ammoCost
        self.cost = stat.cost
        self.refillSpeed = stat.refillSpeed
        self.height = stat.height
        self.length = stat.length
        self.armed = True
        self.shotMesh = stat.shotMesh
        self.abbrev = stat.abbrev
        self.physWorld = self.tank.physWorld
        self.physSpace = self.tank.physSpace
        self.material = ogre.MaterialManager.getSingleton().create( self.name,
                        ogre.ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME)
        self.entName = self.tank.name + "_Weapon" + str(self.number)
        self.posNode = self.tank.sceneManager.createSceneNode(self.entName)
        self.muzzleNode = self.posNode.createChildSceneNode(self.entName +"_muzzle", (self.length,0,0))
        self.muzzleNode.setOrientation(sqrt(0.5),0,0,-sqrt(0.5))
        self.ent = self.tank.sceneManager.createEntity(self.entName, self.name +".mesh")
        self.posNode.attachObject(self.ent)
        self.particle = None
        self.particleRate = 0
        self.particleSize = 0
        if self.name == "Flame Thrower":        
            self.particle = self.tank.sceneManager.createParticleSystem(self.entName, 'Weapon/FlameThrower')
            self.muzzleNode.attachObject(self.particle)
            #self.particle.setEmitting(False)
            self.particleSize = self.particle.getDefaultWidth()
            self.particleRate = self.particle.getEmitter(0).getEmissionRate()
            self.particle.getEmitter(0).setEmissionRate(0)

    def destroy(self):
        self.turret.posNode.removeAndDestroyChild(self.entName)
        self.tank.sceneManager.destroyEntity(self.entName)
        self.tank.sceneManager.destroyParticleSystem(self.entName)

    def fire(self):
        position = self.muzzleNode._getDerivedPosition() # offset to cannon mount
        weaponDir = self.posNode._getDerivedOrientation()
        velocity = weaponDir * ogre.Vector3(1, 0, 0) * self.speed
        if self.name != "Flame Thrower" and self.name != "Acid Sprayer":
            velocity += self.tank.velocity
        if len(self.reShotList):
            shot = self.reShotList.pop()
        else:
            self.tank.shotNumber += 1
            shotName = "Shot_"+ self.tank.name + "_" + str(self.tank.shotNumber)
            shot = Shot(self.tank, self, shotName, self.debugging)
            self.reShotList.append(shot)
        shotBatch = self.tank.parent.shotBatch
        if shotBatch > self.ammo:
            shotBatch = self.ammo
        shot.launch(position, weaponDir, velocity, shotBatch)
        self.reloading = self.reload * shotBatch
        self.tank.heat += self.heat * shotBatch
        self.ammo -= 1 * shotBatch
        if self.particle:
            #self.particle.setEmitting(True)
            self.particle.getEmitter(0).setEmissionRate(self.particleRate)

    def toggle(self, switch=None):
        if switch is 'on':
            self.armed = True
        elif switch is 'off':
            self.armed = False
        else:
            self.armed = not self.armed
