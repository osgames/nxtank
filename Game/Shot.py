#!/usr/bin/env python
# Shot.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Projectile class.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import ogre.physics.OgreOde as OgreOde
from random import random
import Game.Utility as util

class Shot(ogre.UserDefinedObject):
    '''Projectile class

    Attributes:
        tank      (Tank)    : parent Tank
        weapon    (Weapon)  : launcher
        name      (string)  : name
        team      (int)     : team
        RangeTimer(int)     : time in sec range before shot disappears
        maxRangeTime(int)   : lifetime in sec
        mesh      (mesh)    : visible mesh
        material  (material): material for self.mesh
        ent       (entity)  : entity for self.mesh
        posNode   (node)    : current position
        alive     (bool)    : currently in play
        physWorld  (world)  : ODE world
        physSpace  (space)  : ODE space
        physBoxMass(boxMass): ODE boxMass
        physSize   (vector) : boundingBox dimensions
        physOffset (vector) : body center offset from entity center
        physGeom   (geom)   : ODE geometry
        physType   (string) : collision object type
        physMass   (float)  : mass scaled 0.1..10 in Arena

    Methods:
        launch(tank, weapon, position, direction, velocity, number) 
                   : launch projectile from specified weapon of tank
        move(time) : time-scaled projectile logic
        hide()     : remove from play
        show()     : return to play
    '''

    def __init__(self, tank, weapon, name, debugging):
        ogre.UserDefinedObject.__init__(self)
        self.tank = tank
        self.weapon = weapon
        self.name = name
        self.debugging = debugging
        self.team = tank.team
        self.sceneManager = self.tank.sceneManager
        try:
            self.posNode = self.sceneManager.getRootSceneNode().createChildSceneNode(self.name)
        except: # OgreODE bug workaround bug workaround
            self.posNode = self.sceneManager.getRootSceneNode().getChild(self.name)
        self.physWorld = tank.physWorld
        self.physSpace = tank.physSpace
        self.physBody = OgreOde.Body(self.physWorld)
        self.posNode.attachObject(self.physBody)
        self.physBody.setAffectedByGravity(False)
        self.physType = "SHOT"
        self.type = weapon.type
        self.mesh = weapon.shotMesh
        self.ent = self.sceneManager.createEntity(self.name, self.mesh)
        self.material = ogre.MaterialManager.getSingleton().create( self.name,
                        ogre.ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME)
        util.setMaterial(self.ent, self.material, None, tank.teamColor[tank.team], self.debugging.materials)
        self.ent.castShadows = False
        self.posNode.attachObject (self.ent)
        # Physics
        box = self.ent.getBoundingBox()
        boxMin = box.getMinimum()
        boxMax = box.getMaximum()
        self.physSize = ogre.Vector3(boxMax.x - boxMin.x, boxMax.y - boxMin.y, boxMax.z - boxMin.z)   
        self.physOffset = ogre.Vector3((boxMax.x + boxMin.x)/2,(boxMax.y + boxMin.y)/2,(boxMax.z + boxMin.z)/2)
        self.physBoxMass = OgreOde.BoxMass(1.0, self.physSize)
        self.physBody.setMass(self.physBoxMass)
        self.physGeom = OgreOde.BoxGeometry(self.physSize, self.physWorld, self.physSpace)
        self.physGeom.setBody(self.physBody)
        self.physGeom.setOffsetPosition(self.physOffset)
        self.physGeom.setUserObject(self)
        util.setBitfields(self.physGeom, self.physType, self.tank.team)
        self.ent.setUserObject(self.physGeom)
        
    def launch(self, position, direction, velocity, batch):
        self.batch = batch
        self.damage = self.weapon.damage * self.batch
        self.rangeTimer = float(self.weapon.frames)/15
        self.maxRangeTime = self.rangeTimer
        self.physGeom.enable()
        self.posNode.showBoundingBox(False) 
        self.posNode.setVisible(True)
        self.physBody.setPosition(position)
        self.physBody.setOrientation(direction)
        self.physBody.setLinearVelocity(velocity)
        self.alive = True
        self.posNode.setVisible(True)
        if self.weapon.name == "Flame Thrower":        
            self.ent.setVisible(False)
        self.tank.shotList.append(self)

    def hide(self):
        ''' this is equivalent to 'Remove' or 'Destroy'. ogre recommends not destroying
        scene nodes during the game, so we de-activate instead. the actor can be
        re-used if needed without recreating it. in the game you can easily keep a 
        tracker of hidden actors and only create new ones when you need to.
        '''
        if self.weapon.particle:
            range = self.maxRangeTime - self.rangeTimer
            self.weapon.particle.getEmitter(0).setTimeToLive(range)
        self.alive = False
        self.posNode.setVisible(False)
        if self in self.tank.shotList:    # this shouldn't be necessary
            self.weapon.reShotList.append(self)
            self.tank.shotList.remove(self)
            self.physBody.setPosition(ogre.Vector3(10000+random()*10000, 10000+random()*10000, 10000+random()*10000))
            self.physGeom.disable()

    def cleanup(self):
        self.sceneManager.destroyEntity(self.name)
        try:
            self.sceneManager.destroySceneNode(self.name)
        except:
            print "Failed to destroy sceneNode "+self.name

    def move(self, time):        
        # guided projectiles go here
        self.rangeTimer -= time
        if self.rangeTimer <= 0:
            self.hide()

