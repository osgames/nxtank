#!/usr/bin/env python
# MetaClases.py

# Copyright (C) 2008  Colin McCulloch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This class holds the metaclass types used in the game Bloop.  
These are currently;

Singleton

Author: Colin "Zyzle" McCulloch

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

class Singleton(type):
    """This metaclass allows objects to inherit the behaviour of a 
    singleton, i.e. only one instance of the class can be created.
    Whats good about this method is that this happens transparently,
    everytime SomeSingletonClass() constructor is called a check is 
    performed to determine if an instance exists and if so it is 
    returned rather than a new instance being created"""
    def __init__(cls,name,bases,dic):
        super(Singleton,cls).__init__(name,bases,dic)
        cls.instance=None
        
    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance=super(Singleton,cls).__call__(*args,**kw)
        return cls.instance