#!/usr/bin/env python
# Turret.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Turret class.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import operator

class Turret:
    '''Turret class
    Attributes:
        name     (string) : name
        tank     (Tank)   : tank mounting this turret
        ent      (entiry) : turret entity
        material(material): Material used for team color

        aim       (radian): direction of turret.ent
        newAim    (radian): esired direction of turret.ent
        
        turnMax    (float): max turn rate in radians/sec
        turnAccel  (float): turn acceleration in radians/sec
        turnRate   (float): current turn rate in radians/sec
    '''
    def __init__(self, name, tank, posNode, debugging, weaponList=None):
        self.name = name
        self.posNode = posNode
        self.debugging = debugging
        self.ent = None
        if weaponList:
            self.ent = tank.sceneManager.createEntity(self.name, "turret.mesh")
            self.posNode.attachObject(self.ent)
            self.material = ogre.MaterialManager.getSingleton().create(self.name,
                            ogre.ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME)
            #Turret mount locations
            mount = []
            mount.append(None)
            mount.append((0, 1.8,  0))   #1
            mount.append((0, 1.8, -2.1)) #2
            mount.append((0, 1.8,  2.1)) #3
            mount.append((0, 4.5,  0))     #4
            mount.append((0, 4.5, -2.1))   #5
            mount.append((0, 4.5,  2.1))   #6
            weaponList.sort(reverse=True, key=operator.attrgetter('length'))
            turretWeaponCount = 0
            mass = 500
            for weapon in weaponList:
                turretWeaponCount += 1
                weapon.turret = self
                mass += weapon.mass
                self.posNode.addChild(weapon.posNode)
                weapon.posNode.translate(mount[turretWeaponCount])
                weapon.turretOffset = mount[turretWeaponCount]
        else: # gunnerCam
            mass = 1200
        self.turnMax   = float(600)/mass * 15
        self.turnAccel = float(self.turnMax)/10
        self.turnRate  = 0
        self.aim = 0
        self.newAim = 0