#!/usr/bin/env python
# Utility.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Common Utility functions

Author: Shaun McGee

Last Edit By: "$Author$"

__version__ = "$Revision$"
__date__ = "$Date$"

Methods:

    diffAngle       (vector1,vector2)         : Return radian angle between two unit vectors
    vectorPlusAngle (vector,angle)            : Returns vector result of adding angle to vector 
    vectorToAngle   (vector)                  : Returns radian
    #sign           (float)                   : Finally, there's a builtin for this!
    #minMax         (var1,var2,floor,ceiling) : Returns floor <= (var1 + var2) <= ceiling
                                                Uses 0..1 by default
    strToInt        (string)                  : converts string to int, default floor is 0
    wrapPi          (radian)                  : wrap radian to -pi..pi
    debugAxis(sceneMgr, name, node, offset)   : Draws axis debug object
    debugCube(sceneMgr, name, node, offset)   : Draws textured cube debug object
    setMaterial(ent, material, texture, color, debug): Set entity and subentity texture and team colors
    setBitfields    (physGeom, type, team)    : set physics category and collision bitfields
'''

from math import atan2, sin, cos, copysign, pi

import ogre.renderer.OGRE as ogre

def diffAngle(vector1,vector2):
    angle1 = atan2(vector1.z, vector1.x)
    angle2 = atan2(vector2.z, vector2.x)
    absAngle1 = abs(angle1)
    absAngle2 = abs(angle2)
    if (angle1 * angle2) < 0 and (absAngle1 + absAngle2) > pi:
        diff = copysign(1,angle1) * (2*pi - (absAngle1 + absAngle2))
    else:
        diff = angle2 - angle1
    return diff # the Preciousssss...

def vectorPlusAngle(vector,angle):
    newAngle = atan2(vector.z,vector.x) + angle
    newVector = ogre.Vector3(cos(newAngle),0,sin(newAngle))
    newVector.normalise()
    return newVector

#def vectorToAngle(vector):
#    return atan2(vector.z,vector.x)

#def angleToVector(angle):
#    return ogre.Vector3(cos(angle),0,sin(angle))

#def minMax(var1,var2,floor=0,ceiling=1):
#    return min(max(var1 + var2,floor),ceiling)

def strToInt(string,floor=0):
    try:
        integer = int(string)
    except:
        integer = 0
    if integer < floor:
        integer = floor
    return integer

def wrapPi(radian):
    while radian > pi:
        radian -= 2 * pi
    while radian < -pi:
        radian += 2 * pi
    return radian

def debugAxis(sceneManager, name, node, offset=None):
    ent = sceneManager.createEntity(name, "axis.mesh")
    debugNode = node.createChildSceneNode(name)
    if offset:
        debugNode.translate(offset)
    debugNode.attachObject(ent)
    debugNode.setScale(10,10,10)
    return debugNode

def debugCube(sceneManager, name, node, offset=None):
    ent = sceneManager.createEntity(name, "cube.mesh")
    ent.setMaterialName ("Debug/TestCard")
    debugNode = node.createChildSceneNode(name, offset)
    debugNode.attachObject(ent)
    debugNode.setScale(0.1,0.1,0.1)
    return debugNode

def setMaterial(ent, material, texture, color, camo=False, debug=False):
    pass0 = material.getTechnique(0).getPass(0)
    pass0.removeAllTextureUnitStates()
    if debug:
        print "Entity " + ent.name + " has " + str(ent.getNumSubEntities()) + " sub entities\n"
    if texture:
        pass0.createTextureUnitState(texture) 
    for subEntity in range(0,ent.getNumSubEntities()):
        if debug:
            print "This sub entity has material " + ent.getSubEntity(subEntity).getMaterialName() + "\n"
        if ent.getSubEntity(subEntity).getMaterialName() == "team_color" or camo:
            if debug:
                print "Team color submesh found, setting...\n"
            ent.getSubEntity(subEntity).setMaterial(material)
    pass0.setDiffuse(color) 
    pass0.setSpecular(0.08,0.08,0.08,12.5) 
    pass0.setAmbient(color) 
            
def setBitfields(physGeom, type, team=None):
    # Category: 32-bit field
    #   1    Team 0
    #   2    Team 1
    #   4    Team 2
    #   8    Team 3
    #  16    Team 4
    #  32    Team 5
    #  64    Team 6
    teams = 127
    tank  = 128
    shot  = 256
    wall  = 512
    if type == "WALL":
        physGeom.setCategoryBitfield(wall)
        physGeom.setCollisionBitfield(tank + shot)
    if type == "SHOT":
        physGeom.setCategoryBitfield(shot)
        # don't collide with my team
        physGeom.setCollisionBitfield(wall + teams - 2**team)
    if type == "TANK":
        physGeom.setCategoryBitfield(tank + 2**team)
        physGeom.setCollisionBitfield(tank + wall)
