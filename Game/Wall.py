#!/usr/bin/env python
# Wall.py

# Copyright (C) 2009 Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Projectile class.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import ogre.physics.OgreOde as OgreOde
import Game.Utility as util

class Wall(ogre.UserDefinedObject):
    '''Wall class

    Attributes:
        mesh      (mesh)    : visible mesh
        material  (material): material for self.mesh
        ent       (entity)  : entity for self.mesh
        posNode   (node)    : current position
        physGeom  (geom)    : collision geometry
        physType  (string)  : collision object type
        type      (string)  : Wall type: NORM, DEST, HALF
        side      (string)  : Wall side: N, W
        x         (int)     : box coordinate
        y         (int)     : box coordinate
        miniMe    (Wall)    : corresponding Wall on minimap
    '''

    def __init__(self, maze, sceneManager, x, y, side="N", type="NORM", length=1, physWorld=None, physSpace=None, minimap=None):
        ogre.UserDefinedObject.__init__(self)
        self.maze = maze
        self.type = type
        self.side = side
        self.x = x
        self.y = y
        self.miniMe = None
        self.sceneManager = sceneManager
        self.name = "Wall_"+str(side)+"_"+str(x*100+y)
        scale = ogre.Vector3(191.98/100, 30.0/100, 4.0/100)
        depth = 0
        if minimap:
            self.name += "_mini"
            # no material
            depth = -10000
            scale = ogre.Vector3(191.96/100, 30.0/100, 25.0/100)
        #self.ent = sceneManager.createEntity(self.name, "Wall.mesh")
        self.ent = sceneManager.createEntity(self.name, "cube.mesh")
        if not minimap:
            if self.type == "DEST":
                self.ent.setMaterialName("Arena/DestWall")            
            else:
                self.ent.setMaterialName("Arena/Wall")
        posX = self.x * 192
        posY = self.y * 192
        #posNode = sceneManager.getRootSceneNode().createChildSceneNode(self.name, (posX, 0.301, posY))
        posNode = sceneManager.getRootSceneNode().createChildSceneNode(self.name, (posX, depth + 0.001, posY))
        posNode.attachObject(self.ent)
        #box = self.ent.getBoundingBox()
        #boxMin = box.getMinimum()
        #boxMax = box.getMaximum()
        #physSize = ogre.Vector3(boxMax.x - boxMin.x, boxMax.y - boxMin.y, boxMax.z - boxMin.z)   
        #physOffset = ogre.Vector3((boxMax.x + boxMin.x)/2,(boxMax.y + boxMin.y)/2,(boxMax.z + boxMin.z)/2)
        physSize = ogre.Vector3(192, 30, 4)   
        if length > 1:
            scale.x *= length
            physSize.x *= length
        physOffset = physSize / 2 
        if side == "W":
            posNode.yaw(ogre.Degree(-90))
            posNode.translate(0,0,0.02)
            physOffset = ogre.Vector3(physOffset.z, physOffset.y, physOffset.x)
        else:
            posNode.translate(0.02,0,0)
        if physWorld and not minimap:
            self.physGeom = OgreOde.BoxGeometry(physSize, physWorld, physSpace)
            self.physGeom.setPosition(posNode.getPosition() + physOffset)
            self.physGeom.setOrientation(posNode.getOrientation())
            self.ent.setUserObject(self.physGeom)
            self.physGeom.setUserObject(self)
            self.physType = "WALL"
            util.setBitfields(self.physGeom, self.physType)
        posNode.translate(physOffset)
        posNode.setScale(scale)
