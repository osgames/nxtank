#!/usr/bin/env python
# Equipment.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Equipment tables

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

Methods:
    bodyList()      : returns list of valid body names
    body(name)      : returns dict of {size, mass, massLimit, space, drag, handling, turrets, cost} of named body
    engineList()    : returns list of valid engine names
    engine(name)    : returns dict of {power, mass, space, fuelCost, fuelCap, cost} of named engine
    armorList()     : returns list of valid armor names
    armor(name)     : returns dict of {defense, mass, space, cost} of named armor
    suspensionList(): returns list of valid suspension names
    suspension(name): returns dict of {handling, cost} of named suspension
    treadList()     : returns list of valid tread names
    tread(name)     : returns dict of {friction, cost} of named tread
    bumperList()    : returns list of valid bumper names
    bumper(name)    : returns dict of {elasticity, cost} of named bumper
    speciaLlist()   : returns list of valid special names
    special(name)   : returns dict of {cost} of named special
    weaponList()    : returns list of valid weapon names
    weapon(name)    : returns dict of {damage, range, ammo, reload, speed, mass, space, frames, heat, ammoCost, cost, refillSpeed, height, shotMesh} of named weapon
    heatsink()      : returns dict of {mass, space, cost} of a heat sink
'''

from collections import namedtuple

def bodyList():
    #return ["Lightcycle","Trike","Spider","Psycho","Tornado","Marauder","Tiger","Rhino","Medusa","Malice","Panzy","Disk","Delta"]
    return ["Hexo","Tornado","Tiger","Rhino"]

Body = namedtuple('Body','name size mass massLimit spaceLimit drag handling turrets cost momentInertia turretOffset hullOffset') 
def body(name):
    if name == "Lightcycle":
        return Body(name, 2,   200,   800,   600, 0.10, 8, 0,  3000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Trike":
        return Body(name, 2,   400,  1600,  1200, 0.15, 6, 0,  4000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Hexo":
        return Body(name, 4,  1500,  5000,  4000, 0.25, 6, 1,  4000, 5000,\
                ((0,8.82,0),(0,8.82,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((9,5,0),(0,5,-9),(-9,5,0),(0,5,9)))
    elif name == "Spider":
        return Body(name, 3,  2500,  8000,  3000, 0.40, 7, 1,  5000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Psycho":
        return Body(name, 4,  5000, 18000,  8000, 0.60, 4, 1,  5000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Tornado":
        return Body(name, 4,  7000, 22000, 12000, 0.80, 4, 1,  7000, 14150,\
                ((-7, 11, 0),(-7, 11, 0),(0,0,0),(0,0,0),(0,0,0)),\
                ((9.8,8.65,0),(-8.15,9.45,-8.4),(-16.3,4.6,0),(-8.15,9.45,8.4)))
    elif name == "Marauder":
        return Body(name, 5,  9000, 28000, 18000, 1.00, 3, 2, 10000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Tiger":
        return Body(name, 6, 11000, 35000, 22000, 1.50, 5, 1, 12000, 17950,\
                ((-11,11,0),(-11,11,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((20.99,4.2,0),(0,6.5,-6),(-21.75, 4.2,0),(0,6.5,6)))
    elif name == "Rhino":
        return Body(name, 7, 12000, 40000, 30000, 2.00, 3, 2, 10000, 23620,\
                ((-20,15.6,0),(-17.93,15.6,8.25),(-17.93,15.6,-8.25),(0,0,0),(0,0,0)),\
                ((28.86,7.06,0),(6.3,6.56,-10.56),(-25.35,6.57,0),(6.3,6.56,10.56)))
    elif name == "Medusa":
        return Body(name, 7, 14000, 40000, 25000, 1.20, 4, 3, 15000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Malice":
        return Body(name, 5,  4000, 20000, 15000, 0.40, 7, 1, 17000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Panzy":
        return Body(name, 8, 22000, 70000, 45000, 3.00, 3, 4, 25000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Disk":
        return Body(name, 7, 15000, 35000, 25000, 0.15, 6, 2, 15000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Delta":
        return Body(name, 6, 10000, 20000, 18000, 0.15, 6, 2, 14000, 0,\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    elif name == "Sentry":
        return Body(name, 1,   100,   400,   300, 0.15, 1, 1,  1000, 0,\
                ((0,5.75,0),(0,7,0),(0,0,0),(0,0,0),(0,0,0)),\
                ((0,0,0),(0,0,0),(0,0,0),(0,0,0)))
    else:
        raise Exception, "Bad body name: "+str(name)

def engineList():
    return ["Small Battery","Medium Battery","Large Battery","Super Battery",\
        "Small Combustion","Medium Combustion","Large Combustion","Super Combustion",\
        "Small Turbine","Medium Turbine","Large Turbine","Turbojet Turbine",\
        "Fuel Cell","Fission","Breeder Fission","Fusion"]

Engine = namedtuple('Engine','name power mass space fuelCost fuelCap cost') 
def engine(name):
    if name == "Small Battery":
        return Engine(name,   50,  100,   20,  5,  200,  1500)
    elif name == "Medium Battery":
        return Engine(name,  100,  150,   30,  5,  300,  2200)
    elif name == "Large Battery":
        return Engine(name,  200,  200,   40,  5,  400,  3000)
    elif name == "Super Battery":
        return Engine(name,  300,  250,   50,  5,  500,  6000)

    elif name == "Small Combustion":
        return Engine(name,  300,  400,  200,  8,  200,  2000)
    elif name == "Medium Combustion":
        return Engine(name,  400,  500,  300,  8,  300,  2500)
    elif name == "Large Combustion":
        return Engine(name,  500,  600,  400,  8,  400,  3000)
    elif name == "Super Combustion":
        return Engine(name,  600, 1000,  600,  8,  500,  4000)

    elif name == "Small Turbine":
        return Engine(name,  600, 1000,  800, 10,  350,  4000)
    elif name == "Medium Turbine":
        return Engine(name,  700, 1200, 1000, 10,  450,  5000)
    elif name == "Large Turbine":
        return Engine(name,  800, 1500, 1500, 10,  550,  7000)
    elif name == "Turbojet Turbine":
        return Engine(name, 1000, 2000, 2000, 10,  750, 10000)

    elif name == "Fuel Cell":
        return Engine(name, 1200, 1000,  400, 20,  600, 15000)
    elif name == "Fission":
        return Engine(name, 1500, 3000, 3500, 15, 1000, 20000)
    elif name == "Breeder Fission":
        return Engine(name, 1800, 3500, 4000, 15, 1250, 25000)
    elif name == "Fusion":
        return Engine(name, 2250, 4000, 2500,  5, 1500, 40000)
    else:
        raise Exception, "Bad engine name: "+str(name)

def armorList():
    return ["Steel","Kevlar","Hardened Steel","Composite","Compound Steel","Titanium","Tungsten"]
    # ,"New Steel","Carapice","Porcelain","New Tungsten","Crumple"]

Armor = namedtuple('Armor','name defense mass space cost') 
def armor(name):
    if name == "Steel":
        return Armor(name, 0,  8, 3,  10)
    elif name == "Kevlar":
        return Armor(name, 0,  3, 3,  20)
    elif name == "Hardened Steel":
        return Armor(name, 1,  8, 3,  20)
    elif name == "Composite":
        return Armor(name, 1,  4, 3,  30)
    elif name == "Compound Steel":
        return Armor(name, 2,  8, 3,  40)
    elif name == "Titanium":
        return Armor(name, 2,  5, 3,  70)
    elif name == "Tungsten":
        return Armor(name, 3, 20, 3, 100)
#    elif name == "New Steel":
#        result = {'defense':0,   'mass':5, 'space':3,   'cost':1}
#    elif name == "Carapice":
#        result = {'defense':1,   'mass':8, 'space':1,  'cost':80}
#    elif name == "Porcelain":
#        result = {'defense':1,   'mass':2, 'space':5,  'cost':80}
#    elif name == "New Tungsten":
#        result = {'defense':3,  'mass':14, 'space':3, 'cost':120}
#    elif name == "Crumple":
#        result = {'defense':-15, 'mass':1, 'space':0,  'cost':50}
    else:
        raise Exception, "Bad armor type: "+str(name)

def suspensionList():
    return ["Light","Normal","Heavy","Active"]

Suspension = namedtuple('Suspension','name handling cost') 
def suspension(name):
    if name == "Light":
        return Suspension(name, -1,  100)
    elif name == "Normal":
        return Suspension(name,  0,  200)
    elif name == "Heavy":
        return Suspension(name,  1,  400)
    elif name == "Active":
        return Suspension(name,  2, 1000)
    else:
        raise Exception, "Bad suspension name: "+str(name)

def treadList():
    return ["Smooth","Normal","Chained","Spiked"]
    # ,"Hover"]

Tread = namedtuple('Tread','name friction cost') 
def tread(name):
    if name == "Smooth":
        return Tread(name, 0.70,  100)
    elif name == "Normal":
        return Tread(name, 0.80,  200)
    elif name == "Chained":
        return Tread(name, 0.90,  400)
    elif name == "Spiked":
        return Tread(name, 1.00, 1000)
#    elif name == "Hover":
#        return {'friction':0.20,  'cost':500}
    else:
        raise Exception, "Bad tread name: "+str(name)

def bumperList():
    return ["None","Normal","Rubber","Retro"]

Bumper = namedtuple('Bumper','name elasticity cost') 
def bumper(name):
    if name == "None":
        return Bumper(name, 0.0,     0)
    elif name == "Normal":
        return Bumper(name, 0.07,  200)
    elif name == "Rubber":
        return Bumper(name, 0.15,  400)
    elif name == "Retro":
        return Bumper(name, 0.25, 1000)
    else:
        raise Exception, "Bad bumper name: "+str(name)

Heatsink = namedtuple('Heatsink','mass space cost num') 
def heatSink(num):
    return Heatsink(500, 1000, 500, num)

def specialList():
    return ["OverDrive","Radar","HoloCamo","ScreenJam"]
    # "Console","Mapper","Radar","Repair","Ram Plate","HUD",\
    #     "Deep Radar","Stealth","Navigation","New Radar","Tac Link","Camo","RDF"]

Special = namedtuple('Special','name mass space cost') 
def special(name):
    if name == "OverDrive":
        return Special(name, 100, 250, 5000)
    elif name == "Radar":
        return Special(name, 100, 250, 1000)
    elif name == "Sentry":
        return Special(name, 600, 800, 7000)
    elif name == "HoloCamo":
        return Special(name, 100, 250, 1000)
    elif name == "ScreenJam":
        return Special(name, 100, 250, 1000)
    #if name == "Console":
    #    result = {'cost':250}
    #elif name == "Mapper":
    #    result = {'cost':500}
    #elif name == "Repair":
    #    result = {'cost':300000}
    #elif name == "Ram Plate":
    #    result = {'cost':2000}
    #elif name == "HUD":
    #    result = {'cost':1}
    #elif name == "Deep Radar":
    #    result = {'cost':8000}
    #elif name == "Stealth":
    #    result = {'cost':20000}
    #elif name == "Navigation":
    #    result = {'cost':20}
    #elif name == "New Radar":
    #    result = {'cost':3000}
    #elif name == "Tac Link":
    #    result = {'cost':1000}
    #elif name == "RDF":
    #    result = {'cost':1000}
    else:
        raise Exception, "Bad special name: " + name

def weaponList():
    return ["Light Machine Gun","Machine Gun","Heavy Machine Gun",\
        "Light Pulse Rifle","Pulse Rifle","Heavy Pulse Rifle",\
        "Light Autocannon","Autocannon","Heavy Autocannon",\
        "Light Rkt Launcher","Rkt Launcher","Heavy Rkt Launcher",\
        "Acid Sprayer","Flame Thrower","Mine Layer","Oil Slick","Heat Seeker"]

Weapon = namedtuple('Weapon','name damage range ammo reload speed mass space frames heat ammoCost cost refillSpeed height type shotMesh length abbrev') 
def weapon(name):
    if name == "Light Machine Gun": 
        return Weapon(name, 2,  360, 300,  3, 255,   20,  200, 22, 0,  1,  1000,  1, 'normal', 'normal', 'Tiny Shell.mesh', 4.32, 'LMG')
    elif name == "Machine Gun":
        return Weapon(name, 3,  360, 250,  3, 255,   50,  225, 22, 1,  2,  2200,  1, 'normal', 'normal', 'Small Shell.mesh', 5.4, 'MG')
    elif name == "Heavy Machine Gun":
        return Weapon(name, 4,  360, 200,  3, 255,  100,  250, 22, 2,  3,  3000,  1, 'normal', 'normal', 'Shell.mesh', 6.5, 'HMG')

    elif name == "Light Pulse Rifle":
        return Weapon(name, 2,  480, 250,  4, 330,   50,  200, 22, 2,  1,  1500,  1, 'normal', 'normal', 'Small Pulse.mesh', 9.6, 'LPR')
    elif name == "Pulse Rifle":
        return Weapon(name, 3,  480, 225,  4, 330,  125,  225, 22, 3,  2,  3300,  1, 'normal', 'normal', 'Pulse.mesh', 12, 'PR')
    elif name == "Heavy Pulse Rifle":
        return Weapon(name, 4,  480, 200,  4, 330,  300,  250, 22, 4,  3,  4500,  1, 'normal', 'normal', 'Large Pulse.mesh', 14.4, 'HPR')

    elif name == "Light Autocannon":
        return Weapon(name, 3,  400, 250,  3, 330,  200,  300, 19, 3,  4,  3000,  1, 'normal', 'normal', 'Small Shell.mesh', 9.6, 'LAC')
    elif name == "Autocannon":
        return Weapon(name, 4,  400, 225,  3, 330,  300,  350, 19, 4,  5,  6000,  1, 'normal', 'normal', 'Shell.mesh', 12, 'AC')
    elif name == "Heavy Autocannon":
        return Weapon(name, 5,  400, 200,  3, 330,  500,  400, 19, 5,  6, 10000,  1, 'normal', 'normal', 'Large Shell.mesh', 14.4, 'HAC')

    elif name == "Light Rkt Launcher":
        return Weapon(name, 4,  600, 150,  7, 375,  600,  800, 25, 4,  8,  7000,  2, 'normal', 'normal', 'Small Rocket.mesh', 3.2, 'LRL')
    elif name == "Rkt Launcher":
        return Weapon(name, 6,  600, 125,  7, 375,  900, 1200, 25, 6, 10, 10000,  2, 'normal', 'normal', 'Rocket.mesh', 4, 'RL')
    elif name == "Heavy Rkt Launcher":
        return Weapon(name, 8,  600, 100,  7, 375,  900, 1600, 25, 8, 12, 15000,  2, 'normal', 'normal', 'Large Rocket.mesh', 4.8, 'HRL')

    elif name == "Acid Sprayer":
        return Weapon(name, 1,  160, 100,  3, 150,  300,  500, 17, 0, 10,  3000,  1, 'normal', 'acid', 'Spray.mesh', 5, 'Acid')
    elif name == "Flame Thrower":
        return Weapon(name, 3,  200, 300,  1, 180,  700,  500, 17, 1,  2,  4000,  1, 'normal', 'fire', 'Shell.mesh', 5, 'Fire')
    elif name == "Mine Layer":
        return Weapon(name, 6,   50,  50,  2, 150, 1000, 1000, 70, 2, 10,  8000,  4, 'low', 'normal', 'Mine.mesh', 0, 'Mine') # Siize is not fixed yet until mesh is completed
    elif name == "Oil Slick":
        return Weapon(name, 0,   50,  50,  5, 150,  300,  500, 70, 0, 10,  2000,  1, 'low', 'normal', 'Shell.mesh', 0, 'Oil') # Siize is not fixed yet until mesh is completed
    elif name == "Heat Seeker":
        return Weapon(name, 8, 1250,  15, 15, 375, 1000, 1800, 51, 9, 50, 20000, 12, 'high', 'normal', 'Heat Seeker Missile.mesh', 5, 'Seek')

    else:
        raise Exception, "Bad weapon name: "+str(name)
