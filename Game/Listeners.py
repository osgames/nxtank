#!/usr/bin/env python
# Listeners.py

# Copyright (C) 2008  Colin McCulloch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This module contains the various listeners used in the game.  These
were moved out of the Core module since they aren't actually related to
the setup of the game.

Author: Colin "Zyzle" McCulloch and Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__Date__ = "$Date$"

import ogre.renderer.OGRE as ogre
import ogre.physics.OgreOde as OgreOde
import ogre.io.OIS as OIS

class GameFrameListener(ogre.FrameListener):
    """The games frame listener class"""
    def __init__(self, gmanager):
        """constructor takes in the gameManager which this listener will
        forward frame events to"""
        super(GameFrameListener, self).__init__()
        self.gManager = gmanager
        
    def frameStarted(self, evt):
        """forward frame started event, return true on successful
        frame, false otherwise"""
        successfulFrame = self.gManager.frameStarted(evt)
        return successfulFrame
    
    def frameEnded(self, evt):
        """forward frame ended events, return true on successful
        frame false otherwise"""
        successfulFrame = self.gManager.frameEnded(evt)
        return successfulFrame
    #def showDebugOverlay(self, show):
    #    """Turns the debug overlay (frame statistics) on or off."""
    #    overlay = ogre.OverlayManager.getSingleton().getByName('Core/DebugOverlay')
    #    if overlay is None:
    #        raise ogre.Exception(111, "Could not find overlay Core/DebugOverlay", "Listeners.py")
    #    if show:
    #        overlay.show()
    #    else:
    #        overlay.hide()

    
class GameAbstractListener(object):
    def __init__(self, gManager, rwindow):
        self.gameManager = gManager
        windowHandle = rwindow.getCustomAttributeInt("WINDOW")
        paramList = [("WINDOW", str(windowHandle))]
        self.inputManager = OIS.createPythonInputSystem(paramList)
    
class GameKeyListener(GameAbstractListener, OIS.KeyListener):
    
    def __init__(self, gmanager, rwindow):
        """constructor for GameKeyListener, takes in the window
        from which import key events will occur"""
        GameAbstractListener.__init__(self, gmanager, rwindow)
        OIS.KeyListener.__init__(self)
        keyboard = self.inputManager \
                            .createInputObjectKeyboard(OIS.OISKeyboard, True)
        self.keyboard = keyboard
        self.keyboard.setEventCallback(self)
        
    def keyPressed(self, evt):
        """Forward key events to gamemanager, no return required"""
        self.gameManager.keyPressed(evt)
        
    def keyReleased(self, evt):
        """Forward key events to gamemanager, no return required"""
        self.gameManager.keyReleased(evt)
        
class GameMouseListener(GameAbstractListener, OIS.MouseListener):
    
    def __init__(self, gmanager, rwindow):
        GameAbstractListener.__init__(self, gmanager, rwindow)
        OIS.MouseListener.__init__(self)
        mouse = self.inputManager.createInputObjectMouse(OIS.OISMouse, 
                                                         True)
        self.mouse = mouse
        self.mouse.setEventCallback(self)
        
    def mouseMoved(self, evt):
        self.gameManager.mouseMoved(evt)
        
    def mousePressed(self, evt, buttonID):
        self.gameManager.mousePressed(evt, buttonID)
        
    def mouseReleased(self, evt, buttonID):
        self.gameManager.mouseReleased(evt, buttonID)

class GameCollisionListener(OgreOde.CollisionListener):
    """This listener is a simple implementation of collision detection. 
    """
    def __init__(self, world, arena):
        """The constructor for the collision listener
        Takes in a world object and sets this class as
        the collision listener"""
        OgreOde.CollisionListener.__init__(self)
        self.world = world
        self.world.setCollisionListener(self)
        self.arena = arena
        self.cols = 0
        
    def collision(self, contact):
        """In the event of collisions this method is called"""
        g1 = contact.getFirstGeometry()
        g2 = contact.getSecondGeometry()
        #if g1 and g2:
        #    b1 = g1.getBody()
        #    b2 = g2.getBody()
        #    if b1 and b2 and OgreOde.Joint.areConnected(b1, b2):
        #       return False
        first = g1.getUserObject()
        second = g2.getUserObject()
        shot = None
        tank = None
        wall = None
        tanks = False
        
        #if first == None and second.physType == "TANK":
        #    contact.setCoulombFriction(200 * (second.treads["friction"]) - 0.5)
        #    contact.setBouncyness(0.1)
        #    return True
        #elif second == None and first.physType == "TANK":
        #    contact.setCoulombFriction(200 * (first.treads["friction"]) - 0.5)
        #    contact.setBouncyness(0.1)
        #    return True
        if (first == None and second.physType == "TANK") or (second == None and first.physType == "TANK"):
            return True
        elif (first == None and second.physType == "SHOT") or (second == None and first.physType == "SHOT"):
            return False # should cull this
        elif first == None or second == None: # this shouldn't happen
            print g1
            print g2
            print first
            print second
        elif first.physType == "SHOT":
            shot = first
            if second.physType == "SHOT":
                return False
            elif second.physType == "TANK":
                tank = second
            elif second.physType == "WALL":
                wall = second
        elif second.physType == "SHOT":
            shot = second
            if first.physType == "TANK":
                tank = first
            elif first.physType == "WALL":
                wall = first
        elif first.physType == "TANK" and second.physType == "WALL":
            tank = first
            wall = second
        elif first.physType == "WALL" and second.physType == "TANK":
            wall = first
            tank = second
        elif first.physType == "TANK" and second.physType == "TANK":
            tanks = True

        if shot and tank:
            #if tank != shot.tank:
            self.arena.collShotTank.extend([shot, tank])
            return False
        elif tanks:
            if first.spawning:
                first.respawn = True
                return False
            elif second.spawning:
                second.respawn = True
                return False
            self.arena.collTanks.extend([first, second])
            contact.setBouncyness(0.1 + first.bumpers.elasticity + second.bumpers.elasticity)
            return True
        elif shot and wall:
            self.arena.collShotWall.extend([shot, wall])
            return False
        elif tank and wall:
            self.arena.collTankWall.extend([tank, wall])
            contact.setBouncyness(0.1 + tank.bumpers.elasticity)
            return True
        
        #contact.setCoulombFriction(9999999999)
        contact.setBouncyness(0.1)
        
        return False

