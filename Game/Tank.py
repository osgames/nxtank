#!/usr/bin/env python
# Tank.py

# Copyright (C) 2008  Shaun McGee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This is the Tank class.

Author: Shaun McGee

Last Edit By: "$Author$"

"""

__version__ = "$Revision$"
__date__ = "$Date$"

from math import pi, copysign, sqrt, sin, cos, atan2
from random import random
import os
import fileinput
import ogre.renderer.OGRE as ogre
import ogre.physics.OgreOde as OgreOde
import Game.Equipment as equip
from Game.Weapon import Weapon
from Game.Turret import Turret
import Game.Utility as util
from collections import namedtuple
Blip = namedtuple('Blip','size team LOS bearing velocity score burning') 

class Tank(ogre.UserDefinedObject):
    '''Tank class

    Attributes:
        mesh         (mesh)    : visible mesh
        tankFile     (str)     : .tank file
        material     (material): material for self.mesh
        materialTread(material): material for Tread subentities
        materialTreadTex(material): texture unit state for tread animation
        materialWheel(material): material for Wheel subentities
        materialWheelTex(material): texture unit state for wheel animation
        subentMaterial(dict)   : dict of subent materials
        team         (int)     : team number
        teamColor    (tuple)   : tuple of color tuples
        ent          (entity)  : entity for self.mesh
        posNode      (node)    : current position
        rotNode      (node)    : current rotation
        speed        (float)   : current linear speed, relative-X
        newSpeed     (float)   : desired linear speed
        maxSpeed     (float)   : max linear speed
        accel        (float)   : max linear acceleration
        turnMax      (float)   : max turn rate in radians/tick
        turnAccel    (float)   : turn acceleration in radians/tick
        turnRate     (float)   : current turn rate in radians/tick
        turned       (boolean) : False until direction matches course
        velocity     (vector3) : current linear speed
        direction    (radian)  : unit vector direction of self.ent
        newDirection (radian)  : unit vector desired direction of movement
        spawning     (bool)    : freshly respawned
        respawn      (bool)    : flagged to respawn
        alive        (bool)    : currently in play
        burning      (float)   : Countdown until respawn
        firing       (dict)    : dict of weapons to fire next game tick
        firingReset  (dict)    : default values for weapon firing state
        shotNumber   (int)     : total projectiles
        shotList     (list)    : active projectiles
        sfx          (obj)     : sound effects manager
        bot          (Bot)     : AI object
        armor        (dict)    : dict of armor location:points
        maxArmor     (dict)    : dict of max armor location:points
        highestArmor (int)     : highest armor value of any side
        special      (dict)    : dict of special name:object
        specialName  (dict)    : dict of special names
        weapon       (list)    : list of mounted weapon objects
        heat         (float)   : current heat level
        fuel         (float)   : current fuel level
        maxFuel      (float)   : max fuel level
        visualRangeX (float)   : half-width of visible OH view
        visualRangeZ (float)   : half-height of visible OH view
        scanRange    (float)   : range of scan()
        mazeBox      (mazeBox) : current maze location object
        particleBurnSmoke
                    (particle) : death smoke
        particleDamSmoke(dict) : damage smoke particles per side
        muzzleNode      (dict) : hull weapon muzzles

        vehicle      (str)     : design name
        designer     (str)     : designer name
        body       (namedtuple): from equip tables
        engine     (namedtuple): from equip tables
        armorType  (namedtuple): from equip tables
        heatSinks  (namedtuple): from equip tables
        suspension (namedtuple): from equip tables
        treads     (namedtuple): from equip tables
        bumpers    (namedtuple): from equip tables

        turretList   (list)    : list of mounted turret objects, 0=Gunner
        turretLock   (boolean) : Lock turrets in current orientation

        kills        (float)   : tanks destroyed
        deaths       (float)   : times destroyed
        damagedBy    (dict)    : damage taken from each source
        killedBy     (dict)    : deaths from each source
        score        (float)   : score
        score        (float)   : current score
        status       (dict)    : dict of status flags
                                    'Jammed' : Radar jammed
                                    'Camo'   : Camo-hidden

        physWorld    (world)   : ODE world
        physSpace    (space)   : ODE space
        physBoxMass  (boxMass) : ODE boxMass
        physSize     (vector)  : boundingBox dimensions
        physOffset   (vector)  : body center offset from entity center
        physGeom     (geom)    : ODE geometry
        physType     (string)  : collision object type
        physMass     (float)   : mass scaled 0.1..10 in Arena
        physLinearThresh(float): allowed linear drift
        physAngularThresh(radian): allowed spin
        momentInertia(float)   : moment of inertia of physBody
        turnForce    (float)   : force resulting in 1 radian/sec rotation
        
        LOSNode      (node)    : LOS origin
        LOS          (dict)    : dict of target bools
        LOSNav       (float)   : distance to forward obstruction
        LOSNavType   (string)  : type of obstruction
        gunCamNode   (node)    : Gunner Camera mount
        
        bugSpawn     (boolean) : flag to enable ODE bug workaround for first spawn
        bugPosition  (vector3) : first spawn position
        bugSpeed     (float)   : expected speed to check ODE bug
        bugDriven    (float)   : expected position to check ODE bug
        
        debug        (string)  : debugging data

    Methods:
        setMass(mass)             : set physics body mass
        spawn(position)           : place tank on maze
        move(time)                : move tank scaled to frame time
        moveTurrets(time)         : move turrets scaled to frame time
        animate(time, shotBatch)  : animate component textures and meshes
        fire()                    : fire triggered weapons
        damage(amount, side, type): Damage this vehicle
        burn()                    : set self.burning and scorch tank
        setMaterial()             : set material
        setMass(mass)             : set physics mass, scaled for Arena
        scan(type)                : return a list of tank or shot Blips
        setSpeed(speed)           : set self.newSpeed
        setDirection(radian)      : set self.newSpeed
        setAim(radian)            : aim each Turret
        hide()                    : remove from play
        show()                    : return to play
        cleanup()                 : remove entities and sceneNodes
        readTankFile(file)        : read .tank file
    '''

    def __init__(self, parent, name, team, tankFile, debugging, physWorld, physSpace):
        ogre.UserDefinedObject.__init__(self)
        self.debugging = debugging
        self.physWorld = physWorld
        self.physSpace = physSpace
        self.tankFile = tankFile
        self.direction = 0
        self.newDirection = 0
        self.turretLock = False
        self.armor = {}
        self.maxArmor = {}
        self.highestArmor = 0
        self.special = {}
        self.specialName = {}
        self.speed = 0
        self.velocity = ogre.Vector3(1,0,0)
        self.newSpeed = self.speed
        self.maxSpeed = 0
        self.accel = 0
        self.turnMax = 0
        self.turnAccel = 0
        self.turnRate = 0
        self.bot = None
        self.burning = 0
        self.firing = {}
        self.firingReset = {}
        weaponList = range(1,7)
        weaponList.append('all')
        for weapon in weaponList:
            self.firingReset[weapon] = False
        self.firing.update(self.firingReset)
        for specialName in equip.specialList():
            self.special[specialName] = None
        self.shotList = []
        self.shotNumber = 0
        self.weapon = []
        self.heat = 0
        self.turretList = []
        self.kills = 0
        self.deaths = 0
        self.damagedBy = {}
        self.killedBy = {}
        self.score = 0
        self.score = 0
        # Specials
        self.status = {}
        self.visualRangeX = 384 *4/3 # OH view isn't square anymore
        self.visualRangeZ = 384
        self.scanRange = self.visualRangeZ
        # Assemble tank
        self.parent = parent
        self.sceneManager = parent.sceneManager
        self.sfx = parent.sfx
        self.name = name
        self.team = team
        self.mazeBox = None
        if not self.readTankFile(tankFile):
            raise "Failed to load tankFile!"
        if not self.verifyDesign():
            raise "Failed to verify design!"
        self.posNode = self.sceneManager.getRootSceneNode().createChildSceneNode(self.name)
        self.ent = self.sceneManager.createEntity("Tank_"+ self.name, self.mesh)
        self.posNode.attachObject(self.ent)
        # Gunner camera
        name = self.name + "_Camera"
        position = self.body.turretOffset[0]
        turretNode = self.posNode.createChildSceneNode(name, position)
        turret = Turret(name, self, turretNode, self.debugging)
        self.turretList.append(turret)
        node = turretNode.createChildSceneNode(self.name + "_GunCamNode", (-20,10,0))
        self.gunCamNode = node.createChildSceneNode(self.name + "_GunPitchNode")
        
        #Gunner spotlight
        #light = self.sceneManager.createLight (self.name + "_SpotLight")
        #light.type = ogre.Light.LT_SPOTLIGHT
        #light.diffuseColour = (1, 1, 1)
        #light.specularColour = (0.75, 0.75, 0.75)
        #light.direction = (1, -0.25, 0)
        #light.setSpotlightRange(ogre.Degree(35), ogre.Degree(50))
        #self.gunCamNode.attachObject(light)
        #light.position = (10, 0, 0)

        self.LOSNode = turretNode.createChildSceneNode(self.name + "_LOSNode", (0,10,0))
        self.LOS = {}
        self.LOSNav = 0
        self.LOSNavType = None
        self.bugSpawn = True
        self.bugPosition = None
        self.bugDriven = 0
        self.bugSpeed = 0
        # Mount turrets
        for turretIndex in range(1,self.body.turrets+1):
            weaponList = []
            for weapon in self.weapon[1:]:
                if weapon.mount.find("T") > -1:
                    (junk, turretNum) = weapon.mount.split("T",1)
                    if turretIndex == util.strToInt(turretNum):
                        weaponList.append(weapon)
            if not weaponList:
                # don't bother with empty turrets
                continue
            position = self.body.turretOffset[turretIndex]
            name = self.name + "_Turret_" + str(turretIndex)
            turretNode = self.posNode.createChildSceneNode(name, position)
            turret = Turret(name, self, turretNode, self.debugging, weaponList)
            self.turretList.append(turret)
        self.teamColor = (0.75,0.75,0.75),(0.5,0,0),(0.75,0.3,0),(0.5,0.5,0),(0,0.5,0),(0,0,0.5),(0.5,0,0.5),(0,0.5,0.5)
        self.material = ogre.MaterialManager.getSingleton().create(self.name,
                        ogre.ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME)
        self.materialTread = ogre.MaterialManager.getSingleton().create(self.name,
                             ogre.ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME)
        self.materialTread.getTechnique(0).getPass(0).createTextureUnitState("Tank_Tread.jpg")
        self.materialTreadTex = self.materialTread.getTechnique(0).getPass(0).getTextureUnitState(0)
        self.materialWheel = ogre.MaterialManager.getSingleton().create(self.name,
                             ogre.ResourceGroupManager.DEFAULT_RESOURCE_GROUP_NAME)
        self.materialWheel.getTechnique(0).getPass(0).createTextureUnitState("Tank_Wheel.jpg")
        self.materialWheelTex = self.materialWheel.getTechnique(0).getPass(0).getTextureUnitState(0)
        self.subentMaterial = {}
        for subEntity in range(0,self.ent.getNumSubEntities()):
            ent = self.ent.getSubEntity(subEntity)
            name = ent.getMaterialName()
            self.subentMaterial[ent] = name
            if name == "Tank/Tread":
                ent.setMaterial(self.materialTread)
                if self.debugging.materials:
                    print "Tread "+name
            elif name == "Tank/Wheel":
                ent.setMaterial(self.materialWheel)
                if self.debugging.materials:
                    print "Wheel "+name
        for weapon in self.weapon[1:]:
            for subEntity in range(0,weapon.ent.getNumSubEntities()):
                ent = weapon.ent.getSubEntity(subEntity)
                name = ent.getMaterialName()
                self.subentMaterial[ent] = name
        for turret in self.turretList[1:]:
            for subEntity in range(0,turret.ent.getNumSubEntities()):
                ent = turret.ent.getSubEntity(subEntity)
                name = ent.getMaterialName()
                self.subentMaterial[ent] = name

        self.physWorld = physWorld
        self.physBody = OgreOde.Body(physWorld)
        self.posNode.attachObject(self.physBody)
        box = self.ent.getBoundingBox()
        box.setMaximumY(25)
        boxMin = box.getMinimum()
        boxMax = box.getMaximum()
        self.physSize = ogre.Vector3(boxMax.x - boxMin.x, boxMax.y - boxMin.y, boxMax.z - boxMin.z)   
        self.physOffset = ogre.Vector3((boxMax.x + boxMin.x)/2,(boxMax.y + boxMin.y)/2,(boxMax.z + boxMin.z)/2)
        self.physGeom = OgreOde.BoxGeometry(self.physSize, physWorld, physSpace)
        self.physGeom.setBody(self.physBody)
        self.physGeom.setOffsetPosition(self.physOffset)
        self.physGeom.setUserObject(self)
        self.ent.setUserObject(self.physGeom)
        self.physType = "TANK"
        self.physMass = 0
        util.setBitfields(self.physGeom, self.physType, self.team)
        self.turtled = 0
        self.physLinearThresh = self.physWorld.getAutoSleepLinearThreshold()
        self.physAngularThresh = self.physWorld.getAutoSleepAngularThreshold()
        self.momentInertia = self.body.momentInertia
        # Particles
        self.particleBurnSmoke = self.sceneManager.createParticleSystem(self.name +'_smoke', 'BurnSmoke')
        self.posNode.attachObject(self.particleBurnSmoke)
        self.particleDamSmoke = {}
        self.muzzleNode = {}
        offset = {'F':self.body.hullOffset[0],'P':self.body.hullOffset[1],'A':self.body.hullOffset[2],'S':self.body.hullOffset[3]}
        orient = {'F':(sqrt(0.5),0,0,-sqrt(0.5)),'P':(sqrt(0.5),-sqrt(0.5),0,0),'A':(sqrt(0.5),0,0,sqrt(0.5)),'S':(sqrt(0.5),sqrt(0.5),0,0)}
        for side in ('F','A','P','S'):
            self.particleDamSmoke[side] = self.sceneManager.createParticleSystem(self.name +'_smoke'+side, 'DamageSmoke')
            self.muzzleNode[side] = self.posNode.createChildSceneNode(self.name +"_muzzle"+side, offset[side])
            self.muzzleNode[side].setOrientation(orient[side])
            self.muzzleNode[side].attachObject(self.particleDamSmoke[side])
        del orient, offset
        
        self.debug = 0 #"Atomic batteries to power, turbines to speed!"

    def setMass(self, mass):
        self.physMass = mass
        self.physBoxMass = OgreOde.BoxMass(mass, self.physSize)
        self.physBody.setMass(self.physBoxMass)
        self.turnForce = self.physMass * self.momentInertia
        
    def spawn(self, position):
        self.physBody.setPosition(position)
        self.physBody.setOrientation(ogre.Quaternion(1,0,0,0))
        self.physBody.setLinearVelocity(ogre.Vector3(0,0,0))
        self.physBody.setAngularVelocity(ogre.Vector3(0,0,0))
        self.setMaterial()
        self.particleBurnSmoke.getEmitter(0).setEnabled(False)
        for side in ('F','A','P','S'):
            self.particleDamSmoke[side].getEmitter(0).setEmissionRate(0)
        self.posNode.setVisible(True)
        self.speed = 0
        self.newSpeed = 0
        self.newDirection = 0
        self.turnRate = 0
        self.turned = False
        self.burning = 0
        self.heat = 0
        self.fuel = self.maxFuel
        for side in ('F','A','P','S','T','B'):
            self.armor[side] = self.maxArmor[side]
        for special in self.special.values():
            if special:
                special.spawn()
        for weapon in self.weapon[1:]:
            weapon.ammo = weapon.maxAmmo
            weapon.armed = True
        self.posNode.setVisible(True)
        self.alive = True
        self.spawning = True
        self.respawn = False
        if self is self.parent.playerTank:
            # camera frustrums suddenly visible? ogre bug?
            self.parent.OHCamera.setVisible(False)
            self.parent.gunCamera.setVisible(False)
        self.status['Camo'] = False
        self.status['Jammed'] = False
        if self.bot and self.bot.active:
            self.bot.spawn()
        if self.bugSpawn:
            self.bugSpawn = False
            self.bugPosition = self.posNode._getDerivedPosition()
            self.bugPosition.y = 0

    def burn(self):
        if self.burning:
            return
        self.alive = False
        self.burning = 5 * 15    # ticks to burn
        self.setMaterial(burn=True)
        for side in ('F','A','P','S'):
            self.particleDamSmoke[side].getEmitter(0).setEmissionRate(0)
        self.particleBurnSmoke.getEmitter(0).setEnabled(True)

    def hide(self):
        ''' this is equivalent to 'Remove' or 'Destroy'. ogre recommends not destroying
        scene nodes during the game, so we de-activate instead. the actor can be
        re-used if needed without recreating it. in the game you can easily keep a 
        tracker of hidden actors and only create new ones when you need to.
        '''
        self.sceneManager.destroyParticleSystem(self.name +'_explosion')
        self.posNode.setPosition(ogre.Vector3(10000+random()*10000, 10000+random()*10000, 10000+random()*10000))
        self.posNode.setVisible(False)
        self.alive = False
    
    def destroy(self):
        del self.bot
        del self.special
        for weapon in self.weapon:
            if weapon:
                del weapon.reShotList
        del self.weapon
        del self.turretList
        del self.shotList
        del self.LOS
        del self.damagedBy
        del self.killedBy
        
    def cleanup(self):
        self.sceneManager.destroyEntity("Tank_"+ self.name)
        for turret in self.turretList[1:]:
            self.sceneManager.destroyEntity(turret.name)
        for weapon in self.weapon[1:]:
            self.sceneManager.destroyEntity(weapon.entName)
            self.sceneManager.destroyParticleSystem(weapon.entName)
        for shot in self.shotList:
            shot.cleanup()
        self.sceneManager.destroyParticleSystem(self.particleBurnSmoke)
        for side in ('F','A','P','S'):
            self.sceneManager.destroyParticleSystem(self.particleDamSmoke[side])        
        try:
            self.sceneManager.getRootSceneNode().removeAndDestroyChild(self.name)
        except:
            print "Failed to destroy sceneNode "+self.name
            
    def update(self):
        for special in self.special.values():
            if special and special.active:
                special.action()
        weaponList = []
        if self.firing['all']:
            weaponList.append('all')
        else:
            for weapon in self.weapon[1:]:
                if self.firing[weapon.number]:
                    weaponList.append(weapon.number)
                elif weapon.particle:
                    #weapon.particle.setEmitting(False)
                    weapon.particle.getEmitter(0).setEmissionRate(0)
            #for item in self.firing.iteritems():
            #    if item[1] is True:
            #        weaponList.append(item[0])
        if weaponList:
            self.fire(weaponList)
        self.firing.update(self.firingReset)
        if self.burning:
            self.burning -= 1
            if self.burning == 5:
                explosion = self.sceneManager.createParticleSystem(self.name +'_explosion', 'TankDeath')
                self.turretList[0].posNode.attachObject(explosion)
            if self.burning <= 0:
                self.burning = 0
                self.hide()
        # Heat sinks do their thing
        self.heat = max(0, self.heat - float(self.heatSinks.num)/5) # -1 heat every 5 ticks
        if self.heat > 100:
            for weapon in self.weapon[1:]:
                if weapon.heat:
                    self.firing[weapon.number] = False
                    if weapon.particle:
                        weapon.particle.getEmitter(0).setEmissionRate(0)
        # Decrement reload timers
        for weapon in self.weapon[1:]:
            weapon.reloading = max(0, weapon.reloading - 1)
        # Burn fuel
        self.fuel = max(0, self.fuel -0.025 * (self.newSpeed**2/self.maxSpeed**2))
        
    def animate(self, time, shotBatch=1):
        # Cannon recoil
        for weapon in self.weapon[1:]:
            if weapon.name.find("Autocannon") > -1:
                recoil = float(float(weapon.reloading)/shotBatch)/(weapon.reload*10)
                weapon.posNode.setScale(1- recoil,1,1)
                offset = weapon.turretOffset
                weapon.posNode.setPosition(offset[0] - recoil*2, offset[1], offset[2])
        # Treads & Wheels
        texture = self.materialTreadTex
        v = texture.getTextureVScroll() - self.speed/2.0 * time
        texture.setTextureScroll(0,v)
        texture = self.materialWheelTex
        v = texture.getTextureVScroll() - self.speed/4.0 * time
        texture.setTextureScroll(0,v)
        # Damage smoke
        if not self.burning:
            for side in ('F','A','P','S'): #,'T','B'):
                if self.maxArmor[side] > 0:
                    status = float(self.armor[side]) / self.maxArmor[side]  
                    smoke = (1.0 - status) * 10
                    self.particleDamSmoke[side].getEmitter(0).setEmissionRate(smoke)
                
    def move(self, tickTime):
        altitude = self.posNode._getDerivedPosition().y
        if altitude > 1:
            self.physBody.wake()
            # no traction
            if altitude > self.physOffset/2 and not self.spawning:
                self.turtled += tickTime
                if self.turtled > 5: # sec
                    if self.debugging.physics:
                        print "TURTLE "+ str(self.turtled) +" "+ self.name
                    self.respawn = True # we've turtled, give up
                    self.turtled = 0
            return
        self.spawning = False
        ## Return here for MOI testing
        #return
        if self.fuel is 0:
            self.newSpeed = 0
        self.velocity = self.physBody.getLinearVelocity()
        relVelocity = self.physBody.getVectorFromWorld(self.velocity)
        self.speed = relVelocity.x
        # lateral damping, do this with friction?
        slipSpeed = relVelocity.z
        if abs(slipSpeed) >= self.physLinearThresh:
            self.physBody.addRelativeForce(ogre.Vector3(0,0, self.physMass * 3 * self.treads.friction * -slipSpeed))
        # Accelerate
        diff = self.newSpeed - self.speed
        if abs(diff) < self.accel:
            accel = diff
        else:
            accel = self.accel * copysign(1,diff)
        if abs(accel) >= self.physLinearThresh:
            self.physBody.addRelativeForce(ogre.Vector3(self.physMass * accel,0,0))
            self.bugSpeed += accel
            self.bugDriven += self.bugSpeed
        # Turn        
        self.orientation = self.posNode._getDerivedOrientation()
        vector = self.orientation * ogre.Vector3().UNIT_X
        self.direction = atan2(vector.z,vector.x)
        physTorque = self.physBody.getAngularVelocity()
        self.turnRate = physTorque.y
        turning = util.wrapPi(self.newDirection - self.direction + self.turnRate)
        if abs(turning) >= self.physAngularThresh:
            self.turned = False
            turnSign = copysign(1,turning)
            turnForce = self.turnForce * min(self.turnAccel, abs(turning))
            framesToStop = sum(range(self.turnRate/self.turnAccel))
            if framesToStop * self.turnAccel < abs(turning) and abs(self.turnRate) < self.turnMax:
                turnSign *= -1
            self.physBody.addRelativeTorque(ogre.Vector3(0, turnForce * turnSign, 0))
        else:
            self.turned = True            
            
    def moveTurrets(self, tickTime):
        for turret in self.turretList:
            if not self.turretLock or turret is self.turretList[0] and self is self.parent.playerTank:
                heading = turret.posNode._getDerivedOrientation() * ogre.Vector3().UNIT_X
                turret.aim = atan2(heading.z,heading.x)
                if turret.aim != turret.newAim:
                    turning = util.wrapPi(turret.newAim - turret.aim)
                    turnSign = copysign(1,turning)
                    framesToStop = sum(range(turret.turnRate/turret.turnAccel))
                    if framesToStop * turret.turnAccel < abs(turning) and abs(turret.turnRate) < turret.turnMax:
                        turret.turnRate += turret.turnAccel
                    elif turret.turnRate > turret.turnAccel:
                        turret.turnRate -= turret.turnAccel
                    tickTurn = turret.turnRate * tickTime
                    if abs(turning) <= tickTurn:
                        turret.aim = float(turret.newAim)
                        turret.turnRate = 0
                    else:
                        turret.aim = turret.aim + tickTurn * turnSign
                    quat = heading.getRotationTo(ogre.Vector3(cos(turret.aim),0,sin(turret.aim)))
                    turret.posNode.yaw(quat.getYaw()) # STOP FLAILING

    def scan(self, type='tanks'):
        blips = [] 
        myLoc = self.posNode.getPosition()
        if self.status['Jammed']:
            bearing = self.status['Jammed'] - myLoc
            blip = Blip(0.99, -1, True, bearing, ogre.Vector3(0,0,0), 0, 1) # CEGUI breaks this if we use blipSize = 1
            blips.append(blip)                    
        elif type is 'tanks':
            for tank in self.parent.tankList:
                if tank.status['Jammed']:
                    bearing = tank.status['Jammed'] - myLoc
                    blip = Blip(0.99, -1, True, bearing, ogre.Vector3(0,0,0), 0, 1) # CEGUI breaks this if we use blipSize = 1
                    blips.append(blip)                    
                    continue
                if tank == self or tank.spawning or tank.status['Camo'] == True: # this should be fuzzed radar return
                    continue
                blipTeam = None
                blipSize = 0.05
                blipped = False
                burning = 0
                LOS = False
                tankLoc = tank.posNode.getPosition()
                bearing = tankLoc - myLoc
                if self.LOS.has_key(tank) and self.LOS[tank]:
                    blipped = True
                    blipTeam = tank.team
                    burning = tank.burning
                    LOS = True
                elif abs(myLoc.x - tankLoc.x) < self.scanRange and abs(myLoc.z - tankLoc.z) < self.scanRange:
                    blipped = True
                    if not tank.burning: # everyone burns the same
                        blipTeam = tank.team
                if blipped:
                    blip = Blip(blipSize, blipTeam, LOS, bearing, tank.velocity, tank.score, burning) 
                    blips.append(blip)                    
        elif type is 'shots':
            pass
        return blips

    def shoot(self, weapon="all"):
        # This will allow more detailed weapon deployment later
        self.firing[weapon] = True
    
    def fire(self, weaponList=("all",)):
        if weaponList[0] == "all":
            weaponList = range(1, len(self.weapon))
        for wep in weaponList:
            if wep >= len(self.weapon):
                continue
            weapon = self.weapon[wep]
            if not weapon.reloading and not(self.heat >= 100 and weapon.heat) and weapon.ammo and weapon.armed and not self.spawning:
                weapon.fire()
                # This will, of course, eventually be determined by the weapon used.
                soundFile = "gunshot.ogg"
                position = (self.posNode.getPosition())
                if not self.parent.sfxMute:
                    self.sfx.shotFired(soundFile, position)

    def damage(self, damage, hitAngle, source, batch=1, type='normal'):
        # handle detailed damage here later
        if type == "normal":
            damage -= self.armorType.defense * batch
        elif type == 'collision':
            pass
        elif type == 'fire':
            self.heat += damage
            return
        if damage > 0:
            util.wrapPi(hitAngle)
            if abs(hitAngle) < pi/4:
                side = 'F'
            elif abs(hitAngle) > pi*3/4:
                side = 'A'
            elif copysign(1,hitAngle) < 0:
                side = 'P'
            else:
                side = 'S'
            self.armor[side] -= damage
            
            try:
                if not source in self.damagedBy:
                    self.damagedBy[source] = 0
                self.damagedBy[source] += damage
            except:
                print self.name + " has no self.damagedBy!"
            if self.armor[side] < 0:
                if not source in self.killedBy:
                    self.killedBy[source] = 0
                self.killedBy[source] += 1
                self.burn()
            
            #self.debug = str( int((hitAngle +pi)*2/pi +0.5) ) +" = "+str(hitAngle)

    def setMaterial(self, burn=False, camo=False):
        if burn:
            teamColor = (0,0,0)
        else:
            teamColor = self.teamColor[self.team]
        if camo:
            util.setMaterial(self.ent, self.material, None, teamColor, camo, self.debugging.materials)
            for turret in self.turretList[1:]:
                util.setMaterial(turret.ent, self.material, None, teamColor, camo, self.debugging.materials)
            for weapon in self.weapon[1:]:
                util.setMaterial(weapon.ent, self.material, None, teamColor, camo, self.debugging.materials)
        else:
            for ent in self.subentMaterial.keys():
                ent.setMaterialName(self.subentMaterial[ent])
            util.setMaterial(self.ent, self.material, "Tank_"+ self.body.name +".jpg", teamColor, camo, self.debugging.materials)
            for turret in self.turretList[1:]:
                util.setMaterial(turret.ent, turret.material, "Turret.jpg", teamColor, camo, self.debugging.materials)
            for weapon in self.weapon[1:]:
                util.setMaterial(weapon.ent, weapon.material, "Weapon_MountBox.jpg", teamColor, camo, self.debugging.materials)
        
    def setSpeed(self, speed):
        self.physBody.wake()
        if speed < -self.maxSpeed/2:
            speed = -self.maxSpeed/2
        elif speed > self.maxSpeed:
            speed = self.maxSpeed
        self.newSpeed = speed

    def setDirection(self, radian):
        self.physBody.wake()
        self.newDirection = util.wrapPi(radian)
        self.turned = False

    def setAim(self, radian):
        for turret in self.turretList:
            turret.newAim = util.wrapPi(radian)

    def loadSpecial(self, specialName):
        if specialName == "HoloCamo":
            from Special.HoloCamo import HoloCamo
            special = HoloCamo(self)
        elif specialName == "OverDrive":
            from Special.OverDrive import OverDrive
            special = OverDrive(self)
        elif specialName == "Radar":
            from Special.Radar import Radar
            special = Radar(self)
        elif specialName == "ScreenJam":
            from Special.ScreenJam import ScreenJam
            special = ScreenJam(self)
        else:
            raise Exception, "Bad special name: "+str(specialName)
        stat = equip.special(specialName)
        special.mass = stat.mass
        special.space = stat.space
        special.cost = stat.cost
        return special

    def readTankFile(self,tankFile):
        tankFile = "./tanks/" + tankFile # make this more portable later
        if not os.path.isfile(tankFile): 
            return False
        self.weapon.append(None)
        specialNum = 7
        for line in fileinput.input(tankFile):
            (param,value) = line.split(":")
            param = param.strip()
            value = value.strip()
            if param == "Vehicle":
                self.vehicle = value
            if param == "Designer":
                self.designer = value
            if param == "Body":
                self.body = equip.body(value)
            if param == "Engine":
                self.engine = equip.engine(value)
            if param.find("W#") > -1:
                wepnum = util.strToInt(param.split("#")[1])
                (mount, wepname) = value.split(" ",1)
                weapon = Weapon(wepname, wepnum, mount, self, self.debugging)
                self.weapon.append(weapon)
            if param == "Armor Type":
                self.armorType = equip.armor(value)
            if param == "Front":
                self.maxArmor["F"]= util.strToInt(value)
            if param == "Back":
                self.maxArmor["A"]= util.strToInt(value)
            if param == "Left":
                self.maxArmor["P"]= util.strToInt(value)
            if param == "Right":
                self.maxArmor["S"]= util.strToInt(value)
            if param == "Top":
                self.maxArmor["T"]= util.strToInt(value)
            if param == "Bottom":
                self.maxArmor["B"]= util.strToInt(value)
            if param == "Special":
                self.specialName[specialNum] = value
                self.special[value] = self.loadSpecial(value)
                specialNum += 1
            if param == "Heat sinks":
                self.heatSinks = equip.heatSink(util.strToInt(value))
            if param == "Suspension":
                self.suspension = equip.suspension(value)
            if param == "Treads":
                self.treads = equip.tread(value)
            if param == "Bumpers":
                self.bumpers = equip.bumper(value)
        return True

    def verifyDesign(self):
        self.mesh = self.body.name + ".mesh"
        self.armor = self.maxArmor.copy()
        self.highestArmor = max(sorted(self.armor.values()))
        self.maxFuel = self.engine.fuelCap
        # totals
        totalArmor = sum(self.armor.values())
        totalWeaponMass = 0
        totalWeaponSpace = 0
        totalWeaponCost = 0
        for weapon in self.weapon[1:]:
            totalWeaponMass += weapon.mass
            totalWeaponSpace += weapon.space
            totalWeaponCost += weapon.cost
        totalSpecialMass = 0
        totalSpecialSpace = 0
        totalSpecialCost = 0
        for special in self.special.values():
            if special:
                totalSpecialMass += special.mass
                totalSpecialSpace += special.space
                totalSpecialCost += special.cost
        size = self.body.size
        self.mass = self.body.mass + self.engine.mass + self.armorType.mass * totalArmor * size \
            + self.heatSinks.num * self.heatSinks.mass + totalWeaponMass + totalSpecialMass
        self.space = self.engine.space + self.armorType.space * totalArmor * size \
            + self.heatSinks.num * self.heatSinks.space + totalWeaponSpace + totalSpecialSpace
        self.cost = self.body.cost + self.engine.cost + self.armorType.cost * totalArmor * size \
            + self.heatSinks.num * self.heatSinks.cost + totalWeaponCost + totalSpecialCost\
            + (self.suspension.cost + self.treads.cost + self.bumpers.cost) * size 
        # derived statistics
        self.maxSpeed = float(self.engine.power / self.body.drag) **(0.3333) / self.treads.friction
        self.engineAccel = float(16 * self.engine.power) / self.mass
        self.treadAccel = float(self.treads.friction) * 2.5 # MAX_ACCEL
        if self.engineAccel < self.treadAccel: 
            self.accel = self.engineAccel
        else:
            self.accel = self.treadAccel # Xtank speed limit
        # engAcc affects speeding up?
        self.accel *= 15
        self.maxSpeed *= 15
        self.handling = self.body.handling + self.suspension.handling
        self.turnMax = float(self.handling) / 8 
        self.turnMax *= 15
        self.turnAccel = float(self.turnMax) / 1
        return True
