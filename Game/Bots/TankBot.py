from random import random, choice
from math import pi, copysign, sqrt, atan2
from collections import deque
import Game.Utility as util

class TankBot:
    '''Bot: TankBot

    Attributes:
        tank         (Tank) : My Tank. Mine!
        maze         (Maze) : current Maze
        active       (bool) : AI toggle
        avgShotSpeed (float): average projectile speed
        navStuckTimer(float): seconds before I decide I'm stuck
        navStuck     (float): seconds I can't move
        navWav       (float): wall avoidance distance

    Methods:
        think() : Bot logic
    '''

    def __init__(self, tank, maze, gametype = 'Team Combat'):
        self.tank = tank
        self.maze = maze
        self.active = False
        self.avgShotSpeed = 0
        if len(self.tank.weapon) > 1:
            for weapon in self.tank.weapon[1:]:
                self.avgShotSpeed += weapon.speed
            self.avgShotSpeed /= len(self.tank.weapon) -1
        self.navStuckTimer = 3
        self.navStuck = 0
        self.navWav = self.tank.body.momentInertia / 150
        self.navRoute = []
        self.navRouteType = None
        self.priority = {}
        self.priority["ammo"] = random()
        self.priority["fuel"] = random()
        self.priority["repair"] = random()
        self.urgency = {}
        self.urgency["ammo"] = 0
        self.urgency["fuel"] = 0
        self.urgency["repair"] = 0
        self.landmarks = {}
        for type in ("ammo","fuel","repair"):
            self.landmarks[type] = self.maze.getLandmarks(type)
        
    def spawn(self):
        self.navStuck = 0
        self.navRouteType = None
        self.navRoute = None
        self.tank.setDirection(random()*2 * pi)

    def think(self, time):        
        # Gunner
        enemy = None
        enemyDistance = 33177600
        blips = self.tank.scan()
        for blip in blips:
            if (blip.team != self.tank.team or blip.team is 0): 
                distance = blip.bearing.squaredLength()
                if distance < enemyDistance or blip.LOS and enemy and not enemy.LOS:
                    enemy = blip
                    enemyDistance = distance
        turret = self.tank.turretList[0]
        if enemy:
            shotTime = sqrt(enemyDistance) / self.avgShotSpeed
            aim = (enemy.bearing + enemy.velocity * shotTime - self.tank.velocity) 
            self.tank.setAim(atan2(aim.z,aim.x))
            difficulty = min(max(float(enemy.score - self.tank.score) / self.tank.cost, 0.0125), 1)
            if random() < difficulty:
                range = aim.squaredLength()
                for weapon in self.tank.weapon[1:]:
                    if range > weapon.range ** 2:
                        weapon.toggle("off")
                    else:
                        weapon.toggle("on")
                if enemy.LOS and not enemy.burning and abs(turret.newAim - turret.aim) < pi/8:
                    self.tank.shoot()
        else:
            self.tank.setAim(self.tank.newDirection)
        # Navigator
        if self.tank.LOSNavType == "DEST" and self.tank.LOSNav < 200 \
          and abs(self.tank.newDirection - turret.aim) < pi/8:
            self.tank.shoot()
        if self.tank.newSpeed != 0 and abs(self.tank.speed) < 3:
            self.navStuck += time
            if self.navStuck > self.navStuckTimer:
                self.navStuck = -self.navStuckTimer
                self.tank.setSpeed(copysign(self.tank.maxSpeed,self.tank.newSpeed) * -1)
                self.tank.setDirection(random()*2*pi)
                self.navRoute = None
                self.navRouteType = None
        if self.navStuck <0:
            self.navStuck += time
        else:
            self.tank.setSpeed((random()*0.5 + 0.25) * self.tank.maxSpeed)
        # Maze pathing
        # Prioritize urgency
        self.urgency["fuel"] = 1 - float(self.tank.fuel) / self.tank.engine.fuelCap + self.priority["fuel"]
        self.urgency["repair"] = 0
        lowestArmor = 1
        for side in ('F','A','P','S','T','B'):
            if self.tank.maxArmor[side] > 0:
                status = float(self.tank.armor[side]) / self.tank.maxArmor[side]
                if status < lowestArmor:
                    lowestArmor = status
        self.urgency["repair"] = 1 - lowestArmor + self.priority["repair"]
        lowestAmmo = 1
        for weapon in self.tank.weapon[1:]:
            status = float(weapon.ammo) / weapon.maxAmmo
            if status < lowestAmmo:
                lowestAmmo = status
        self.urgency["ammo"] = 1 - lowestAmmo + self.priority["ammo"]
        maxUrgency = max(self.urgency,key = lambda a: self.urgency.get(a))
        # Get path
        position = self.tank.posNode._getDerivedPosition()
        myNode = (self.tank.mazeBox.x,self.tank.mazeBox.y)
        if self.urgency[maxUrgency] > 1:
            if self.navRouteType != maxUrgency:
                self.navRoute = self.findRoute(maxUrgency)
            elif not self.navRoute:
                self.navRoute = [myNode]
        elif not self.navRoute:
            self.navRoute = self.findRoute("random")
        # Steering & throttle
        nextNode = self.navRoute[0]
        nodeX, nodeY = nextNode[0]*192 + 96, nextNode[1]*192 + 96
        self.tank.setDirection(atan2(nodeY - position.z, nodeX - position.x))
        if myNode == nextNode:
            if abs(position.x - nodeX) <50 and abs(position.z - nodeY) < 50:
                self.navRoute.pop(0)
                if len(self.navRoute) == 0:
                    self.tank.setSpeed(0)
            else:
                self.tank.setSpeed(self.tank.maxSpeed/4)
        else:
            self.tank.setSpeed(self.tank.maxSpeed)            
        return
        
        # OLD NAV
            #self.tank.setSpeed(self.tank.maxSpeed)
        if self.tank.LOSNav < 50 + self.tank.speed * 2 and self.tank.turned:
            course = choice([0,1,2,3]) *pi/2 + random()*pi/4 - pi/8
            if course < 0:
                course += 2*pi
            mazeX = int(position.x/192)
            mazeY = int(position.z/192)
            wall = self.maze.getWalls(mazeX, mazeY)
            index = int(course / (pi/2))
            if not wall[index]:
                self.tank.setDirection(course)

    def findRoute(self, type): #(type = "fuel"|"repair"|"ammo"|tankName):
        self.navRouteType = type
        myBox = self.tank.mazeBox
        startBox = (myBox.x, myBox.y)
        if type == "fuel" or type == "repair" or type == "ammo":
            goalBox = None
            goalRange = 900
            boxList = self.landmarks[type]
            if startBox in boxList:
                return [startBox]
            if not boxList:
                print "NO BOXLIST"
                exit()
            for box in boxList:
                distance = abs(startBox[0] - box[0]) + abs(startBox[1] - box[1])
                if distance < goalRange:
                    goalBox = box
                    goalRange = distance
        elif type == "random":
            while (True):
                goalBox = choice(self.maze.spawnBoxList)
                if goalBox != startBox:
                    break
        else:
            # goalBox = tankName current box # will update with every box change
            print "type "+str(type)
            exit()
        if not goalBox:
            print "NO GOAL"
            exit()
        # Leaf walk
        tree = [startBox]
        route = {}
        route[startBox] = []
        seen = {}
        foundRoute = None
        while not foundRoute and tree:
            node = tree.pop(0)
            seen[node] = True
            for neighbor in self.maze.getNeighbors(node[0], node[1]):
                if not neighbor in seen:
                    route[neighbor] = route[node][:]
                    route[neighbor].append(neighbor)
                    if neighbor == goalBox:
                        foundRoute = route[neighbor][:]
                    else:
                        tree.append(neighbor)
        return foundRoute

