import ogre.renderer.OGRE as ogre

class MOITest:
    '''Bot: MOITest'''

    def __init__(self, tank, gametype = 'Team Combat'):
        self.tank = tank
        self.spawnCool = 0
        self.turnForce = 0
        
    def spawn(self):
        self.spawnCool = 0
        self.turnForce = 0

    def think(self, time):
        turnRate = self.tank.physBody.getAngularVelocity().y
        if not self.tank.spawning and self.spawnCool < 5:
            self.spawnCool += time
            self.tank.debug = "SpawnCool"
        elif turnRate >= 1 and not self.tank.burning:
            self.turnForce *= -1
            self.tank.physBody.wake()
            self.tank.physBody.addRelativeTorque(ogre.Vector3(0, self.turnForce * self.tank.physMass, 0))
            self.tank.debug = "SpinDown"
        elif self.turnForce < 0:
            print self.tank.body.name +": "+ str(-self.turnForce)+" = "+str(turnRate)
            self.tank.burn()
            self.tank.debug = "Reset"
        else:
            increment = 10
            self.turnForce += increment
            self.tank.physBody.wake()
            self.tank.physBody.addRelativeTorque(ogre.Vector3(0, increment * self.tank.physMass, 0))
            self.tank.debug = "SpinUp"
            
