nXtank Aplha development lab 

Run "nXtank.py"

FILES
-----
Delete ogre.cfg to change config options at next startup.

=====
ARENA
=====

CONTROLS
--------

C toggles between topdown and gunner cameras.

Overhead View:

	LMB aims the player tank's turret and fires

	RMB steers the player tank

Gunner View:

	LMB fires at the aiming reticle

	RMB steers the player tank toward the aiming reticle

Mousewheel controls tank movement speed

W/S accelerates/decelerates

A/D turns left/right

SPACE stops

X toggles reverse

LeftArrow/RightArrow traverses left/right

UpArrow/DownArrow aligns turret to tank front/rear

RCTRL fires all player tank's weapons

1-6 fire individual weapon

= toggles weapons for reloading

SHIFT+1-6 toggles individual weapon

T toggles turret lock

B toggles AI control

V toggles sound effects

O toggles debug overlay

Q toggles visual quality

P pauses the simulation

TESTING
-------

F fires a shot from all non-player tanks

K takes a screenshot

Shift-K self-destruct

N switches Tanks in Demo mode

U unsticks a Tank which ODE has stuck

===========
TANK DESIGN
===========

L toggles 3rd-person 2-axis pseudo-mouselook for model testing purposes.

O toggles debug overlay

P pauses the simulation

